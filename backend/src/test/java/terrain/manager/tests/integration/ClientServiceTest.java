package terrain.manager.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.ClientDTO;
import terrain.manager.services.ClientService;
import terrain.manager.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class ClientServiceTest {

	@Autowired
	private ClientService clientService;

	@Test
	public void testCreate() {

       ClientDTO dto1 = new ClientDTO.Builder()
                .address("jud Cluj,Cluj, Str Andrei Saguna")
                .amount(1000)
                .email("adi.pacurarbg@gmail.com")
                .ICN("234633")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argV1")
                .username("argc")
                .create();
       
      // ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Cluj,Cluj, Str Andrei Saguna","regular",1000,new Date(),"argc","argv","adi.pacurarbg@gmail.com");
		
		ClientDTO dto2 = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		clientService.create(dto1);
		clientService.create(dto2);

		List<ClientDTO> fromDB = clientService.findAll();

		assertTrue("One entity inserted", fromDB.size() == 2);
	}
	@Test
	public void testFindByType() {

		ClientDTO dto1 = new ClientDTO.Builder()
                .address("jud Cluj,Cluj, Str Andrei Saguna")
                .amount(1000)
                .email("adi.pacurarbg@gmail.com")
                .ICN("234633")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argV1")
                .username("argc")
                .create();
       
      // ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Cluj,Cluj, Str Andrei Saguna","regular",1000,new Date(),"argc","argv","adi.pacurarbg@gmail.com");
		
		ClientDTO dto2 = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		clientService.create(dto1);
		clientService.create(dto2);

		List<ClientDTO> fromDB = clientService.findClientByType("regular");

		assertTrue("Two entities inserted", fromDB.size() == 2);
	}
	@Test
	public void testGetByIdSuccessful() {

        ClientDTO dto = new ClientDTO.Builder()
        		.address("jud Cluj,Cluj, Str Andrei Saguna")
        		 .amount(1000)
                 .email("adi.pacurarbg@gmail.com")
                 .ICN("234632")
                 .name("Ion Agarboviceanu")
                 .type("regular")
                 .PNC("1285938569")
                 .password("argV1")
                 .username("argc")
                .create();

		int clientId = clientService.create(dto);
		ClientDTO accFromDB = clientService.findClientAccountById(clientId);
		ClientDTO infoFromDB = clientService.findClientInformationById(clientId);
		
		assertTrue("Adress ", dto.getAddress().equals(infoFromDB.getAddress()));
		assertTrue("PNC ", dto.getPNC().equals(infoFromDB.getPNC()));
		assertTrue("ICN ", dto.getICN().equals(infoFromDB.getICN()));
		assertTrue("Name ", dto.getName().equals(infoFromDB.getName()));
		
		assertTrue("Email ", dto.getEmail().equals(accFromDB.getEmail()));
		assertTrue("Amount ", Double.compare(dto.getAmount(),accFromDB.getAmount())==0);
		assertTrue("Username ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Password ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Type ", dto.getType().equals(accFromDB.getType()));
	}


	


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulArea() {

		ClientDTO dto = new ClientDTO.Builder()
				
				.address("jud Cluj,Cluj, Str Andrei Saguna")
       		 	.amount(-1000)
                .email("adi.pacurarbg@gmail.com")
                .ICN("234630")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argV1")
                .username("argc")
                
				.create();

		int clientId = clientService.create(dto);
		ClientDTO accFromDB = clientService.findClientAccountById(clientId);
		ClientDTO infoFromDB = clientService.findClientInformationById(clientId);
		
		assertTrue("Adress ", dto.getAddress().equals(infoFromDB.getAddress()));
		assertTrue("PNC ", dto.getPNC().equals(infoFromDB.getPNC()));
		assertTrue("ICN ", dto.getICN().equals(infoFromDB.getICN()));
		assertTrue("Name ", dto.getName().equals(infoFromDB.getName()));
		
		assertTrue("Email ", dto.getEmail().equals(accFromDB.getEmail()));
		assertTrue("Amount ", Double.compare(dto.getAmount(),accFromDB.getAmount())==0);
		assertTrue("Username ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Password ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Type ", dto.getType().equals(accFromDB.getType()));
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulEmail() {

		ClientDTO dto = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Andrei Saguna")
       		 	.amount(1000)
                .email("kjg9934o3")
                .ICN("234633")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argV1")
                .username("argc")
				.create();

		int clientId = clientService.create(dto);
		ClientDTO accFromDB = clientService.findClientAccountById(clientId);
		ClientDTO infoFromDB = clientService.findClientInformationById(clientId);
		
		assertTrue("Adress ", dto.getAddress().equals(infoFromDB.getAddress()));
		assertTrue("PNC ", dto.getPNC().equals(infoFromDB.getPNC()));
		assertTrue("ICN ", dto.getICN().equals(infoFromDB.getICN()));
		assertTrue("Name ", dto.getName().equals(infoFromDB.getName()));
		
		assertTrue("Email ", dto.getEmail().equals(accFromDB.getEmail()));
		assertTrue("Amount ", Double.compare(dto.getAmount(),accFromDB.getAmount())==0);
		assertTrue("Username ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Password ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Type ", dto.getType().equals(accFromDB.getType()));
	}


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulPassword() {

		ClientDTO dto = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Andrei Saguna")
       		 	.amount(1000)
                .email("adi.pacurarbg@gmail.com")
                .ICN("234631")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argv")
                .username("argc")
				.create();

		int clientId = clientService.create(dto);
		ClientDTO accFromDB = clientService.findClientAccountById(clientId);
		ClientDTO infoFromDB = clientService.findClientInformationById(clientId);
		
		assertTrue("Adress ", dto.getAddress().equals(infoFromDB.getAddress()));
		assertTrue("PNC ", dto.getPNC().equals(infoFromDB.getPNC()));
		assertTrue("ICN ", dto.getICN().equals(infoFromDB.getICN()));
		assertTrue("Name ", dto.getName().equals(infoFromDB.getName()));
		
		assertTrue("Email ", dto.getEmail().equals(accFromDB.getEmail()));
		assertTrue("Amount ", Double.compare(dto.getAmount(),accFromDB.getAmount())==0);
		assertTrue("Username ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Password ", dto.getUsername().equals(accFromDB.getUsername()));
		assertTrue("Type ", dto.getType().equals(accFromDB.getType()));
		
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

        ClientDTO dto = new ClientDTO.Builder()
        		.address("jud Cluj,Cluj, Str Andrei Saguna")
       		 	.amount(1000)
                .email("adi.pacurarbg@gmail.com")
                .ICN("234630")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("1285938569")
                .password("argV1")
                .username("argc")
                .create();

		int clientId = clientService.create(dto);
		clientService.findClientAccountById(clientId + 1);

	}

}
