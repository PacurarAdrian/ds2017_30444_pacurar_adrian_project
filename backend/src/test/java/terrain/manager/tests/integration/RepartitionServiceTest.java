package terrain.manager.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.ClientDTO;
import terrain.manager.dto.RepartitionDTO;
import terrain.manager.dto.RepartitionLightDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.entities.Terrain;
import terrain.manager.entities.Type;
import terrain.manager.services.ClientService;
import terrain.manager.services.RepartitionService;
import terrain.manager.services.TerrainService;
import terrain.manager.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class RepartitionServiceTest {

	@Autowired
	private RepartitionService historyService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private TerrainService terrServ;

	
	@Test
	public void testCreate() {

     
		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(10613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
		ClientDTO client2 = new ClientDTO.Builder()
				.address("jud Dambovita")
                .amount(14366)
                .email("adi.pacurarbg@gmail.com")
                .ICN("669879")
                .name("Ion Agarboviceanu")
                .type("regular")
                .PNC("7545898459")
                .password("argV1")
                .username("argc")
                .create();     

		client.setId(clientService.create(client));
		client2.setId(clientService.create(client2));
		
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("Agricultural")
                .ownerID(null)
                .create();
		TerrainDTO terrain2 = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de Mijloc, nr 345, Cluj,RO")
                .type("Agricultural")
                .ownerID(null)
                .create();
		Type type=new Type();
		
		type.setName("Agricultural");
		type.setPrice(3);
		
		terrServ.createType(type);
		
		
		
		RepartitionLightDTO dto1=new RepartitionLightDTO();
		dto1.setClient(client.getId());
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)).getId());
		
		
		
		RepartitionLightDTO dto2=new RepartitionLightDTO();
		dto2.setClient(client2.getId());
		dto2.setTerrain(terrServ.findTerrainById(terrServ.create(terrain2)).getId());
		
		

		historyService.createLight(dto1);
		historyService.createLight(dto2);

		List<RepartitionDTO> fromDB = historyService.findAll();

		assertTrue("Two entities inserted", fromDB.size() == 2);
	}
    
	@Test
	public void testGetByIdSuccessful() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(10613)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		
		Type type=new Type();
		type.setName("Agricultural");
		type.setPrice(3);
		terrServ.createType(type);
		
		Terrain terrOb=new Terrain(1,type,1,899.0,"fadsa");
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("Agricultural")
                .ownerID(null)
                .create();
		

		
		
		
		RepartitionLightDTO dto1=new RepartitionLightDTO();
		dto1.setClient(client.getId());
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)).getId());
		

		int terrainId = historyService.createLight(dto1);
		RepartitionDTO fromDB = historyService.findRepartitionById(terrainId);

		assertTrue("Client ", dto1.getClient()==fromDB.getClient().getId());
		
		assertTrue("Terrain ", dto1.getTerrain()==fromDB.getTerrain().getId());
		//System.out.println("credit "+clientService.findClientAccountById(dto1.getClient()).getAmount());
		assertTrue("Credit",clientService.findClientAccountById(dto1.getClient()).getAmount()==10613-TerrainService.computePrice(terrOb) );
	
	}


	


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulClient() {

		
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("Agricultural")
                .ownerID(null)
                .create();
		

		
		
		
		RepartitionLightDTO dto1=new RepartitionLightDTO();
		
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)).getId());
		
		

		int terrainId = historyService.createLight(dto1);
		RepartitionDTO fromDB = historyService.findRepartitionById(terrainId);

	
		
		
		assertTrue("Terrain ", dto1.getTerrain()==fromDB.getTerrain().getId());
		
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulTerrain() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		
		

		
		
		
		RepartitionLightDTO dto1=new RepartitionLightDTO();
		dto1.setClient(client.getId());
		
		
		

		int terrainId = historyService.createLight(dto1);
		RepartitionDTO fromDB = historyService.findRepartitionById(terrainId);

		assertTrue("Client ", dto1.getClient()==fromDB.getClient().getId());
		
		
		
	}


	
	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(10613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("Agricultural")
                .ownerID(null)
                .create();
		Type type=new Type();
		
		type.setName("Agricultural");
		type.setPrice(3);
		
		terrServ.createType(type);
		
		RepartitionLightDTO dto1=new RepartitionLightDTO();
		dto1.setClient(client.getId());
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)).getId());
		

		

		int terrainId = historyService.createLight(dto1);
		historyService.findRepartitionById(terrainId + 1);

	}

}
