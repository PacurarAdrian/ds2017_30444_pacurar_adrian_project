package terrain.manager.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.entities.Type;
import terrain.manager.services.TerrainService;
import terrain.manager.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class TerrainServiceTest {

	@Autowired
	private TerrainService terrainService;

	@Test
	public void testCreate() {

       /* TerrainDTO dto1 = new TerrainDTO.Builder()
                .area(345)
                .place("Str. de jos, nr 8907, Cluj,RO")
                .type("agricultural")
                .ownerID(0)
                .create();*/
        TerrainDTO dto1=new TerrainDTO("agricultural",null, 345,"Str. de jos, nr 8907, Cluj,RO");
        TerrainDTO dto2 = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		
		terrainService.create(dto1);
		terrainService.create(dto2);

		List<TerrainDTO> fromDB = terrainService.findAll();

		assertTrue("One entity inserted", fromDB.size() == 2);
	}
    
	@Test
	public void testGetByIdSuccessful() {

        TerrainDTO dto = new TerrainDTO.Builder()
        		.area(345)
                .place("Str. de jos, nr 8907, Cluj,RO")
                .type("agricultural")
                
                .create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		int terrainId = terrainService.create(dto);
		TerrainDTO fromDB = terrainService.findTerrainById(terrainId);

		assertTrue("Place ", dto.getPlace().equals(fromDB.getPlace()));
		
		assertTrue("Area ", Double.compare(dto.getArea(),fromDB.getArea())==0);
		assertTrue("Type ", dto.getType().equals(fromDB.getType()));
	
	}


	


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulArea() {

		TerrainDTO dto = new TerrainDTO.Builder()
				
                .place("Str. de jos, nr 8907, Cluj,RO")
                .type("agricultural")
                
				.create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		int terrainId = terrainService.create(dto);
		TerrainDTO fromDB = terrainService.findTerrainById(terrainId);

		assertTrue("Place ", dto.getPlace().equals(fromDB.getPlace()));
		assertTrue("Type ", dto.getType().equals(fromDB.getType()));
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulPlace() {

		TerrainDTO dto = new TerrainDTO.Builder()
				.area(345)
                
                .type("agricultural")
                
				.create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		int terrainId = terrainService.create(dto);
		TerrainDTO fromDB = terrainService.findTerrainById(terrainId);

		
		
		assertTrue("Area ", Double.compare(dto.getArea(),fromDB.getArea())!=0);
		assertTrue("Type ", dto.getType().equals(fromDB.getType()));
	}


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulType() {

		TerrainDTO dto = new TerrainDTO.Builder()
				.area(345)
                .place("Str. de jos, nr 8907, Cluj,RO")
                
				.create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		int terrainId = terrainService.create(dto);
		TerrainDTO fromDB = terrainService.findTerrainById(terrainId);

		assertTrue("Place ", dto.getPlace().equals(fromDB.getPlace()));
		
		assertTrue("Area ", Double.compare(dto.getArea(),fromDB.getArea())==0);
		
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

        TerrainDTO dto = new TerrainDTO.Builder()
        		.area(345)
                .place("Str. de jos, nr 8907, Cluj,RO")
                .type("agricultural")
                
                .create();
        Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrainService.createType(type);
		int terrainId = terrainService.create(dto);
		terrainService.findTerrainById(terrainId + 1);

	}

}
