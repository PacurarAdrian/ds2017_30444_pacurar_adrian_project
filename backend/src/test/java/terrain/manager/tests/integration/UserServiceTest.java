package terrain.manager.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.UserDTO;
import terrain.manager.services.UserService;
import terrain.manager.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void testCreate() {

        UserDTO dto1 = new UserDTO.Builder()
                .firstname("Kona")
                .surname("Diana")
                .city("Cluj")
                .address("Str. de jos, nr 326")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0854357824")
                .country("RO")
                .postcode("85943")
                .salary(2450)
                .username("didiana")
                .password("PassParola1")
                .type(userService.getTypes().get(1))
                .create();
       // UserDTO userDTO=new UserDTO("Konay","Diana","adi.pacurarbg@gmail.com","Str. de jos, nr 326","23534","Cluj","RO", "0542345346", 2450,"passparola",userService.getTypes().get(1));
    	
        UserDTO dto2 = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userService.getTypes().get(1))
                .create();

		userService.create(dto1);
		userService.create(dto2);

		List<UserDTO> fromDB = userService.findAll();

		assertTrue("Two entities inserted", fromDB.size() == 2);
	}
	@Test
	public void testFindByType() {

        UserDTO dto1 = new UserDTO.Builder()
                .firstname("Kona")
                .surname("Diana")
                .city("Cluj")
                .address("Str. de jos, nr 326")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0854357824")
                .country("RO")
                .postcode("85943")
                .salary(2450)
                .username("didiana")
                .password("PassParola1")
                .type(userService.getTypes().get(0))
                .create();
       // UserDTO userDTO=new UserDTO("Konay","Diana","adi.pacurarbg@gmail.com","Str. de jos, nr 326","23534","Cluj","RO", "0542345346", 2450,"passparola",userService.getTypes().get(1));
    	
        UserDTO dto2 = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userService.getTypes().get(0))
                .create();

		userService.create(dto1);
		userService.create(dto2);

		List<UserDTO> fromDB = userService.findUserByType(userService.getTypes().get(0));

		assertTrue("Two entities inserted", fromDB.size() == 2);
	}
	@Test
	public void testGetByIdSuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .firstname("Razvan")
                .surname("Ursutz")
                .city("Cluj")
                .address("Bontzida, nr 73")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0854357824")
                .country("RO")
                .salary(859.0)
                .username("razvy_94")
                .password("Uzzi94")
                .type("employee")
                .create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("First Name ", dto.getFirstname().equals(fromDB.getFirstname()));
		assertTrue("Sur Name ", dto.getSurname().equals(fromDB.getSurname()));
		assertTrue("City ", dto.getCity().equals(fromDB.getCity()));
		assertTrue("Address", dto.getAddress().equals(fromDB.getAddress()));
		assertTrue("Email ", dto.getEmail().equals(fromDB.getEmail()));
		assertTrue("Telephone", dto.getTelephone().equals(fromDB.getTelephone()));
		assertTrue("Country ", dto.getCountry().equals(fromDB.getCountry()));
		assertTrue("Salary ", Double.compare(dto.getSalary(),fromDB.getSalary())==0);
		assertTrue("Username ", dto.getUsername().equals(fromDB.getUsername()));
		assertTrue("Password ", dto.getPassword().equals(fromDB.getPassword()));
		assertTrue("Type ", dto.getType().equals(fromDB.getType()));
	}


	@Test
	public void testGetByIdDoubleFirstName() {

		UserDTO dto = new UserDTO.Builder()				
				 .firstname("Razvan Dan")
                .surname("Ursutz")
                .email("adi.pacurarbg@gmail.com")
                .salary(345)
                .username("razvy_94")
                .password("Uzzi94")
                .type("employee")
				.create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);
//System.out.println(dto.getSalary()+"salary"+fromDB.getSalary());
		assertTrue("First Name ", dto.getFirstname().equals(fromDB.getFirstname()));
		assertTrue("Sur Name ", dto.getSurname().equals(fromDB.getSurname()));
		assertTrue("Salary ", Double.compare(dto.getSalary(),fromDB.getSalary())==0);
		assertTrue("Username ", dto.getUsername().equals(fromDB.getUsername()));
		assertTrue("Password ", dto.getPassword().equals(fromDB.getPassword()));
		assertTrue("Type ", dto.getType().equals(fromDB.getType()));
		assertTrue("Email ", dto.getEmail().equals(fromDB.getEmail()));
	}


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulEmail() {

		UserDTO dto = new UserDTO.Builder()
				.firstname("First First2")
				.surname("User")
				.salary(859.0)
                .username("razvy_94")
                .password("uzzi_94")
                .type("employee")
				.create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("First Name ", dto.getFirstname().equals(fromDB.getFirstname()));
		assertTrue("Sur Name ", dto.getSurname().equals(fromDB.getSurname()));
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulSurname() {

		UserDTO dto = new UserDTO.Builder()
				.firstname("First First2")
				.email("user@gmail.com")
				.salary(859.0)
                .username("razvy_94")
                .password("uzzi_94")
                .type("employee")
				.create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("First Name ", dto.getFirstname().equals(fromDB.getFirstname()));
		assertTrue("Sur Name ", dto.getSurname().equals(fromDB.getSurname()));
	}


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulFirstname() {

		UserDTO dto = new UserDTO.Builder()
				.surname("Surname")
				.email("user@gmail.com")
				.salary(344)
                .username("razvy_94")
                .password("uzzi_94")
                .type("employee")
				.create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("First Name ", dto.getFirstname().equals(fromDB.getFirstname()));
		assertTrue("Sur Name ", dto.getSurname().equals(fromDB.getSurname()));
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .firstname("Victor")
                .surname("Babes")
                .city("Dambovita")
                .address("str Craiului, nr 100")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0735634562")
                .country("RO")
                .salary(435)
                .username("victor")
                .password("Vbabesh1")
                .type("regular")
                .create();

		int userId = userService.create(dto);
		userService.findUserById(userId + 1);

	}

}
