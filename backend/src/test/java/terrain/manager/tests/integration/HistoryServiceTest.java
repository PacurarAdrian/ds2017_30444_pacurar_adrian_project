package terrain.manager.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.ClientDTO;
import terrain.manager.dto.HistoryDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.dto.UserDTO;
import terrain.manager.entities.Type;
import terrain.manager.services.ClientService;
import terrain.manager.services.HistoryService;
import terrain.manager.services.TerrainService;
import terrain.manager.services.UserService;
import terrain.manager.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class HistoryServiceTest {

	@Autowired
	private HistoryService historyService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private TerrainService terrServ;

	@Autowired
	private UserService userServ;
	
	@Test
	public void testCreate() {

     
		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrServ.createType(type);
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
		UserDTO user = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userServ.getTypes().get(0))
                .create();

		
		
		
		HistoryDTO dto1=new HistoryDTO();
		dto1.setClient(client);
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)));
		dto1.setUser(userServ.findUserById(userServ.create(user)));
		dto1.setOperation("Client bought terrain");
		
		HistoryDTO dto2=new HistoryDTO();
		dto2.setClient(clientService.findAll().get(0));
		dto2.setTerrain(terrServ.findAll().get(0));
		dto2.setUser(userServ.findAll().get(0));
		dto2.setOperation("Client client sold terrain terrain");

		historyService.create(dto1);
		historyService.create(dto2);

		List<HistoryDTO> fromDB = historyService.findAll();

		assertTrue("One entity inserted", fromDB.size() == 2);
	}
    
	@Test
	public void testGetByIdSuccessful() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrServ.createType(type);
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
		UserDTO user = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userServ.getTypes().get(0))
                .create();

		
		
		
		HistoryDTO dto1=new HistoryDTO();
		dto1.setClient(client);
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)));
		dto1.setUser(userServ.findUserById(userServ.create(user)));
		dto1.setOperation("Client bought terrain");

		int terrainId = historyService.create(dto1);
		HistoryDTO fromDB = historyService.findHistoryById(terrainId);

		assertTrue("Client ", dto1.getClient().getId()==fromDB.getClient().getId());
		assertTrue("Operation ", dto1.getOperation().equals(fromDB.getOperation()));
		assertTrue("Terrain ", dto1.getTerrain().getId()==fromDB.getTerrain().getId());
		assertTrue("User ", dto1.getUser().getId()==fromDB.getUser().getId());
		
	
	}


	


	@Test
	public void testCreateSuccessfulWitoutClient() {

		
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
		Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrServ.createType(type);
		UserDTO user = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userServ.getTypes().get(0))
                .create();

		
		
		
		HistoryDTO dto1=new HistoryDTO();
		
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)));
		dto1.setUser(userServ.findUserById(userServ.create(user)));
		dto1.setOperation("Client bought terrain");

		int terrainId = historyService.create(dto1);
		HistoryDTO fromDB = historyService.findHistoryById(terrainId);

	
		
		assertTrue("Operation ", dto1.getOperation().equals(fromDB.getOperation()));
		assertTrue("Terrain ", dto1.getTerrain().getId()==fromDB.getTerrain().getId());
		assertTrue("User ", dto1.getUser().getId()==fromDB.getUser().getId());
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulTerrain() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                

		client.setId(clientService.create(client));
		
		UserDTO user = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userServ.getTypes().get(0))
                .create();

		
		
		
		HistoryDTO dto1=new HistoryDTO();
		dto1.setClient(client);
		
		dto1.setUser(userServ.findUserById(userServ.create(user)));
		dto1.setOperation("Client bought terrain");

		int terrainId = historyService.create(dto1);
		HistoryDTO fromDB = historyService.findHistoryById(terrainId);

		assertTrue("Client ", dto1.getClient().getId()==fromDB.getClient().getId());
		assertTrue("Operation ", dto1.getOperation().equals(fromDB.getOperation()));
		
		assertTrue("User ", dto1.getUser().getId()==fromDB.getUser().getId());
	}


	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulUser() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                
		Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrServ.createType(type);
		client.setId(clientService.create(client));
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
		
		
		HistoryDTO dto1=new HistoryDTO();
		dto1.setClient(client);
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)));
		
		dto1.setOperation("Client bought terrain");

		int terrainId = historyService.create(dto1);
		HistoryDTO fromDB = historyService.findHistoryById(terrainId);

		assertTrue("Client ", dto1.getClient().getId()==fromDB.getClient().getId());
		assertTrue("Operation ", dto1.getOperation().equals(fromDB.getOperation()));
		assertTrue("Terrain ", dto1.getTerrain().getId()==fromDB.getTerrain().getId());
		
		
		
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

		ClientDTO client = new ClientDTO.Builder()
				.address("jud Cluj,Cluj, Str Dorobantilor")
                .amount(613.45)
                .email("adi.pacurarbg@gmail.com")
                .ICN("666579")
                .name("Tinca Olteanu")
                .type("regular")
                .PNC("7545333289")
                .password("TINCAr1tatata")
                .username("secretary1")
                .create();
                
		Type type=new Type();
		
		type.setName("agricultural");
		type.setPrice(0.1);
		terrServ.createType(type);
		client.setId(clientService.create(client));
		TerrainDTO terrain = new TerrainDTO.Builder()
        		.area(899)
                .place("Str. de sus, nr 42, Oradea,RO")
                .type("agricultural")
                .ownerID(null)
                .create();
		UserDTO user = new UserDTO.Builder()
                .firstname("Stefanescu")
                .surname(" Delavrancea")
                .city("Timisoara")
                .address("str Clujului, nr 3244")
                .email("adi.pacurarbg@gmail.com")
                .telephone("0734645475")
                .country("RO")
                .salary(2000)
                .postcode("65645")
                .username("std_dev")
                .password("DeUndeMam1")
                .type(userServ.getTypes().get(0))
                .create();

		
		
		
		HistoryDTO dto1=new HistoryDTO();
		dto1.setClient(client);
		dto1.setTerrain(terrServ.findTerrainById(terrServ.create(terrain)));
		dto1.setUser(userServ.findUserById(userServ.create(user)));
		dto1.setOperation("Client bought terrain");

		

		int terrainId = historyService.create(dto1);
		historyService.findHistoryById(terrainId + 1);

	}

}
