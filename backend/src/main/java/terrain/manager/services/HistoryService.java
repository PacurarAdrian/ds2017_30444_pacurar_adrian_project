package terrain.manager.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.dto.HistoryDTO;
import terrain.manager.entities.ClientAccount;
import terrain.manager.entities.History;
import terrain.manager.entities.PersistenceClient;
import terrain.manager.entities.PersistenceTerrain;
import terrain.manager.entities.PersistenceUser;
import terrain.manager.entities.Terrain;
import terrain.manager.entities.User;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.repositories.ClientAccountRepository;
import terrain.manager.repositories.HistoryRepository;
import terrain.manager.repositories.PersistenceClientRepository;
import terrain.manager.repositories.PersistenceTerrainRepository;
import terrain.manager.repositories.PersistenceUserRepository;
import terrain.manager.repositories.TerrainRepository;
import terrain.manager.repositories.UserRepository;

@Service
public class HistoryService {
	@Autowired
	private HistoryRepository historyRepository;
	@Autowired
	private PersistenceTerrainRepository terrRepository;
	@Autowired
	private PersistenceClientRepository accRepository;
	@Autowired
	private PersistenceUserRepository userRepository;
	@Autowired
	private TerrainRepository terrRep;
	@Autowired
	private ClientAccountRepository accRep;
	@Autowired
	private UserRepository userRep;
	
	public List<HistoryDTO> findHistoryByDates(Date start,Date end) {
		
		List<History> hists = historyRepository.findByDate(start,end);
		List<HistoryDTO> toReturn = new ArrayList<HistoryDTO>();
		for (History hist : hists) {
			HistoryDTO dto = new HistoryDTO.Builder()
						.id(hist.getId())
						.date(hist.getDate())
						.operation(hist.getOperation())
						.terrain(PersistenceTerrainService.terrainToDTOBrief(hist.getTerrain()))
						
						.user(PersistenceUserService.userToDTOBrief(hist.getUser()))
						.create();
			if(hist.getClient()!=null)
				dto.setClient(PersistenceClientService.clientToDTOBrief(hist.getClient()));
			toReturn.add(dto);
		}
		return toReturn;
	}
	public List<HistoryDTO> findHistoryByDate(Date start) {
		Date end = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(start); 
		c.add(Calendar.DATE, 1);
		end = c.getTime();
		List<History> hists = historyRepository.findByDate(start,end);
		List<HistoryDTO> toReturn = new ArrayList<HistoryDTO>();
		for (History hist : hists) {
			HistoryDTO dto = new HistoryDTO.Builder()
						.id(hist.getId())
						.date(hist.getDate())
						.operation(hist.getOperation())
						.terrain(PersistenceTerrainService.terrainToDTOBrief(hist.getTerrain()))
						
						.user(PersistenceUserService.userToDTOBrief(hist.getUser()))
						.create();
			if(hist.getClient()!=null)
				dto.setClient(PersistenceClientService.clientToDTOBrief(hist.getClient()));
			toReturn.add(dto);
		}
		return toReturn;
	}
	public void deleteHistory(int histId){
		try{
		historyRepository.delete(histId);
		}
		catch(IllegalArgumentException e){
			throw new ResourceNotFoundException(History.class.getSimpleName());
		}
		
	}
	public HistoryDTO findHistoryById(int histId) {
		History hist = historyRepository.findById(histId);
		if (hist == null) {
			throw new ResourceNotFoundException(History.class.getSimpleName());
		}

		HistoryDTO dto = new HistoryDTO.Builder()
						.id(hist.getId())
						.date(hist.getDate())
						.operation(hist.getOperation())
						.terrain(PersistenceTerrainService.terrainToDTOBrief(hist.getTerrain()))
						
						.user(PersistenceUserService.userToDTOBrief(hist.getUser()))
						.create();
		if(hist.getClient()!=null)
			dto.setClient(PersistenceClientService.clientToDTOBrief(hist.getClient()));
		return dto;
	}
	
	public List<HistoryDTO> findAll() {
		List<History> hists = historyRepository.findAll();
		List<HistoryDTO> toReturn = new ArrayList<HistoryDTO>();
		for (History history : hists) {
			
			HistoryDTO dto = new HistoryDTO.Builder()
						.id(history.getId())
						.date(history.getDate())
						.operation(history.getOperation())
						.terrain(PersistenceTerrainService.terrainToDTOBrief(history.getTerrain()))
					
						.user(PersistenceUserService.userToDTOBrief(history.getUser()))
						.create();
			if(history.getClient()!=null)
				dto.setClient(PersistenceClientService.clientToDTOBrief(history.getClient()));
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(HistoryDTO historyDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateHistory(historyDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(History.class.getSimpleName(),validationErrors);
		}

		History history = new History();
		//user.setId(userDTO.getId());
		PersistenceClient info=null;
		if(historyDTO.getClientId()!=0)
		{
			info = accRepository.findById(historyDTO.getClientId());
			
			if(info==null)
			{
				info=new PersistenceClient();
				ClientAccount acc=accRep.findById(historyDTO.getClientId());
				info.setAddress(acc.getInfo().getAddress());
				info.setAmount(acc.getAmount());
				info.setCreationDate(acc.getCreationDate());
				info.setEmail(acc.getEmail());
				info.setICN(acc.getInfo().getICN());
				info.setId(acc.getId());
				info.setName(acc.getInfo().getName());
				info.setPnc(acc.getInfo().getPnc());
				info.setType(acc.getType());
				info=accRepository.save(info);
			}
		}
		PersistenceTerrain terr = terrRepository.findById(historyDTO.getTerrainId());
		if(terr==null)
		{
			terr=new PersistenceTerrain();
			Terrain t=terrRep.findById(historyDTO.getTerrainId());
			terr.setArea(t.getArea());
			terr.setId(t.getId());
			terr.setPnc(info.getPnc());
			terr.setPlace(t.getPlace());
			terr.setPrice(TerrainService.computePrice(t));
			terr.setType(t.getType().getName());
			terr=terrRepository.save(terr);
		}
		PersistenceUser usr=userRepository.findById(historyDTO.getUserId());
		if(usr==null)
		{
			usr=new PersistenceUser();
			User u=userRep.findById(historyDTO.getUserId());
			usr.setAddress(u.getAddress());
			usr.setCity(u.getCity());
			usr.setCountry(u.getCountry());
			usr.setCreated(u.getCreated());
			usr.setEmail(u.getEmail());
			usr.setId(u.getId());
			usr.setName(u.getName());
			usr.setPostcode(u.getPostcode());
			usr.setSalary(u.getSalary());
			usr.setTelephone(u.getTelephone());
			usr.setType(u.getType());
			usr=userRepository.save(usr);
		}
		history.setClient(info);
		history.setDate(new Date());
		history.setOperation(historyDTO.getOperation());
		history.setTerrain(terr);
		history.setUser(usr);
		


		History hist = historyRepository.save(history);
		//userDTO.setId(user.getId());
		System.out.println(hist.getId());
		return hist.getId();
	}
	
	public void update(HistoryDTO historyDTO) {
		//System.out.println("intra");
		
		History history =historyRepository.findById(historyDTO.getId());
		
		if (history == null) {
			throw new ResourceNotFoundException(History.class.getSimpleName());
		}else{
			List<String> validationErrors = validateHistory(historyDTO);
			if (!validationErrors.isEmpty()) {
				throw new EntityValidationException(History.class.getSimpleName(),validationErrors);
			}
			PersistenceClient info=null;
			if(historyDTO.getClient()!=null)
				info = accRepository.findById(historyDTO.getClient().getId());
			
			PersistenceTerrain terr = terrRepository.findById(historyDTO.getTerrain().getId());
			PersistenceUser usr=userRepository.findById(historyDTO.getUser().getId());
			
			history.setClient(info);
			history.setOperation(historyDTO.getOperation());
			history.setTerrain(terr);
			history.setUser(usr);
			
		
		
			History hist = historyRepository.save(history);
			//userDTO.setId(user.getId());
			System.out.println(hist.getId());
		
		
		}
		
	}

	private List<String> validateHistory(HistoryDTO hist) {
		List<String> validationErrors = new ArrayList<String>();

		if (hist.getOperation() == null || "".equals(hist.getOperation())) {
			validationErrors.add("Operation field should not be empty");
		}

		if (hist.getUser() == null&&hist.getUserId()==0) {
			validationErrors.add("User field should not be empty");
		}
		if (hist.getTerrain() == null&&hist.getUserId()==0) {
			validationErrors.add("Terrain field should not be empty");
		}
		
		
		
		return validationErrors;
	}


}
