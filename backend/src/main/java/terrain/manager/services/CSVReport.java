package terrain.manager.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import terrain.manager.dto.HistoryDTO;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

public class CSVReport implements Report{
	
	public HttpServletResponse download(HttpServletResponse response,List<HistoryDTO> listBooks) throws IOException
	{
		String csvFileName = "HistoryOperation.csv";

	       response.setContentType("text/csv");

	       // creates mock data
	       String headerKey = "Content-Disposition";
	       String headerValue = String.format("attachment; filename=\"%s\"",
	               csvFileName);
	       response.setHeader(headerKey, headerValue);

	      //Book book1=new Book("0547928211","The Fellowship of the Ring: Being the First Part of The Lord of the Rings","J.R.R. Tolkien","Science Fiction & Fantasy",123,8.29, "2012-09-18");
			//Book book2=new Book("0553380168","A Brief History of Time","Stephen Hawking","Astrophysics & Space Science",341,11.11, "1998-09-01");
			//Book book3=new Book("0679417397","Nineteen Eighty-Four","George Orwell","Political",313,12, "1992-11-03");

	       

	       // uses the Super CSV API to generate CSV data from the model data
	       ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
	               CsvPreference.STANDARD_PREFERENCE);

	       String[] header = { "Operation","User", "Client", "Terrain","Date" };

	       csvWriter.writeHeader(header);

	       for (HistoryDTO aBook : listBooks) {
	           csvWriter.write(aBook, header);
	       }

	       csvWriter.close();
	       return response;
	}

}
