package terrain.manager.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.dto.TerrainDTO;
import terrain.manager.entities.PersistenceTerrain;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.repositories.PersistenceTerrainRepository;

@Service
public class PersistenceTerrainService {
	
	@Autowired
	private PersistenceTerrainRepository terrRepository;
	
	
	
	
	public TerrainDTO findTerrainById(int terrId) {
		PersistenceTerrain terr = terrRepository.findById(terrId);
		if (terr == null) {
			throw new ResourceNotFoundException(PersistenceTerrain.class.getSimpleName());
		}

		TerrainDTO dto = new TerrainDTO.Builder()
						.id(terr.getId())
						.area(terr.getArea())
						.pnc(terr.getPnc())
						.place(terr.getPlace())
						.type(terr.getType())
						.price(terr.getPrice())
						.create();
		return dto;
	}
	
	public int create(TerrainDTO terrDTO) {
		

		PersistenceTerrain terrain = new PersistenceTerrain();
		//terrain.setId(terrainDTO.getId());
		
		terrain.setArea(terrDTO.getArea());
		//if(terrDTO.getOwnerID()!=null)
		terrain.setPnc(terrDTO.getPnc());
		terrain.setPlace(terrDTO.getPlace());
		terrain.setType(terrDTO.getType());
		


		PersistenceTerrain terr = terrRepository.save(terrain);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		return terr.getId();
	}
	public List<TerrainDTO> findAll() {
		List<PersistenceTerrain> terrains = terrRepository.findAll();
		List<TerrainDTO> toReturn = new ArrayList<TerrainDTO>();
		for (PersistenceTerrain terrain : terrains) {
			
			/*TerrainDTO dto = new TerrainDTO.Builder()
						.id(terrain.getId())
						.area(terrain.getArea())
						.place(terrain.getPlace())
						.create();*/
			toReturn.add(terrainToDTOBrief(terrain));
		}
		return toReturn;
	}
	public static TerrainDTO terrainToDTOBrief(PersistenceTerrain terrain)
	{
		TerrainDTO dto = new TerrainDTO.Builder()
				.id(terrain.getId())
				.area(terrain.getArea())
				.place(terrain.getPlace())
				.type(terrain.getType())
				.pnc(terrain.getPnc())
				.price(terrain.getPrice())
				.create();
		return dto;
	}
	public List<TerrainDTO> findTerrainByType(String type) {
		List<PersistenceTerrain> terrains = terrRepository.findByType(type);
		List<TerrainDTO> toReturn = new ArrayList<TerrainDTO>();
		for (PersistenceTerrain terrain : terrains) {
			
			toReturn.add(terrainToDTOBrief(terrain));
		}
		return toReturn;
	}
	
	
	
	
}
