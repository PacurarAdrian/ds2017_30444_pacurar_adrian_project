package terrain.manager.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.dto.TerrainDTO;
import terrain.manager.entities.Terrain;
import terrain.manager.entities.Type;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.repositories.TerrainRepository;
import terrain.manager.repositories.TypeRepository;

@Service
public class TerrainService {
	//public static final List<String> types=Arrays.asList("Recreational","transport","agricultural","residential","commercial");
	
	@Autowired
	private TerrainRepository terrRepository;
	@Autowired
	private TypeRepository terrTypeRepository;
	
	public static double computePrice(Terrain terr)
	{
		Type terrType = terr.getType();
		return terr.getArea()*terrType.getPrice();
				
	}
	
	//=======Terrain type operations===================================================
	public 	List<String> getStringTypes(){
		List<Type> terrains = terrTypeRepository.findAll();
		List<String> toReturn=new ArrayList<String>();
		for(Type terr:terrains)
		{
			toReturn.add(terr.getName());
		}
		return toReturn;
	}
	public 	List<Type> getTerrainTypes(){
		List<Type> terrains = terrTypeRepository.findAll();
		
		return terrains;
	}
	public void deleteTerrainType(int terrainId){
		try{
		terrTypeRepository.delete(terrainId);
		}
		catch(IllegalArgumentException e){
			throw new ResourceNotFoundException(Type.class.getSimpleName());
		}
		
	}
	public Type findTerrainTypeById(int terrId) {
		Type terr = terrTypeRepository.findById(terrId);
		if (terr == null) {
			throw new ResourceNotFoundException(Type.class.getSimpleName());
		}

		
		return terr;
	}
	public int createType(Type terrDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateTerrainType(terrDTO,true);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Terrain.class.getSimpleName(),validationErrors);
		}
		Type terr = terrTypeRepository.save(terrDTO);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		return terr.getId();
	}
	public void updateType(Type terrDTO) {
		//System.out.println("intra");
		
		if ((terrTypeRepository.findById(terrDTO.getId())) == null) {
			throw new ResourceNotFoundException(Terrain.class.getSimpleName());
		}else{
		List<String> validationErrors = validateTerrainType(terrDTO,false);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Type.class.getSimpleName(),validationErrors);
		}
		Type terr = terrTypeRepository.save(terrDTO);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		}
	}
	
	//-------Terrain operations-----------------======================================
	
	public void deleteTerrain(int terrainId){
		try{
		terrRepository.delete(terrainId);
		}
		catch(IllegalArgumentException e){
			throw new ResourceNotFoundException(Terrain.class.getSimpleName());
		}
		
	}
	public TerrainDTO findTerrainById(int terrId) {
		Terrain terr = terrRepository.findById(terrId);
		if (terr == null) {
			throw new ResourceNotFoundException(Terrain.class.getSimpleName());
		}

		TerrainDTO dto = new TerrainDTO.Builder()
						.id(terr.getId())
						.area(terr.getArea())
						.ownerID(terr.getOwnerID())
						.place(terr.getPlace())
						.type(terr.getType().getName())
						.price(computePrice(terr))
						.create();
		return dto;
	}
	
	public int create(TerrainDTO terrDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateTerrain(terrDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Terrain.class.getSimpleName(),validationErrors);
		}

		Terrain terrain = new Terrain();
		//terrain.setId(terrainDTO.getId());
		
		terrain.setArea(terrDTO.getArea());
		//if(terrDTO.getOwnerID()!=null)
			terrain.setOwnerID(terrDTO.getOwnerID());
		terrain.setPlace(terrDTO.getPlace());
		terrain.setType(terrTypeRepository.findByName(terrDTO.getType()));
		


		Terrain terr = terrRepository.save(terrain);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		return terr.getId();
	}
	public List<TerrainDTO> findAll() {
		List<Terrain> terrains = terrRepository.findAll();
		List<TerrainDTO> toReturn = new ArrayList<TerrainDTO>();
		for (Terrain terrain : terrains) {
			
			/*TerrainDTO dto = new TerrainDTO.Builder()
						.id(terrain.getId())
						.area(terrain.getArea())
						.place(terrain.getPlace())
						.create();*/
			toReturn.add(terrainToDTOBrief(terrain));
		}
		return toReturn;
	}
	public static TerrainDTO terrainToDTOBrief(Terrain terrain)
	{
		TerrainDTO dto = new TerrainDTO.Builder()
				.id(terrain.getId())
				.area(terrain.getArea())
				.place(terrain.getPlace())
				.type(terrain.getType().getName())
				.ownerID(terrain.getOwnerID())
				.price(TerrainService.computePrice(terrain))
				.create();
		return dto;
	}
	public List<TerrainDTO> findTerrainByType(String type) {
		List<Terrain> terrains = terrRepository.findByType(terrTypeRepository.findByName(type));
		List<TerrainDTO> toReturn = new ArrayList<TerrainDTO>();
		for (Terrain terrain : terrains) {
			
			toReturn.add(terrainToDTOBrief(terrain));
		}
		return toReturn;
	}
	public void update(TerrainDTO terrDTO) {
		//System.out.println("intra");
		Terrain terrPrev;
		if ((terrPrev=terrRepository.findById(terrDTO.getId())) == null) {
			throw new ResourceNotFoundException(Terrain.class.getSimpleName());
		}else{
		List<String> validationErrors = validateTerrain(terrDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Terrain.class.getSimpleName(),validationErrors);
		}

		Terrain terrain = new Terrain();
		//terrain.setId(terrainDTO.getId());
		terrain.setId(terrDTO.getId());
		terrain.setArea(terrDTO.getArea());
		terrain.setOwnerID(terrPrev.getOwnerID());
		terrain.setPlace(terrDTO.getPlace());
		terrain.setType(terrTypeRepository.findByName(terrDTO.getType()));
		


		Terrain terr = terrRepository.save(terrain);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		}
	}

	public void updateOwner(TerrainDTO terrDTO) {
		//System.out.println("intra");
		if (terrRepository.findById(terrDTO.getId()) == null) {
			throw new ResourceNotFoundException(Terrain.class.getSimpleName());
		}else{
		List<String> validationErrors = validateTerrain(terrDTO);
		if (terrDTO.getOwnerID()!=null) {
			validationErrors.add("OwnerID invalid");
			throw new EntityValidationException(Terrain.class.getSimpleName(),validationErrors);
		}

		Terrain terrain = new Terrain();
		//terrain.setId(terrainDTO.getId());
		//terrain.setId(terrDTO.getId());
		//terrain.setArea(terrDTO.getArea());
		terrain.setOwnerID(terrDTO.getOwnerID());
		//terrain.setPlace(terrDTO.getPlace());
		//terrain.setType(terrDTO.getType());
		


		Terrain terr = terrRepository.save(terrain);
		//terrainDTO.setId(terrain.getId());
		System.out.println(terr.getId());
		}
	}
	private List<String> validateTerrain(TerrainDTO terrain) {
		
		List<String> validationErrors = new ArrayList<String>();
		
		if (terrain.getArea() <=0 || "".equals(terrain.getArea())) {
			validationErrors.add("Invalid area");
		}
		if (terrain.getPlace() == null || "".equals(terrain.getPlace())) {
			validationErrors.add("Place field should not be empty");
		}
		if (terrain.getType() == null || "".equals(terrain.getType())|| terrTypeRepository.findByName(terrain.getType())==null) {
			validationErrors.add("Type is incorect");
		}
		return validationErrors;
	}
	private List<String> validateTerrainType(Type terrain,boolean notUpdate) {
		
		List<String> validationErrors = new ArrayList<String>();
		
		if (terrain.getPrice() <=0 || "".equals(terrain.getPrice())) {
			validationErrors.add("Invalid Price");
		}
		if (terrain.getName() == null || "".equals(terrain.getName())) {
			validationErrors.add("Name field should not be empty");
		}
		if (notUpdate&&terrTypeRepository.findByName(terrain.getName())!=null) {
			validationErrors.add("Type is already present");
		}
		return validationErrors;
	}
}
