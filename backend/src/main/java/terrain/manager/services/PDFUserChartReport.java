package terrain.manager.services;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import terrain.manager.dto.HistoryDTO;

public class PDFUserChartReport implements Report{
	
	
	
	@Override
	public HttpServletResponse download(HttpServletResponse response,List<HistoryDTO> listBooks) throws IOException {
		//final ServletContext servletContext = request.getSession().getServletContext();
	      // final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	      // final String temperotyFilePath = tempDirectory.getAbsolutePath();
	    
	       String fileName = "OperationHistoryGraph.pdf";
	       response.setContentType("application/pdf");
	       response.setHeader("Content-disposition", "attachment; filename="+ fileName);
	    
	       try {
	    
	           //CreatePDF.createPDF(fileName,listBooks);
	    	   PDFChartService.writeChartToPDF(PDFChartService.generateBarChart(listBooks), 500, 400, fileName);
	           ByteArrayOutputStream baos = new ByteArrayOutputStream();
	           baos = PDFReport.convertPDFToByteArrayOutputStream(fileName);
	           OutputStream os = response.getOutputStream();
	           baos.writeTo(os);
	           os.flush();
	       } catch (IOException e1) {
	           e1.printStackTrace();
	           throw new IOException("PDF download generation failed!");
	       }
		return response;
	}
	
	 
		
	
	
}
