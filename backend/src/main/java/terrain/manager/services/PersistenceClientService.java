package terrain.manager.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.dto.ClientDTO;
import terrain.manager.entities.PersistenceClient;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.repositories.PersistenceClientRepository;

@Service
public class PersistenceClientService {
	@Autowired
	private PersistenceClientRepository infoRepository;
	
	public List<ClientDTO> findClientByType(String type) {
		List<PersistenceClient> users = infoRepository.findByType(type);
		List<ClientDTO> toReturn = new ArrayList<ClientDTO>();
		for (PersistenceClient user : users) {
			
			
			toReturn.add(clientToDTOBrief(user));
		}
		return toReturn;
	}
	
	public ClientDTO findClientInformationById(int accId) {
		PersistenceClient acc = infoRepository.findById(accId);
		if (acc == null) {
			throw new ResourceNotFoundException(PersistenceClient.class.getSimpleName());
		}
		

		ClientDTO dto = new ClientDTO.Builder()
						.address(acc.getAddress())
						.ICN(acc.getICN())
						.PNC(acc.getPnc())
						.id(acc.getId())
						.name(acc.getName())
						.type(acc.getType())
						.amount(acc.getAmount())
						.email(acc.getEmail())
						.create();
		return dto;
	}
	public int findClientIdByPNC(String pnc)
	{
		
		PersistenceClient info=infoRepository.findByPnc(pnc);
		if (info == null) {
			throw new ResourceNotFoundException(PersistenceClient.class.getSimpleName());
		}
		
		
		return info.getId();
		
	}
	public ClientDTO findClientAccountById(int infoId) {
		PersistenceClient info = infoRepository.findById(infoId);
		if (info == null) {
			throw new ResourceNotFoundException(PersistenceClient.class.getSimpleName());
		}
		

		ClientDTO dto = new ClientDTO.Builder()
						.creationDate(info.getCreationDate())
						.email(info.getEmail())
						.id(info.getId())
						.amount(info.getAmount())
						.type(info.getType())
						.create();
		
		
		return dto;
	}
	
	public List<ClientDTO> findAll() {
		
		List<PersistenceClient> accs=infoRepository.findAll();
		
		List<ClientDTO> toReturn = new ArrayList<ClientDTO>();
		for (PersistenceClient acc : accs) {
			
			/*ClientDTO dto = new ClientDTO.Builder()
						.id(acc.getInfo().getId())
						.name(acc.getInfo().getName())
						.PNC(acc.getInfo().getPNC())
						.creationDate(acc.getCreationDate())
						.username(acc.getUsername())
						.create();*/
			toReturn.add(clientToDTOBrief(acc));
		}
		return toReturn;
	}
	public static ClientDTO clientToDTOBrief(PersistenceClient usr)
	{
		ClientDTO dto = new ClientDTO.Builder()
				.id(usr.getId())
				.name(usr.getName())
				.address(usr.getAddress())
				.email(usr.getEmail())
				.PNC(usr.getPnc())
				.ICN(usr.getICN())
				.creationDate(usr.getCreationDate())
				.type(usr.getType())
				.amount(usr.getAmount())
				.create();
				
		
		return dto;
	}
	public ClientDTO findClientById(int id)
	{
		PersistenceClient usr =infoRepository.findById(id);
		if (usr == null) {
			throw new ResourceNotFoundException(PersistenceClient.class.getSimpleName());
		}
		return new ClientDTO.Builder()
		.id(usr.getId())
		.name(usr.getName())
		.address(usr.getAddress())
		.email(usr.getEmail())
		.PNC(usr.getPnc())
		.ICN(usr.getICN())
		.creationDate(usr.getCreationDate())
		.type(usr.getType())
		.amount(usr.getAmount())
		.create();
			
		
		
		
	}
	public int create(ClientDTO clientDTO) {
		//System.out.println("intra");
		

		PersistenceClient info = new PersistenceClient();
		//user.setId(userDTO.getId());
		
		info.setAddress(clientDTO.getAddress());
		info.setICN(clientDTO.getICN());
		info.setName(clientDTO.getName());
		info.setPnc(clientDTO.getPNC());
	
		info.setEmail(clientDTO.getEmail());
		info.setAmount(clientDTO.getAmount());
		info.setCreationDate(new Date());
		
		info.setType(clientDTO.getType());
	
		PersistenceClient account;
		try{
			account=infoRepository.save(info);
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new EntityValidationException(PersistenceClient.class.getSimpleName(),
					Arrays.asList("Could not save to database"));
		}
		
		return account.getId();
	}
	
	
	
	
	



}
