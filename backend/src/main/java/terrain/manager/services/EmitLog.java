package terrain.manager.services;

import com.rabbitmq.client.ConnectionFactory;

import terrain.manager.entities.*;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class EmitLog {

  private static final String EXCHANGE_NAME = "logs";

  public static void emitLogs(Notification dvd) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

    String message =null;//;= getMessage(argv);
    Content<?> dvdcontent= new Content<>(dvd);
    
    channel.basicPublish(EXCHANGE_NAME, "", null, dvdcontent.getBytes());
    message=dvdcontent.toString();
    System.out.println(" [x] Sent '" + message + "'");

    channel.close();
    connection.close();
  }
}