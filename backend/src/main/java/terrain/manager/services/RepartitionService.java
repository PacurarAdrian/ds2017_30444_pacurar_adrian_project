package terrain.manager.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.services.EmitLog;
import terrain.manager.dto.ClientDTO;
import terrain.manager.dto.RepartitionDTO;
import terrain.manager.dto.RepartitionLightDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.entities.*;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.errorhandler.ServiceNotFoundException;
import terrain.manager.repositories.ClientAccountRepository;
import terrain.manager.repositories.RepartitionRepository;
import terrain.manager.repositories.TerrainRepository;

@Service
public class RepartitionService {

	@Autowired
	private RepartitionRepository repRepository;
	
	@Autowired
	private TerrainRepository terrRepository;
	@Autowired
	private ClientAccountRepository accRepository;
	
	
	//private MailService mailService = new MailService("ds.laboratory1@gmail.com","ro.tuc.dsrl.ds.handsonbdv");
	
	public List<RepartitionDTO> findRepartitionByDates(Date start,Date end) {
		
		List<Repartition> hists = repRepository.findByPurchaseDate(start,end);
		List<RepartitionDTO> toReturn = new ArrayList<RepartitionDTO>();
		for (Repartition rep : hists) {
			Terrain terrain=rep.getTerrain();
			TerrainDTO terrDTO = new TerrainDTO.Builder()
					.id(terrain.getId())
					.area(terrain.getArea())
					.place(terrain.getPlace())
					.price(TerrainService.computePrice(terrain))
					.create();
			ClientAccount acc=rep.getClient();
			ClientDTO accDTO = new ClientDTO.Builder()
							.id(acc.getId())
							.name(acc.getInfo().getName())
							.PNC(acc.getInfo().getPnc())
							.creationDate(acc.getCreationDate())
							.username(acc.getUsername())
							.create();
			RepartitionDTO dto = new RepartitionDTO.Builder()
						.id(rep.getId())
						.terrain(terrDTO)
						.client(accDTO)
						.purchaseDate(rep.getPurchaseDate())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public List<RepartitionDTO> findRepartitionByDate(Date start) {
		Date end = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(start); 
		c.add(Calendar.DATE, 1);
		end = c.getTime();
		List<Repartition> hists = repRepository.findByPurchaseDate(start,end);
		List<RepartitionDTO> toReturn = new ArrayList<RepartitionDTO>();
		for (Repartition rep : hists) {
			Terrain terrain=rep.getTerrain();
			TerrainDTO terrDTO = new TerrainDTO.Builder()
					.id(terrain.getId())
					.area(terrain.getArea())
					.place(terrain.getPlace())
					.price(TerrainService.computePrice(terrain))
					.create();
			ClientAccount acc=rep.getClient();
			ClientDTO accDTO = new ClientDTO.Builder()
							.id(acc.getId())
							.name(acc.getInfo().getName())
							.PNC(acc.getInfo().getPnc())
							.creationDate(acc.getCreationDate())
							.username(acc.getUsername())
							.create();
			RepartitionDTO dto = new RepartitionDTO.Builder()
						.id(rep.getId())
						.terrain(terrDTO)
						.client(accDTO)
						.purchaseDate(rep.getPurchaseDate())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public void deleteRepartition(int repId){
		Repartition rep;
		
		rep = repRepository.findById(repId);
		if (rep == null) {
			throw new ResourceNotFoundException(Repartition.class.getSimpleName());
		}
		ClientAccount info = accRepository.findById(rep.getClient().getId());
		Terrain terr = terrRepository.findById(rep.getTerrain().getId());
		
		
		try{//set terrain as available
			terr.setOwnerID(null);
			terrRepository.save(terr);
		}catch(Exception e)
		{
			throw new EntityValidationException(Terrain.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		double price=TerrainService.computePrice(terr);
		
		try{//give previous owner credits for the terrain
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
		}catch(Exception e)
		{
			//roll back terrain modifications
			terr.setOwnerID(info.getId());
			terrRepository.save(terr);
			throw new EntityValidationException(ClientAccount.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		try{
			repRepository.delete(repId);
		}catch(Exception e)
		{
			//roll back terrain modifications
			terr.setOwnerID(info.getId());
			terrRepository.save(terr);
			//roll back credit transaction
			info.setAmount(info.getAmount()-price);
			info=accRepository.save(info);
			
			throw new EntityValidationException(Repartition.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		Notification dvd=new Notification("You have sold a land title for "+price+"!\n"+terr.toString(),info.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}	
		
	}
	public List<TerrainDTO> findTerrainsByClientId(int clientId)
	{
		List<Repartition> rep = repRepository.findByClientId(clientId);
		if (rep == null) {
			throw new ResourceNotFoundException(Repartition.class.getSimpleName());
		}
		List<TerrainDTO> toReturn = new ArrayList<TerrainDTO>();
		for(Repartition r:rep)
		{
			toReturn.add(TerrainService.terrainToDTOBrief(r.getTerrain()));
		}
		
		
		return toReturn;
	}
	public RepartitionDTO findRepartitionById(int repId) {
		Repartition rep = repRepository.findById(repId);
		if (rep == null) {
			throw new ResourceNotFoundException(Repartition.class.getSimpleName());
		}
		Terrain terr=rep.getTerrain();
		TerrainDTO terrDTO = new TerrainDTO.Builder()
				.id(terr.getId())
				.area(terr.getArea())
				.ownerID(terr.getOwnerID())
				.place(terr.getPlace())
				.type(terr.getType().getName())
				.price(TerrainService.computePrice(terr))
				.create();
		ClientAccount user=rep.getClient();
		ClientDTO clientDTO = new ClientDTO.Builder()
				.id(user.getId())
				.type(user.getType())
				.name(user.getInfo().getName())
				.PNC(user.getInfo().getPnc())
				.ICN(user.getInfo().getICN())
				.create();


		RepartitionDTO dto = new RepartitionDTO.Builder()
						.id(rep.getId())
						.terrain(terrDTO)
						.purchaseDate(rep.getPurchaseDate())
						.client(clientDTO)
						.create();
		return dto;
	}
	
	public List<RepartitionDTO> findAll() {
		List<Repartition> reps = repRepository.findAll();
		List<RepartitionDTO> toReturn = new ArrayList<RepartitionDTO>();
		for (Repartition rep : reps) {
			Terrain terrain=rep.getTerrain();
			TerrainDTO terrDTO = new TerrainDTO.Builder()
					.id(terrain.getId())
					.area(terrain.getArea())
					.place(terrain.getPlace())
					.price(TerrainService.computePrice(terrain))
					.create();
			ClientAccount acc=rep.getClient();
			ClientDTO accDTO = new ClientDTO.Builder()
							.id(acc.getId())
							.name(acc.getInfo().getName())
							.PNC(acc.getInfo().getPnc())
							.creationDate(acc.getCreationDate())
							.username(acc.getUsername())
							.create();
			RepartitionDTO dto = new RepartitionDTO.Builder()
						.id(rep.getId())
						.terrain(terrDTO)
						.client(accDTO)
						.purchaseDate(rep.getPurchaseDate())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int createLight(RepartitionLightDTO repDTO) {
		//System.out.println("intra");
		List<String> validationErrors =validateRepartitionLight(repDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Repartition.class.getSimpleName(),validationErrors);
		}

		Repartition rep = new Repartition();
		//rep.setId(repDTO.getId());
				
		ClientAccount info = accRepository.findById(repDTO.getClient());
		Terrain terr = terrRepository.findById(repDTO.getTerrain());
		
		double price;
		if((price=TerrainService.computePrice(terr))>info.getAmount())
			throw new EntityValidationException(Repartition.class.getSimpleName(),
					Arrays.asList("Client does not have enough money"));
		//System.out.println("Terrain costed"+price);
		
		try{//subtract price from client credits
			info.setAmount(info.getAmount()-price);
			info=accRepository.save(info);
		}catch(Exception e)
		{
			throw new EntityValidationException(ClientAccount.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		
		//System.out.println("client account after buying:"+info);		
		Terrain terrs;
		try{//update owner in terrain information
			terr.setOwnerID(info.getId());
			terrs=terrRepository.save(terr);
		}catch(Exception e)
		{
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			
			throw new EntityValidationException(Terrain.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		
		
		
		Repartition usr;
		
		try{//save repartition to database
			rep.setClient(info);
			rep.setTerrain(terrs);
			rep.setPurchaseDate(new Date());
			usr = repRepository.save(rep);
			System.out.println(usr.getId());
		}catch(Exception e)
		{
			
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			
			throw new EntityValidationException(Repartition.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		Notification dvd=new Notification("You have bought a new land title!\n"+terr.toString(),info.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
        
		return usr.getId();
	}
	
	
	private List<String> validateRepartitionLight(RepartitionLightDTO histDTO) {
		List<String> validationErrors = new ArrayList<String>();

		if (terrRepository.findById(histDTO.getTerrain())==null) {
			validationErrors.add("The requested terrain could not be found");
		}
		if (accRepository.findById(histDTO.getClient())==null) {
			validationErrors.add("The requested account could not be found");
		}
		
		return validationErrors;
	}
	
	public void updateLight(RepartitionLightDTO repDTO) {
		//System.out.println("intra");
		Repartition rep=repRepository.findById(repDTO.getId());
		if (rep == null) 
			throw new ResourceNotFoundException(Repartition.class.getSimpleName());
		
		List<String> validationErrors = validateRepartitionLight(repDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(Repartition.class.getSimpleName(),validationErrors);
		}

		
		//rep.setId(repDTO.getId());
		ClientAccount info = accRepository.findById(repDTO.getClient());
		Terrain terr = terrRepository.findById(repDTO.getTerrain());
		double price;
		if((price=TerrainService.computePrice(terr))>info.getAmount())
			throw new EntityValidationException(Repartition.class.getSimpleName(),
					Arrays.asList("Client does not have enough money"));

		
		try{//subtract price from client credits
			info.setAmount(info.getAmount()-price);
			info=accRepository.save(info);
		}catch(Exception e)
		{
			throw new EntityValidationException(ClientAccount.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		
		ClientAccount prevOwner;
		try{//get previous owner
			prevOwner=accRepository.findById(terr.getOwnerID());
		}catch(Exception e)
		{
			//set terrain owner
			prevOwner=rep.getClient();
			terr.setOwnerID(prevOwner.getId());
			terrRepository.save(terr);
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			throw new ResourceNotFoundException(ClientAccount.class.getSimpleName());
		}
		try{//increase amount of credits of previous owner
			
			prevOwner.setAmount(prevOwner.getAmount()+price);
			prevOwner=accRepository.save(prevOwner);
		}catch(Exception e)
		{
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			throw new EntityValidationException(ClientAccount.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		Terrain terrs;
		try{//update owner in terrain information
			terr.setOwnerID(info.getId());
			terrs=terrRepository.save(terr);
		}catch(Exception e)
		{
			//roll back previous owner credits
			prevOwner.setAmount(prevOwner.getAmount()-price);
			prevOwner=accRepository.save(prevOwner);
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			
			throw new EntityValidationException(Terrain.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		
		
		try{//save repartition to database
			rep.setClient(info);
			rep.setTerrain(terrs);
			rep.setPurchaseDate(new Date());
			Repartition usr = repRepository.save(rep);
			System.out.println(usr.getId());
		}catch(Exception e)
		{
			//roll back terrain updating
			terr.setOwnerID(prevOwner.getId());
			terrs=terrRepository.save(terr);
			//roll back price subtraction
			info.setAmount(info.getAmount()+price);
			info=accRepository.save(info);
			//roll back previous owner credits
			prevOwner.setAmount(prevOwner.getAmount()-price);
			prevOwner=accRepository.save(prevOwner);
			throw new EntityValidationException(Repartition.class.getSimpleName(),
					Arrays.asList("Problem occured while saving entity to database"));
		}
		
		
		Notification dvd=new Notification("You have bought a new land title!\n"+terr.toString(),info.getEmail());
		Notification dvd2=new Notification("You have sold a land title!\n"+terr.toString(),prevOwner.getEmail());
        try {
			EmitLog.emitLogs(dvd);
			EmitLog.emitLogs(dvd2);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
		
		
	}
}
