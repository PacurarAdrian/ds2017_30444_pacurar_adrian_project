package terrain.manager.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.errorhandler.ServiceNotFoundException;
import terrain.manager.dto.LoginDTO;
import terrain.manager.dto.UserDTO;
import terrain.manager.entities.*;
import terrain.manager.entities.User;
import terrain.manager.repositories.UserRepository;

@Service
public class UserService {
	private static final String SPLIT_CH = " ";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	public static final List<String> types=Arrays.asList("regular","employee","admin");
	//private MailService mailService = new MailService("ds.laboratory1@gmail.com","ro.tuc.dsrl.ds.handsonbdv");
	
	@Autowired
	private UserRepository usrRepository;

	public 	List<String> getTypes(){
		return types;
	}
	
	public List<UserDTO> findUserByType(String type) {
		List<User> users = usrRepository.findByType(type);
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			String[] names = extractNames(user.getName());
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.telephone(user.getTelephone())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public void deleteUser(int userId){
		try{
			User usr = usrRepository.findById(userId);
			usrRepository.delete(userId);
		
			//mailService.sendMail(usr.getEmail(),"Your account on Land title manager was deleted!",usr.toString());
			Notification dvd=new Notification("Your account on Land title manager was deleted!\n"+usr.toString(),usr.getEmail());	
	        try {
				EmitLog.emitLogs(dvd);
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new ServiceNotFoundException(Notification.class.getSimpleName());
			}
		}
		catch(Exception e){
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		
	}
	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		String[] names = extractNames(usr.getName());

		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.firstname(names[0])
						.surname(names[1])
						.city(usr.getCity())
						.address(usr.getAddress())
						.email(usr.getEmail())
						.telephone(usr.getTelephone())
						.country(usr.getCountry())
						.postcode(usr.getPostcode())
						.username(usr.getUsername())
						.password(usr.getPassword())
						.type(usr.getType())
						.salary(usr.getSalary())
						.create();
		return dto;
	}
	public UserDTO login(LoginDTO credentials)
	{
		List<User> users = usrRepository.findAll();
		
		for(User usr:users)
			if(usr.getUsername().equalsIgnoreCase(credentials.getUsername()))
			{
				String[] names = extractNames(usr.getName());
				return new UserDTO.Builder()
				.id(usr.getId())
				.firstname(names[0])
				.surname(names[1])
				.city(usr.getCity())
				.address(usr.getAddress())
				.email(usr.getEmail())
				.telephone(usr.getTelephone())
				.country(usr.getCountry())
				.postcode(usr.getPostcode())
				.username(usr.getUsername())
				.password(usr.getPassword())
				.type(usr.getType())
				.salary(usr.getSalary())
				.create();
			}
		
		
		return null;
	}
	
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			String[] names = extractNames(user.getName());
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.telephone(user.getTelephone())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public static UserDTO userToDTOBrief(User user)
	{
		String fullname=user.getName();
		
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		
		UserDTO dto = new UserDTO.Builder()
				.id(user.getId())
				.firstname(names[0])
				.surname(names[1])
				.telephone(user.getTelephone())
				.create();
		return dto;
	}
	public int create(UserDTO userDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			System.out.println(validationErrors);
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		//user.setId(userDTO.getId());
		user.setName(userDTO.getFirstname().trim() + SPLIT_CH + userDTO.getSurname().trim());
		user.setEmail(userDTO.getEmail());
		user.setAddress(userDTO.getAddress());
		user.setPostcode(userDTO.getPostcode());
		user.setCity(userDTO.getCity());
		user.setCountry(userDTO.getCountry());
		user.setTelephone(userDTO.getTelephone());
		user.setSalary(userDTO.getSalary());
		user.setCreated(new Date());
		user.setPassword(userDTO.getPassword());
		user.setUsername(userDTO.getUsername());
		user.setType(userDTO.getType());
		

		//mailService.sendMail(user.getEmail(),"Your account on Land title manager was created!",user.toString());
		User usr = usrRepository.save(user);
		Notification dvd=new Notification("Your account on Land title manager was created!\n"+usr.toString(),usr.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
		//userDTO.setId(user.getId());
		System.out.println(usr.getId());
		return usr.getId();
	}
	
	public void update(UserDTO userDTO) {
		//System.out.println("intra");
		User user =usrRepository.findById(userDTO.getId());
		if (user == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}else{
			List<String> validationErrors = validateUser(userDTO);
			if (!validationErrors.isEmpty()) {
				throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
			}

			
			//user.setId(userDTO.getId());
			user.setName(userDTO.getFirstname().trim() + SPLIT_CH + userDTO.getSurname().trim());
			user.setEmail(userDTO.getEmail());
			user.setAddress(userDTO.getAddress());
			user.setPostcode(userDTO.getPostcode());
			user.setCity(userDTO.getCity());
			user.setCountry(userDTO.getCountry());
			user.setTelephone(userDTO.getTelephone());
			user.setSalary(userDTO.getSalary());
			user.setCreated(new Date());
			user.setPassword(userDTO.getPassword());
			user.setUsername(userDTO.getUsername());
			user.setType(userDTO.getType());
			
		
			//mailService.sendMail(user.getEmail(),"Your account on Land title manager was updated",user.toString());
			Notification dvd=new Notification("Your account on Land title manager was updated!\n"+user.toString(),user.getEmail());	
	        try {
				EmitLog.emitLogs(dvd);
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new ServiceNotFoundException(Notification.class.getSimpleName());
			}
			User usr = usrRepository.save(user);
			//userDTO.setId(user.getId());
			System.out.println(usr.getId());
		
		
		}
		
	}

	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getFirstname() == null || "".equals(usr.getFirstname())) {
			validationErrors.add("First Name field should not be empty");
		}

		if (usr.getSurname() == null || "".equals(usr.getSurname())) {
			validationErrors.add("Surname field should not be empty");
		}

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}
		if (usr.getPassword() == null || !validatePassword(usr.getPassword())) {
			validationErrors.add("Password field should verify the following: \n"+
					"*password should be 4 or more alphanumeric characters long\n"+
					"* at least 1 number\n"+
					"* at least 1 lower case letter\n"+
					"* at least 1 upper case letter\n"+
					"* no special characters allowed.");
		}
		if (usr.getUsername() == null || "".equals(usr.getUsername())) {
			validationErrors.add("Username field should not be empty.");
		}
		if (usr.getSalary() <=0 || "".equals(usr.getSalary())) {
			validationErrors.add("Invalid salary.");
		}
		if (usr.getType() ==null || "".equals(usr.getType())||!types.contains(usr.getType())) {
			validationErrors.add("Invalid user type.");
		}
		return validationErrors;
	}
	
	/*
	 * password should be 4 or more alphanumeric characters long
	 * at least 1 number
	 * at least 1 lower case letter
	 * at least 1 upper case letter
	 * no special characters allowed
	 */
	private boolean validatePassword(String password) {
		Pattern p = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{4,}$");
		//Pattern p1=Pattern.compile("[!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]*");
		//Pattern p2=Pattern.compile("\\d*");
		 Matcher m = p.matcher(password);
		 boolean b = m.matches();
		//m=p2.matcher(password);
		//b=b&&m.matches();
		//m=p1.matcher(password);
		//b=b&&m.matches();
		
		
		
		return b;
	}
	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}
}
