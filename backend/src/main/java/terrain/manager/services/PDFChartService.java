package terrain.manager.services;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.DefaultFontMapper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import terrain.manager.dto.HistoryDTO;

public class PDFChartService{
	
	
	
	
	
	 
	public static JFreeChart generatePieChart() {
		DefaultPieDataset dataSet = new DefaultPieDataset();
		dataSet.setValue("China", 19.64);
		dataSet.setValue("India", 17.3);
		dataSet.setValue("United States", 4.54);
		dataSet.setValue("Indonesia", 3.4);
		dataSet.setValue("Brazil", 2.83);
		dataSet.setValue("Pakistan", 2.48);
		dataSet.setValue("Bangladesh", 2.38);

		JFreeChart chart = ChartFactory.createPieChart(
				"World Population by countries", dataSet, true, true, false);

		return chart;
	}
	 
	public static JFreeChart generateBarChart(List<HistoryDTO> data) {
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		Map<String,Integer> names=new HashMap<String,Integer>();
		for(HistoryDTO o:data)
		{
			String name=o.getUser().getFirstname()+" "+o.getUser().getSurname();
			if(!names.containsKey(name))
				names.put(name,1);
			else
			{
				int v=names.get(name);
				names.replace(name, v, v+1);
			}
		}
		Iterator<Entry<String, Integer>> it = names.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String,Integer> pair = (Entry<String, Integer>)it.next();
	       // System.out.println(pair.getKey() + " = " + pair.getValue());
	        dataSet.setValue(pair.getValue(), "Operations" ,pair.getKey());
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
		
		/*dataSet.setValue(791, "Population", "1750 AD");
		dataSet.setValue(978, "Population", "1800 AD");
		dataSet.setValue(1262, "Population", "1850 AD");
		dataSet.setValue(1650, "Population", "1900 AD");
		dataSet.setValue(2519, "Population", "1950 AD");
		dataSet.setValue(6070, "Population", "2000 AD");*/

		JFreeChart chart = ChartFactory.createBarChart(
				"Operation per employee", "employee", "operations",
				dataSet, PlotOrientation.VERTICAL, false, true, false);

		return chart;
	}
	public static JFreeChart generateChartByDate(List<HistoryDTO> data) {
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		
		Map<Calendar,Map<String,Integer>> dates=new HashMap<Calendar,Map<String,Integer>>();
		for(HistoryDTO o:data)
		{
			
			Calendar c=new GregorianCalendar();
			c.clear();
			c.setTime(o.getDate());
			
			c.clear(Calendar.SECOND);
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.clear(Calendar.MINUTE);
			c.clear(Calendar.MILLISECOND);
			String name=o.getUser().getFirstname()+" "+o.getUser().getSurname();
			if(!dates.containsKey(c))
			{
				Map<String,Integer> temp=new HashMap<String,Integer>();
				temp.put(name, 1);
				dates.put(c,temp);
			}
			else
			{
				int val;
				Map<String,Integer> v=dates.get(c);
				if(v.get(name)==null)
				{
				
					v.put(name, 1);
					dates.put(c,v);
				}
				else
				 {
					val=v.get(name);
					v.replace(name,val, val+1);
				
					dates.replace(c, v);
				 }
			}
		}
		Iterator<Entry<Calendar, Map<String, Integer>>> it = dates.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry<Calendar, Map<String, Integer>> pair = (Entry<Calendar, Map<String, Integer>>)it.next();
	       // System.out.println(pair.getKey() + " = " + pair.getValue());
	        
	        Iterator<Entry<String, Integer>> it2 = pair.getValue().entrySet().iterator();
		    while (it2.hasNext()) {
		        Map.Entry<String,Integer> pair2 = (Entry<String, Integer>)it2.next();
		       // System.out.println(pair.getKey() + " = " + pair.getValue());
		        dataSet.setValue(pair2.getValue(),pair2.getKey(), pair.getKey().getTime() );
		       // it.remove(); // avoids a ConcurrentModificationException
		    }
	        
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
		
		/*dataSet.setValue(791, "Population", "1750 AD");
		dataSet.setValue(978, "Population", "1800 AD");
		dataSet.setValue(1262, "Population", "1850 AD");
		dataSet.setValue(1650, "Population", "1900 AD");
		dataSet.setValue(2519, "Population", "1950 AD");
		dataSet.setValue(6070, "Population", "2000 AD");*/

		JFreeChart chart = ChartFactory.createBarChart(
				"Operation per day,per employee", "date", "operations",
				dataSet, PlotOrientation.VERTICAL, true, true, false);

		return chart;
	}
	
	public static Document writeChartToPDF(JFreeChart chart, int width, int height, String fileName) {
		PdfWriter writer = null;

		Document document = new Document();

		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(
					fileName));
			document.open();
			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height,
					new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width,
					height);

			chart.draw(graphics2d, rectangle2d);
			
			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
		document.close();
		return document;
	}
	
}
