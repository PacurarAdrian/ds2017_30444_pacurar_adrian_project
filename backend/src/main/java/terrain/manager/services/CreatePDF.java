package terrain.manager.services;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
 
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import terrain.manager.dto.HistoryDTO;

public class CreatePDF {
	
	 
	private static Font TIME_ROMAN = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	private static Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	 
	/**
	* @param args
	*/
	public static Document createPDF(String file,List<HistoryDTO> listBooks) {
	 
	Document document = null;
	 
	try {
	document = new Document();
	PdfWriter.getInstance(document, new FileOutputStream(file));
	document.open();
	 
	addMetaData(document);
	 
	addTitlePage(document);
	 
	createTable(document,listBooks);
	 
	document.close();
	 
	} catch (FileNotFoundException e) {
	 
	e.printStackTrace();
	} catch (DocumentException e) {
	e.printStackTrace();
	}
	return document;
	 
	}
	 
	private static void addMetaData(Document document) {
	document.addTitle("Operation history");
	document.addSubject("Operation history report");
	document.addAuthor("administrator");
	document.addCreator("administrator");
	}
	 
	private static void addTitlePage(Document document)
	throws DocumentException {
	 
	Paragraph preface = new Paragraph();
	creteEmptyLine(preface, 1);
	preface.add(new Paragraph("History operation report", TIME_ROMAN));
	 
	creteEmptyLine(preface, 1);
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	preface.add(new Paragraph("Report created on "
	+ simpleDateFormat.format(new Date()), TIME_ROMAN_SMALL));
	document.add(preface);
	 
	}
	 
	private static void creteEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	 
	private static void createTable(Document document,List<HistoryDTO> listBooks) throws DocumentException {
		Paragraph paragraph = new Paragraph();
		creteEmptyLine(paragraph, 2);
		document.add(paragraph);
		PdfPTable table = new PdfPTable(5);
		 
		PdfPCell c1 = new PdfPCell(new Phrase("Operation"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		 
		c1 = new PdfPCell(new Phrase("Date"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		 
		c1 = new PdfPCell(new Phrase("Employee"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		c1 = new PdfPCell(new Phrase("Client"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		c1 = new PdfPCell(new Phrase("Terrain"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		
		table.setHeaderRows(1);
		 
		for (HistoryDTO e:listBooks) {
			table.setWidthPercentage(100);
			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(e.getOperation());
			SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
			
			table.addCell(sdf.format(e.getDate()));
			table.addCell(e.getUser().getFirstname()+" "+e.getUser().getSurname());
			if(e.getClient()!=null)
			table.addCell(e.getClient().getName());
			else table.addCell("-");
			table.addCell(e.getTerrain().toString());
		
			
		}
		 
		document.add(table);
	}
	 
	

}
