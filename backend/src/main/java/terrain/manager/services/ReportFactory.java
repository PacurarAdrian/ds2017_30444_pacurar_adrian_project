package terrain.manager.services;

public class ReportFactory {
	//use getShape method to get object of type shape 
	   public Report getReport(String reportType){
	      if(reportType == null){
	         return null;
	      }		
	      if(reportType.equalsIgnoreCase("CSV"))
	         return new CSVReport();
	      else if(reportType.equalsIgnoreCase("PDF"))
	    	  	return new PDFReport();
	      if(reportType.equalsIgnoreCase("PDFGraph"))
	    	  	return new PDFChartReport(); 
	      if(reportType.equalsIgnoreCase("PDFUserGraph"))
	    	  return new PDFUserChartReport();
	      return null;
	   
	}
}
