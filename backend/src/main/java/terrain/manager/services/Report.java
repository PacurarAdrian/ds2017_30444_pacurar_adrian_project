package terrain.manager.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import terrain.manager.dto.HistoryDTO;

public interface Report {
	
	HttpServletResponse download(HttpServletResponse response,List<HistoryDTO> listBooks) throws IOException;
	

}
