package terrain.manager.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.dto.ClientDTO;
import terrain.manager.entities.ClientAccount;
import terrain.manager.entities.ClientInformation;
import terrain.manager.entities.Notification;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.errorhandler.ServiceNotFoundException;
import terrain.manager.repositories.ClientAccountRepository;
import terrain.manager.repositories.ClientInformationRepository;

@Service
public class ClientService {
	@Autowired
	private ClientInformationRepository infoRepository;
	@Autowired
	private ClientAccountRepository accRepository;
	public static final List<String> types=Arrays.asList("regular","other","private");
	/*private static final Pattern EMAIL_PATTERN=Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"
			+ "\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@"
			+ "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?"
			+ "[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e"
			+ "-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");*/
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	public 	List<String> getTypes(){
		return types;
	}
	
	public List<ClientDTO> findClientByType(String type) {
		List<ClientAccount> users = accRepository.findByType(type);
		List<ClientDTO> toReturn = new ArrayList<ClientDTO>();
		for (ClientAccount user : users) {
			
			
			toReturn.add(clientToDTOBrief(user));
		}
		return toReturn;
	}
	public void deleteClient(int clientId){
		try{
		ClientAccount acc=accRepository.findById(clientId);
		if (acc == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		try{
		accRepository.delete(clientId);
		infoRepository.delete(acc.getInfo().getId());
		
		}catch(ConstraintViolationException e)
		{
			throw new EntityValidationException(ClientInformation.class.getSimpleName(),
							Arrays.asList("Could not delete entity because it is involved in other operations."));
		}
		Notification dvd=new Notification("Your account on Land title manager was deleted!\n"+acc.toString(),acc.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
		
		}
		catch(IllegalArgumentException e){
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		
	}
	public ClientDTO findClientInformationById(int accId) {
		ClientAccount acc = accRepository.findById(accId);
		if (acc == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		

		ClientDTO dto = new ClientDTO.Builder()
						.address(acc.getInfo().getAddress())
						.ICN(acc.getInfo().getICN())
						.PNC(acc.getInfo().getPnc())
						.id(acc.getInfo().getId())
						.name(acc.getInfo().getName())
						.type(acc.getType())
						.amount(acc.getAmount())
						.email(acc.getEmail())
						.create();
		return dto;
	}
	public int findClientIdByPNC(String pnc)
	{
		
		ClientInformation info=infoRepository.findByPnc(pnc);
		if (info == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		ClientAccount acc=accRepository.findByInfoId(info.getId());
		if (acc == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		
		return acc.getId();
		
	}
	public ClientDTO findClientAccountById(int infoId) {
		ClientAccount info = accRepository.findById(infoId);
		if (info == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		

		ClientDTO dto = new ClientDTO.Builder()
						.creationDate(info.getCreationDate())
						.username(info.getUsername())
						.password(info.getPassword())
						.email(info.getEmail())
						.id(info.getId())
						.amount(info.getAmount())
						.type(info.getType())
						.create();
		if(info.getInfo()!=null)
			dto.setInfoId(info.getInfo().getId());
		else dto.setInfoId(-1);
		return dto;
	}
	
	public List<ClientDTO> findAll() {
		
		List<ClientAccount> accs=accRepository.findAll();
		
		List<ClientDTO> toReturn = new ArrayList<ClientDTO>();
		for (ClientAccount acc : accs) {
			
			/*ClientDTO dto = new ClientDTO.Builder()
						.id(acc.getInfo().getId())
						.name(acc.getInfo().getName())
						.PNC(acc.getInfo().getPNC())
						.creationDate(acc.getCreationDate())
						.username(acc.getUsername())
						.create();*/
			toReturn.add(clientToDTOBrief(acc));
		}
		return toReturn;
	}
	public static ClientDTO clientToDTOBrief(ClientAccount acc)
	{
		ClientDTO dto = new ClientDTO.Builder()
				.id(acc.getId())
				.name(acc.getInfo().getName())
				.PNC(acc.getInfo().getPnc())
				.creationDate(acc.getCreationDate())
				.username(acc.getUsername())
				.create();
		if(acc.getInfo()!=null)
			dto.setInfoId(acc.getInfo().getId());
		else dto.setInfoId(-1);
		return dto;
	}
	public ClientDTO findClientById(int id)
	{
		ClientAccount usr = accRepository.findById(id);
		if (usr == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}
		return new ClientDTO.Builder()
		.id(usr.getId())
		.name(usr.getInfo().getName())
		.address(usr.getInfo().getAddress())
		.email(usr.getEmail())
		.PNC(usr.getInfo().getPnc())
		.ICN(usr.getInfo().getICN())
		.creationDate(usr.getCreationDate())
		.infoId(usr.getInfo().getId())
		.username(usr.getUsername())
		.password(usr.getPassword())
		.type(usr.getType())
		.amount(usr.getAmount())
		.create();
			
		
		
		
	}
	public int create(ClientDTO clientDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateClientInformation(clientDTO);
		validationErrors.addAll(validateClientAccount(clientDTO));
		if (!validationErrors.isEmpty()) {
			System.out.println(validationErrors);
			throw new EntityValidationException(ClientInformation.class.getSimpleName(),validationErrors);
		}

		ClientInformation info = new ClientInformation();
		//user.setId(userDTO.getId());
		
		info.setAddress(clientDTO.getAddress());
		info.setICN(clientDTO.getICN());
		info.setName(clientDTO.getName());
		info.setPnc(clientDTO.getPNC());
		ClientInformation inf;
		
		try{
		inf = infoRepository.save(info);
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new EntityValidationException(ClientInformation.class.getSimpleName(),
					Arrays.asList("Could not save to database"));
		}
		
		
		ClientAccount acc=new ClientAccount();
		acc.setEmail(clientDTO.getEmail());
		acc.setAmount(clientDTO.getAmount());
		acc.setCreationDate(new Date());
		acc.setInfo(inf);
		acc.setPassword(clientDTO.getPassword());
		acc.setType(clientDTO.getType());
		acc.setUsername(clientDTO.getUsername());
		ClientAccount account;
		try{
			account=accRepository.save(acc);
		}catch(Exception e)
		{
			infoRepository.delete(inf);
			e.printStackTrace();
			throw new EntityValidationException(ClientAccount.class.getSimpleName(),
					Arrays.asList("Could not save to database"));
		}
		//userDTO.setId(user.getId());
		System.out.println("info ID:"+inf.getId());
		System.out.println("acc ID:"+account.getId());
		Notification dvd=new Notification("Your account on Land title manager was created!\n"+acc.toString(),acc.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
		return account.getId();
	}
	public int createAccount(ClientDTO clientDTO) {
		//System.out.println("intra");
		List<String> validationErrors = validateClientAccount(clientDTO);
		
		if (!validationErrors.isEmpty()) {
			System.out.println(validationErrors);
			throw new EntityValidationException(ClientInformation.class.getSimpleName(),validationErrors);
		}

		ClientAccount acc=new ClientAccount();
		acc.setEmail(clientDTO.getEmail());
		acc.setAmount(clientDTO.getAmount());
		acc.setCreationDate(new Date());
		acc.setInfo(null);
		acc.setPassword(clientDTO.getPassword());
		acc.setType(clientDTO.getType());
		acc.setUsername(clientDTO.getUsername());
		ClientAccount account=accRepository.save(acc);
		//userDTO.setId(user.getId());
		
		System.out.println("acc ID:"+account.getId());
		Notification dvd=new Notification("Your account on Land title manager was created!\n"+acc.toString(),acc.getEmail());	
        try {
			EmitLog.emitLogs(dvd);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new ServiceNotFoundException(Notification.class.getSimpleName());
		}
		
		return account.getId();
	}
	
	
	public void updateInfo(ClientDTO accDTO) {
		//System.out.println("intra");
		ClientAccount acc=accRepository.findById(accDTO.getId());
		if (acc == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}else{
			List<String> validationErrors = validateClientInformation(accDTO);
			if (!validationErrors.isEmpty()) {
				throw new EntityValidationException(ClientInformation.class.getSimpleName(),validationErrors);
			}

			ClientInformation inf=acc.getInfo();
			//inf.setId(accDTO.getInfoId());
			inf.setAddress(accDTO.getAddress());
			inf.setICN(accDTO.getICN());
			inf.setName(accDTO.getName());
			inf.setPnc(accDTO.getPNC());
			
			acc.setType(accDTO.getType());
			acc.setAmount(accDTO.getAmount());
		
		
			ClientInformation usr = infoRepository.save(inf);
			accRepository.save(acc);
			Notification dvd=new Notification("Your client information on Land title manager was updated!\n"+inf.toString(),acc.getEmail());	
	        try {
				EmitLog.emitLogs(dvd);
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new ServiceNotFoundException(Notification.class.getSimpleName());
			}
			//userDTO.setId(user.getId());
			System.out.println(usr.getId());
			
		
		}
		
	}
	public void updateAccount(ClientDTO accDTO) {
		//System.out.println("intra");
		ClientAccount info=accRepository.findById(accDTO.getId());
		if (info == null) {
			throw new ResourceNotFoundException(ClientInformation.class.getSimpleName());
		}else{
			List<String> validationErrors = validateClientAccount(accDTO);
			if (!validationErrors.isEmpty()) {
				throw new EntityValidationException(ClientInformation.class.getSimpleName(),validationErrors);
			}

			info.setId(accDTO.getId());
			info.setAmount(accDTO.getAmount());
			info.setUsername(accDTO.getUsername());
			info.setPassword(accDTO.getPassword());
			info.setType(accDTO.getType());
			info.setEmail(accDTO.getEmail());
		
			ClientAccount usr = accRepository.save(info);
			//userDTO.setId(user.getId());
			System.out.println(usr.getId());
			Notification dvd=new Notification("Your account on Land title manager was deleted!\n"+usr.toString(),usr.getEmail());	
	        try {
				EmitLog.emitLogs(dvd);
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new ServiceNotFoundException(Notification.class.getSimpleName());
			}
		
		}
		
	}
	private List<String> validateClientAccount(ClientDTO info) {
		List<String> validationErrors = new ArrayList<String>();
		
		if (info.getUsername() == null || "".equals(info.getUsername())) {
			validationErrors.add("Username field should not be empty");
		}
		if (info.getAmount() <=0 || "".equals(info.getAmount())) {
			validationErrors.add("Amount field should not be empty");
		}
		if (info.getPassword()== null || !validatePassword(info.getPassword())) {
			validationErrors.add("Password field should verify the following: \n"+
					"*password should be 4 or more alphanumeric characters long\n"+
					"* at least 1 number\n"+
					"* at least 1 lower case letter\n"+
					"* at least 1 upper case letter\n"+
					"* no special characters allowed.");
		}
		if (info.getType() == null || !types.contains(info.getType())) {
			validationErrors.add("Type should be from predefined types.");
		}
		if(info.getEmail()==null|| !validateEmail(info.getEmail()))
		{
			validationErrors.add("Invalid email address");
		}
		return validationErrors;
	}
	private boolean validatePassword(String password) {
		Pattern p = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{4,}$");
		//Pattern p1=Pattern.compile("[!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]*");
		//Pattern p2=Pattern.compile("\\d*");
		 Matcher m = p.matcher(password);
		 boolean b = m.matches();
		//m=p2.matcher(password);
		//b=b&&m.matches();
		//m=p1.matcher(password);
		//b=b&&m.matches();
		
		
		
		return b;
	}
	private boolean validateEmail(String email){
		
		Matcher m= VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		boolean b=m.matches();
		
		return b;
	}
	private List<String> validateClientInformation(ClientDTO info) {
		List<String> validationErrors = new ArrayList<String>();

		if (info.getName() == null || "".equals(info.getName())) {
			validationErrors.add("Name field should not be empty");
		}

		if (info.getPNC() == null || !validateNR(info.getPNC(),10)) {
			validationErrors.add("PNC does not have the correct format.");
		}
		if (info.getICN() == null || !validateNR(info.getICN(),6)) {
			validationErrors.add("ICN does not have the correct format.");
		}
		if (info.getAddress() == null || "".equals(info.getAddress())) {
			validationErrors.add("Address field should not be empty.");
		}
		
		return validationErrors;
	}
	private boolean validateNR(String pnc,int nr) {
		
		Pattern p = Pattern.compile("\\d{"+nr+"}");
		 Matcher m = p.matcher(pnc);
		 boolean b = m.matches();
		return b;
	}



}
