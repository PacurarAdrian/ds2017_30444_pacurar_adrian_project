package terrain.manager.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.dto.UserDTO;
import terrain.manager.entities.*;
import terrain.manager.repositories.PersistenceUserRepository;

@Service
public class PersistenceUserService {
	private static final String SPLIT_CH = " ";
	
	public static final List<String> types=Arrays.asList("regular","employee","admin");
	//private MailService mailService = new MailService("ds.laboratory1@gmail.com","ro.tuc.dsrl.ds.handsonbdv");
	
	@Autowired
	private PersistenceUserRepository usrRepository;

	
	
	public List<UserDTO> findUserByType(String type) {
		List<PersistenceUser> users = usrRepository.findByType(type);
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (PersistenceUser user : users) {
			String[] names = extractNames(user.getName());
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.telephone(user.getTelephone())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	
	public UserDTO findUserById(int userId) {
		PersistenceUser usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(PersistenceUser.class.getSimpleName());
		}
		String[] names = extractNames(usr.getName());

		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.firstname(names[0])
						.surname(names[1])
						.city(usr.getCity())
						.address(usr.getAddress())
						.email(usr.getEmail())
						.telephone(usr.getTelephone())
						.country(usr.getCountry())
						.postcode(usr.getPostcode())
						.type(usr.getType())
						.salary(usr.getSalary())
						.create();
		return dto;
	}
	
	
	public List<UserDTO> findAll() {
		List<PersistenceUser> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (PersistenceUser user : users) {
			String[] names = extractNames(user.getName());
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.telephone(user.getTelephone())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public static UserDTO userToDTOBrief(PersistenceUser user)
	{
		String fullname=user.getName();
		
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		
		UserDTO dto = new UserDTO.Builder()
				.id(user.getId())
				.firstname(names[0])
				.surname(names[1])
				.city(user.getCity())
				.address(user.getAddress())
				.email(user.getEmail())
				.telephone(user.getTelephone())
				.country(user.getCountry())
				.postcode(user.getPostcode())
				.type(user.getType())
				.salary(user.getSalary())
				.create();
		return dto;
	}
	public int create(UserDTO userDTO) {
		

		PersistenceUser user = new PersistenceUser();
		//user.setId(userDTO.getId());
		user.setName(userDTO.getFirstname().trim() + SPLIT_CH + userDTO.getSurname().trim());
		user.setEmail(userDTO.getEmail());
		user.setAddress(userDTO.getAddress());
		user.setPostcode(userDTO.getPostcode());
		user.setCity(userDTO.getCity());
		user.setCountry(userDTO.getCountry());
		user.setTelephone(userDTO.getTelephone());
		user.setSalary(userDTO.getSalary());
		user.setCreated(new Date());
		
		user.setType(userDTO.getType());
		

		//mailService.sendMail(user.getEmail(),"Your account on Land title manager was created!",user.toString());
		PersistenceUser usr = usrRepository.save(user);
		
		//userDTO.setId(user.getId());
		System.out.println(usr.getId());
		return usr.getId();
	}
	
	

	
	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}
}
