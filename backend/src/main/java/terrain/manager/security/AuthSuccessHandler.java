package terrain.manager.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import terrain.manager.dto.LoginDTO;
import terrain.manager.entities.Authentificable;

@Component
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler{

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthSuccessHandler.class);
	private final ObjectMapper mapper;
	
	@Autowired
	AuthSuccessHandler(MappingJackson2HttpMessageConverter messageConverter){
		this.mapper=messageConverter.getObjectMapper();
	}
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,HttpServletResponse response,
			Authentication authentication) throws IOException,ServletException{
		response.setStatus(HttpServletResponse.SC_OK);
		
		NuvolaUserDetails userDetails=(NuvolaUserDetails)authentication.getPrincipal();
		Authentificable user=userDetails.getUser();
		userDetails.setUser(user);
		LoginDTO log=new LoginDTO();
		System.out.println("login data:"+userDetails.getUsername());
		System.out.println("authorities:"+userDetails.getAuthorities());
		log.setUsername(userDetails.getUsername());
		Collection<? extends GrantedAuthority> authorities=userDetails.getAuthorities();
		for(GrantedAuthority grantedAuthority:authorities){
			if(grantedAuthority.getAuthority().startsWith("userId")){
				try{
					log.setId(Integer.parseInt(grantedAuthority.getAuthority().substring(6)));
					System.out.println("log.id:"+log.getId());
				}catch(Exception e)
				{
					log.setId(-1);
				}
			}
			if(grantedAuthority.getAuthority().startsWith("type:")){
				log.setType(grantedAuthority.getAuthority().substring(5));
			}
		}
		LOGGER.info(userDetails.getUsername()+"got connected");
		
		PrintWriter writer=response.getWriter();
		mapper.writeValue(writer, log);
		writer.flush();
	}
}
