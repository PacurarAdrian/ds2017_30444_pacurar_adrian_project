package terrain.manager.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import terrain.manager.entities.Authentificable;

public class NuvolaUserDetails implements UserDetails{
	private static final long serialVersionUID=1L;
	private Authentificable user;
	private List<GrantedAuthority> authorities;
	
	public NuvolaUserDetails(Authentificable user,List<GrantedAuthority> authorities){
		this.user=user;
		this.authorities=authorities;
	}
	
	public Authentificable getUser(){
		return user;
	}
	
	public void setUser(Authentificable user){
		this.user=user;
	}
	
	@Override
	public String getUsername(){
		return user.getUsername();
	}
	@Override
	public String getPassword(){
		return user.getPassword();
	}
	@Override
	public boolean isAccountNonExpired(){
		return true;
	}
	@Override
	public boolean isAccountNonLocked(){
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired(){
		return true;
	}
	@Override
	public boolean isEnabled(){
		return true;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities(){
		return authorities;
	}

}
