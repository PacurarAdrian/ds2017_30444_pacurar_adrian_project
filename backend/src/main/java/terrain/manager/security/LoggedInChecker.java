package terrain.manager.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import terrain.manager.entities.Authentificable;

@Component
public class LoggedInChecker {

	public Authentificable getLoggedInUser(){
		Authentificable user=null;
		Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
		if(authentication !=null){
			Object principal =authentication.getPrincipal();
			
			//principal cand be "anonymousUser" (String)
			if(principal instanceof NuvolaUserDetails){
				NuvolaUserDetails userDetails=(NuvolaUserDetails) principal;
				user=userDetails.getUser();
			}
		}
		return user;
	}
	
}
