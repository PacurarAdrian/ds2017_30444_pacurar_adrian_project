package terrain.manager.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import terrain.manager.entities.Authentificable;
import terrain.manager.entities.ClientAccount;
import terrain.manager.entities.User;
import terrain.manager.repositories.ClientAccountRepository;
import terrain.manager.repositories.UserRepository;

@Service
public class NuvolaUserDetailsService implements UserDetailsService{
	private final UserRepository userService;
	private final ClientAccountRepository clientService;
	@Autowired
	NuvolaUserDetailsService(UserRepository userService,ClientAccountRepository repository)
	{
		this.userService=userService;
		this.clientService=repository;
	}
	
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		System.out.println("intra in userDetails");
		User user=userService.findByUsername(username);
		List<GrantedAuthority> grantedAuthorities=new ArrayList<>();
		Authentificable auth=null;
		if(user==null){
			ClientAccount client=clientService.findByUsername(username);
			if(client==null)
				throw new UsernameNotFoundException(username);
			auth=client;
			System.out.println("intra in userDetails,gaseste clientu");
			grantedAuthorities.add(new SimpleGrantedAuthority("type:client"));
		}else{
			auth=user;
			System.out.println("intra in userDetails, gaseste useru");
			
			if(auth.getType().equals("admin"))
			{
				grantedAuthorities.add(new SimpleGrantedAuthority("type:user"));
				grantedAuthorities.add(new SimpleGrantedAuthority("type:admin"));
				
			}else grantedAuthorities.add(new SimpleGrantedAuthority("type:user"));
			
		}
		grantedAuthorities.add(new SimpleGrantedAuthority("userId"+String.valueOf(auth.getId())));
		return new NuvolaUserDetails(auth,grantedAuthorities);
	}

}
