package terrain.manager.dto;

public class TerrainDTO {
	private int id;
	private String type;
	private Integer OwnerID;
	private double area;
	private String place;
	private double price;
	private String pnc;
	public TerrainDTO(){
		
	}
	public TerrainDTO(int id,String type,Integer OwnerID,double area,String place,double price)
	{
		this(type,OwnerID,area,place);
		this.id=id;
		this.price=price;
	}
	public TerrainDTO(int id,String type,Integer OwnerID,double area,String place,double price,String pnc)
	{
		this(id,type,OwnerID,area,place,price);
		this.setPnc(pnc);
	}
	public TerrainDTO(String type,Integer OwnerID,double area,String place){
		
		this.setType(type);
		this.setOwnerID(OwnerID);
		this.setArea(area);
		this.setPlace(place);
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getOwnerID() {
		return OwnerID;
	}
	public void setOwnerID(Integer ownerID) {
		OwnerID = ownerID;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	@Override
	public String toString()
	{
		return "Terrain is located at"+place+" it has an area of "+area+" and it is "+type+" terrain";
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String pnc) {
		this.pnc = pnc;
	}
	public static class Builder {
		private int nestedid;
		private String nestedtype;
		private Integer nestedOwnerID;
		private double nestedarea;
		private String nestedplace;
		private double nestedPrice;
		private String nestedPnc;
		public Builder id(int id)
		{
			this.nestedid = id;
			return this;
		}
		
		public Builder type(String type){
			this.nestedtype=type;
			return this;
		}
		public Builder ownerID(Integer id)
		{
			this.nestedOwnerID=id;
			return this;
		}
		public Builder area(double area)
		{
			this.nestedarea=area;
			return this;
		}
		public Builder place(String place)
		{
			this.nestedplace=place;
			return this;
			
		}
		public Builder price(double price)
		{
			this.nestedPrice=price;
			return this;
			
		}
		public Builder pnc(String pnc) {
			
			this.nestedPnc=pnc;
			return this;
		}
		public TerrainDTO create() {
			return new TerrainDTO(nestedid, nestedtype, nestedOwnerID, nestedarea, nestedplace,nestedPrice,nestedPnc);
			
		}

		
	
	}
}
