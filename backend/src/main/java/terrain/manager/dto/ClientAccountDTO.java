package terrain.manager.dto;

import java.util.Date;

import terrain.manager.entities.ClientInformation;

public class ClientAccountDTO {
	private int id;
	private String type;
	private double amount;
	private Date creationDate;
	private String username;
	private String password;
	private String email;
	private ClientInformation info;
	
	public ClientAccountDTO(){
		
	}
	public ClientAccountDTO(int id,String type,double amount,Date creationDate,String username,String password,String email,ClientInformation info)
	{
		super();
		this.setId(id);
		this.setType(type);
		this.setAmount(amount);
		this.setCreationDate(creationDate);
		this.setUsername(username);
		this.setPassword(password);
		this.setEmail(email);
		this.setInfo(info);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ClientInformation getInfo() {
		return info;
	}
	public void setInfo(ClientInformation info) {
		this.info = info;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public class Builder{
		private int nestedId;
		private String nestedType;
		private double nestedAmount;
		private Date nestedCreationDate;
		private String nestedUsername;
		private String nestedPassword;
		private String nestedEmail;
		private ClientInformation nestedInfo;
		

		public Builder id(int id) {
			this.nestedId = id;
			return this;
		}

		public Builder type(String type) {
			this.nestedType = type;
			return this;
		}

		public Builder amount(double amount) {
			this.nestedAmount = amount;
			return this;
		}

		public Builder username(String username) {
			this.nestedUsername = username;
			return this;
		}
		public Builder password(String password){
			this.nestedPassword=password;
			return this;
			
		}
		public Builder email(String email){
			this.nestedEmail=email;
			return this;
			
		}

		public Builder info(ClientInformation info) {
			this.nestedInfo = info;
			return this;
		}
		
		public ClientAccountDTO create() {
			return new ClientAccountDTO(nestedId,nestedType,nestedAmount,nestedCreationDate,nestedUsername,nestedPassword,nestedEmail,nestedInfo);
		}
	}

}
