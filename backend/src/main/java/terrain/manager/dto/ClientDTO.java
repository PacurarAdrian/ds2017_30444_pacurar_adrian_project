package terrain.manager.dto;

import java.util.Date;

public class ClientDTO {

	private int id;
	private String name;
	private String ICN;
	private String infoPNC;
	private String address;
	private String type;
	private double amount;
	private Date creationDate;
	private String username;
	private String password;
	private String email;
	private int infoId;
	
	public ClientDTO()
	{
		
	}
	public ClientDTO(String name, String ICN,String PNC,String address,String type,double amount,Date creationDate,String username,String password,String email,int infoId)
	{
		super();
		this.setType(type);
		this.setAmount(amount);
		this.setCreationDate(creationDate);
		this.setUsername(username);
		this.setPassword(password);
		this.setName(name);
		this.setICN(ICN);
		this.setPNC(PNC);
		this.setAddress(address);
		this.setEmail(email);
		this.setInfoId(infoId);
	}
	
	public ClientDTO(int id,String name, String ICN,String PNC,String address,String type,double amount,Date creationDate,String username,String password,String email,int infoId)
	{
		
		this(name,ICN,PNC,address,type,amount,creationDate,username,password,email,infoId);
		this.setId(id);
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getICN() {
		return ICN;
	}
	public void setICN(String iCN) {
		ICN = iCN;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPNC() {
		return infoPNC;
	}
	public void setPNC(String infoPNC) {
		this.infoPNC = infoPNC;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getInfoId() {
		return infoId;
	}
	public void setInfoId(int infoId) {
		this.infoId = infoId;
	}
	public static class Builder{
		private int nestedId;
		private String nestedName;
		private String nestedICN;
		private String nestedPNC;
		private String nestedAddress;
		private String nestedType;
		private double nestedAmount;
		private Date nestedCreationDate;
		private String nestedUsername;
		private String nestedPassword;
		private String nestedEmail;
		private int nestedinfoId;
		public Builder id(int id) {
			this.nestedId = id;
			return this;
		}
		public Builder name(String name) {
			this.nestedName = name;
			return this;
		}
		public Builder ICN(String ICN) {
			this.nestedICN = ICN;
			return this;
		}
		public Builder PNC(String PNC) {
			this.nestedPNC = PNC;
			return this;
		}
		public Builder address(String address) {
			this.nestedAddress = address;
			return this;
		}
		public Builder type(String type) {
			this.nestedType = type;
			return this;
		}

		public Builder amount(double amount) {
			this.nestedAmount = amount;
			return this;
		}

		public Builder username(String username) {
			this.nestedUsername = username;
			return this;
		}
		public Builder password(String password){
			this.nestedPassword=password;
			return this;
			
		}
		public Builder email(String email){
			this.nestedEmail=email;
			return this;
			
		}
		public Builder creationDate(Date date)
		{
			this.nestedCreationDate=date;
			return this;
		}
		public Builder infoId(int date)
		{
			this.nestedinfoId=date;
			return this;
		}
		public ClientDTO create() {
			return new ClientDTO(nestedId,nestedName,nestedICN,nestedPNC,nestedAddress, nestedType, nestedAmount, nestedCreationDate, nestedUsername, nestedPassword,nestedEmail,nestedinfoId);
		}
	}
}
