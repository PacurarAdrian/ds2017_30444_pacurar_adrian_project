package terrain.manager.dto;

public class RepartitionLightDTO {
	
	private int terrain;
	private int client;
	private int id;
	
	public RepartitionLightDTO(){
		
	}
	public RepartitionLightDTO(int terrain,int client)
	{
		this.setTerrain(terrain);
		this.setClient(client);
		
	}
	public RepartitionLightDTO(int id,int terrain,int client)
	{
		this.setTerrain(terrain);
		this.setClient(client);
		
	}
	
	public int getTerrain() {
		return terrain;
	}
	public void setTerrain(int terrain2) {
		this.terrain = terrain2;
	}
	public int getClient() {
		return client;
	}
	public void setClient(int client2) {
		this.client = client2;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	public static class Builder {
		
		private int nestedTerrain;
		private int nestedClient;
		private int nestedId;
		public Builder id(int terrain) {
			this.nestedTerrain = terrain;
			return this;
		}
		
		
		public Builder terrain(int terrain) {
			this.nestedTerrain = terrain;
			return this;
		}
		public Builder client(int acc) {
			this.nestedClient = acc;
			return this;
		}
		
		public RepartitionLightDTO create() {
			return new RepartitionLightDTO(nestedId,nestedTerrain,nestedClient);
		}
	}

}
