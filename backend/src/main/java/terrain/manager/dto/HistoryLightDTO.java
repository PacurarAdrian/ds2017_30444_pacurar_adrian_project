package terrain.manager.dto;

import java.util.Date;

public class HistoryLightDTO {
	
	private int id;
	private String operation;
	private int user;
	private int client;
	private int terrain;
	private Date date;
	
	public HistoryLightDTO()
	{
		
		
	}
	public HistoryLightDTO(int id,String operation,int user,int client,int terrain,Date date)
	{
		this.setId(id);
		this.setOperation(operation);
		this.setUser(user);
		this.setClient(client);
		this.setTerrain(terrain);
		this.setDate(date);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getClient() {
		return client;
	}
	public void setClient(int client) {
		this.client = client;
	}
	public int getTerrain() {
		return terrain;
	}
	public void setTerrain(int terrain) {
		this.terrain = terrain;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public static class Builder {
		private int nestedid;
		private String nestedOperation;
		private int nestedUser;
		private int nestedClient;
		private int nestedTerrain;
		private Date nestedDate;	
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder operation(String op) {
			this.nestedOperation = op;
			return this;
		}
		public Builder user(int user) {
			this.nestedUser = user;
			return this;
		}
		public Builder client(int cli) {
			this.nestedClient = cli;
			return this;
		}
		public Builder terrain(int terrain) {
			this.nestedTerrain = terrain;
			return this;
		}
		public Builder date(Date date) {
			this.nestedDate = date;
			return this;
		}
		public HistoryLightDTO create() {
			return new HistoryLightDTO(nestedid,nestedOperation,nestedUser,nestedClient,nestedTerrain,nestedDate);
		}

	}

}
