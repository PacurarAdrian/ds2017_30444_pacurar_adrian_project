package terrain.manager.dto;

import java.util.Date;

public class HistoryDTO {
	
	private int id;
	private String operation;
	private UserDTO user;
	private ClientDTO client;
	private TerrainDTO terrain;
	private Date date;
	private int clientId;
	private int userId;
	private int terrainId;
	
	public HistoryDTO()
	{
		
		
	}
	public HistoryDTO(int id,String operation,UserDTO user,ClientDTO client,TerrainDTO terrain,Date date)
	{
		this.setId(id);
		this.setOperation(operation);
		this.setUser(user);
		this.setClient(client);
		this.setTerrain(terrain);
		this.setDate(date);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public ClientDTO getClient() {
		return client;
	}
	public void setClient(ClientDTO client) {
		this.client = client;
	}
	public TerrainDTO getTerrain() {
		return terrain;
	}
	public void setTerrain(TerrainDTO terrain) {
		this.terrain = terrain;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTerrainId() {
		return terrainId;
	}
	public void setTerrainId(int terrainId) {
		this.terrainId = terrainId;
	}
	public static class Builder {
		private int nestedid;
		private String nestedOperation;
		private UserDTO nestedUser;
		private ClientDTO nestedClient;
		private TerrainDTO nestedTerrain;
		private Date nestedDate;	
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder operation(String op) {
			this.nestedOperation = op;
			return this;
		}
		public Builder user(UserDTO user) {
			this.nestedUser = user;
			return this;
		}
		public Builder client(ClientDTO cli) {
			this.nestedClient = cli;
			return this;
		}
		public Builder terrain(TerrainDTO terrain) {
			this.nestedTerrain = terrain;
			return this;
		}
		public Builder date(Date date) {
			this.nestedDate = date;
			return this;
		}
		public HistoryDTO create() {
			return new HistoryDTO(nestedid,nestedOperation,nestedUser,nestedClient,nestedTerrain,nestedDate);
		}

	}

}
