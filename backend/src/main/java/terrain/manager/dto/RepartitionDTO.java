package terrain.manager.dto;

import java.util.Date;

public class RepartitionDTO {
	private int id;
	private TerrainDTO terrain;
	private ClientDTO client;
	private Date purchaseDate;
	
	public RepartitionDTO(){
		
	}
	public RepartitionDTO(TerrainDTO terrain,ClientDTO client)
	{
		this.setTerrain(terrain);
		this.setClient(client);
		
	}
	
	public RepartitionDTO(int id,TerrainDTO terrain,ClientDTO client,Date purchaseDate)
	{
		this(terrain,client);
		this.setPurchaseDate(purchaseDate);
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public TerrainDTO getTerrain() {
		return terrain;
	}
	public void setTerrain(TerrainDTO terrain2) {
		this.terrain = terrain2;
	}
	public ClientDTO getClient() {
		return client;
	}
	public void setClient(ClientDTO client2) {
		this.client = client2;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	public static class Builder {
		private int nestedId;
		private TerrainDTO nestedTerrain;
		private ClientDTO nestedClient;
		private Date nestedPurchaseDate;
		
		public Builder id(int id) {
			this.nestedId = id;
			return this;
		}
		
		public Builder terrain(TerrainDTO terrain) {
			this.nestedTerrain = terrain;
			return this;
		}
		public Builder client(ClientDTO acc) {
			this.nestedClient = acc;
			return this;
		}
		public Builder purchaseDate(Date date) {
			this.nestedPurchaseDate = date;
			return this;
		}
		
		public RepartitionDTO create() {
			return new RepartitionDTO(nestedId,nestedTerrain,nestedClient,nestedPurchaseDate);
		}
	}

}
