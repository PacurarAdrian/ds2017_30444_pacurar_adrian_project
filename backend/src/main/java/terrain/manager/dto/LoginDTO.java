package terrain.manager.dto;

public class LoginDTO {
	private int id;
	
	private String type;
	
	private String username;
	
	private String password;
	
	public LoginDTO(){
		this("","");
	}
	
	public LoginDTO(String username,String password){
		
		this(-1,username,password,"");
		
	}
	public LoginDTO(int id,String username,String password,String type){
		this.id=id;
		this.setUsername(username);
		this.setPassword(password);
		this.type=type;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String type) {
		this.username = type;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String place) {
		this.password = place;
	}
	@Override
	public String toString()
	{
		return "Terrain is located at"+password+" and it is "+username+" terrain";
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public static class Builder {
		private String nestedUsername;
		private String nestedtype;
		private String nestedpass;
		private int nestedid;
		
		

		public Builder id(int type){
			this.nestedid=type;
			return this;
		}

		public Builder type(String type){
			this.nestedtype=type;
			return this;
		}
		public Builder username(String type){
			this.nestedUsername=type;
			return this;
		}
		
		public Builder password(String place)
		{
			this.nestedpass=place;
			return this;
			
		}
		public LoginDTO create() {
			return new LoginDTO(nestedid, nestedUsername, nestedpass,nestedtype);
			
		}
	
	}
}
