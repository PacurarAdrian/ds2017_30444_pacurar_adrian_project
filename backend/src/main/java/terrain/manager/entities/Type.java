package terrain.manager.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "type")
public class Type implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private double price;
	
	

	public Type()
	{
		
	}
	
	public Type(int id,String type)
	{
		this.id=id;
		this.name=type;		
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "name", nullable = false, length = 100,unique=true)
	public String getName() {
		return name;
	}
	public void setName(String type) {
		this.name = type;
	}
	
	@Column(name = "price", nullable = false)
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString()
	{
		return name+" terrain, costs "+price+"/MU";
	}
}
