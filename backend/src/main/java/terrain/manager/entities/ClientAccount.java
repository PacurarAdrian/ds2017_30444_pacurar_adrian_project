package terrain.manager.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "clientAccount")
public class ClientAccount extends Authentificable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String type;
	private double amount;
	private Date creationDate;
	private String username;
	private String password;
	private String email;
	private ClientInformation info;
	
	public ClientAccount(){
		
	}
	public ClientAccount(int id,String type,double amount, Date creationDate,String username,String password)
	{
		this.id=id;
		this.type=type;
		this.amount=amount;
		this.creationDate=creationDate;
		
		this.username=username;
		this.password=password;
	}
	
	
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	@Column(name = "type", nullable = false, length = 100)
	public String getType() {
		return type;
	}
	@Override
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "amount", nullable = false,columnDefinition = "DECIMAL(10,2)")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name = "creation_date", nullable = false, length = 100)
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	@Column(name = "username", nullable = false, length = 100,unique = true)
	public String getUsername() {
		return username;
	}
	@Override
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	@Column(name = "password", nullable = false, length = 100,unique = true)
	public String getPassword() {
		return password;
	}
	@Override
	public void setPassword(String password) {
		this.password = password;
	}
	
	@OneToOne
	@JoinColumn(name = "infoID", nullable = true,updatable=true)
	public ClientInformation getInfo() {
		return info;
	}
	public void setInfo(ClientInformation info) {
		this.info = info;
	}
	@Override
	@Column(name="email", nullable=false,length=100)
	public String getEmail() {
		return email;
	}
	@Override
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString()
	{
		return "Client "+type+" account has username:"+username+" password:"+password+", email:"+email+
				" was created on "+creationDate+".\nAmount of credit on account is "+amount+". Accociated client information:\n"+info;
	
	}
	
}
