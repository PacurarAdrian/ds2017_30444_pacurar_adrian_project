package terrain.manager.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "persistenceClient")
public class PersistenceClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String type;
	private double amount;
	private Date creationDate;
	private String email;
	private String name;
	private String ICN;
	private String pnc;
	private String address;
	
	public PersistenceClient(){
		
	}
	public PersistenceClient(int id,String type,double amount, Date creationDate,String username,String password,String name, String ICN,String PNC,String address)
	{
		this.id=id;
		this.type=type;
		this.amount=amount;
		this.creationDate=creationDate;		
		this.name=name;
		this.ICN=ICN;
		this.pnc=PNC;
		this.address=address;
	}
	
	

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name = "type", nullable = false, length = 100)
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "amount", nullable = false,columnDefinition = "DECIMAL(10,2)")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name = "creation_date", nullable = false, length = 100)
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
	
	
	
	@Column(name="email", nullable=false,length=100)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "ICN", nullable = false,columnDefinition = "BIGINT(6)",unique=true)
	public String getICN() {
		return ICN;
	}
	public void setICN(String iCN) {
		ICN = iCN;
	}
	
	@Column(name = "PNC", nullable = false,columnDefinition = "VARCHAR(13)",unique=true)
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String pNC) {
		this.pnc = pNC;
	}
	@Column(name = "address", nullable = false, length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString()
	{
		return "Client "+type+" account has email:"+email+
				" was created on "+creationDate+".\nAmount of credit on account is "+amount+
				".Accociated client information:\n"+
				"Client "+name+" has PNC "+pnc+", ICN "+ICN+" has residence in "+address+".";
	
	}
	
}
