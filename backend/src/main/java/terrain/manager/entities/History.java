package terrain.manager.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "history")
public class History implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String operation;
	private PersistenceUser user;
	private PersistenceClient client;
	private PersistenceTerrain terrain;
	private Date date;
	
	public History(){
		
	}
	public History(int id,String operation, PersistenceUser user,PersistenceClient client,PersistenceTerrain terrain,Date date)
	{
		this.id=id;
		this.operation=operation;
		this.user=user;
		this.client=client;
		this.terrain=terrain;
		this.date=date;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "operation", nullable = false, length = 100)
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	@ManyToOne
	@JoinColumn(name = "useId", nullable = false,updatable=true)
	public PersistenceUser getUser() {
		return user;
	}
	public void setUser(PersistenceUser user) {
		this.user = user;
	}
	@ManyToOne
	@JoinColumn(name = "clientId", nullable = true,updatable=true)
	public PersistenceClient getClient() {
		return client;
	}
	
	public void setClient(PersistenceClient client) {
		this.client = client;
	}
	
	@ManyToOne
	@JoinColumn(name = "terrainId", nullable = false,updatable=true)
	public PersistenceTerrain getTerrain() {
		return terrain;
	}
	public void setTerrain(PersistenceTerrain terrain) {
		this.terrain = terrain;
	}
	@Column(name = "date", nullable = false, length = 100)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	

		
}
