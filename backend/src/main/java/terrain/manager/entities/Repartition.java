package terrain.manager.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "repartition")
public class Repartition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Terrain terrain;
	private ClientAccount client;
	private Date purchaseDate;
	
	public Repartition(){
		
	}
	public Repartition(int id,Terrain terrain,ClientAccount client,Date purchaseDate)
	{
		this.setId(id);
		this.setTerrain(terrain);
		this.setClient(client);
		this.setPurchaseDate(purchaseDate);
		
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name = "terrainId", nullable = false,updatable=true)
	public Terrain getTerrain() {
		return terrain;
	}
	public void setTerrain(Terrain terrain) {
		this.terrain = terrain;
	}
	
	@ManyToOne
	@JoinColumn(name = "clientId", nullable = false,updatable=true)
	public ClientAccount getClient() {
		return client;
	}
	public void setClient(ClientAccount client) {
		this.client = client;
	}
	
	@Column(name = "date", nullable = false, length = 100)
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

}
