package terrain.manager.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user")
public class User extends Authentificable implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String email;
	private String address;
	private String postcode;
	private String city;
	private String country;
	private String telephone;
	private double salary;
	private Date created;
	private String password;
	private String username;
	private String type;

	public User() {
	}
public String toString()
{
	return "User "+type+" account with email "+email+", telephone "+telephone+",salary "+salary+" for person "+name+" located in "
			+city+", postcode "+postcode+" "+country+" was created on"+created;
}
	public User(Integer id, String name, String email, String address, String postcode, String city, String country,
			String telephone, double salary, Date created, String password,String type,String username) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.address = address;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
		this.telephone = telephone;
		this.salary = salary;
		this.created = created;
		this.password=password;
		this.type=type;
		this.username=username;
	}
@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	@Column(name = "email", nullable = false, length = 200)
	public String getEmail() {
		return this.email;
	}
	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "address", nullable = true, length = 200)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "postcode", nullable = true, length = 50)
	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Column(name = "city", nullable = true, length = 100)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "country", nullable = true, columnDefinition = "char(2)")
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "telephone", nullable = true, length = 50)
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "salary", nullable = false)
	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}


	@Column(name = "created", nullable = false, length = 100)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	@Override
	@Column(name = "password", nullable = false, length = 100,unique=true)
	public String getPassword() {
		return password;
	}
	@Override
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	@Column(name = "type", nullable = false, length = 100)
	public String getType() {
		return type;
	}
	@Override
	public void setType(String type) {
		this.type = type;
	}
	@Override
	@Column(name = "username", nullable = false, length = 100,unique=true)
	public String getUsername() {
		return username;
	}
	@Override
	public void setUsername(String username) {
		this.username = username;
	}

}
