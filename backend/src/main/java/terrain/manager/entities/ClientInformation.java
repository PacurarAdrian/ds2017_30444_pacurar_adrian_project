package terrain.manager.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;


@Entity
@Table(name = "clientInformation")
public class ClientInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String ICN;
	private String pnc;
	private String address;
	
	
	public ClientInformation(){
		
	}
	public ClientInformation(int id,String name, String ICN,String PNC,String address)
	{
		this.id=id;
		this.name=name;
		this.ICN=ICN;
		this.pnc=PNC;
		this.address=address;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "ICN", nullable = false,columnDefinition = "BIGINT(6)",unique=true)
	public String getICN() {
		return ICN;
	}
	public void setICN(String iCN) {
		ICN = iCN;
	}
	
	@Column(name = "PNC", nullable = false,columnDefinition = "VARCHAR(13)",unique=true)
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String pNC) {
		this.pnc = pNC;
	}
	@Column(name = "address", nullable = false, length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString()
	{
		return "Client "+name+" has PNC "+pnc+", ICN "+ICN+" has residence in "+address+".";
	}
}
