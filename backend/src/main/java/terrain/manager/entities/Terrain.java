package terrain.manager.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "terrain")
public class Terrain implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private Type type;
	private Integer OwnerID;
	private double area;
	private String place;
	
	public Terrain()
	{
		
	}
	
	public Terrain(int id,Type type,Integer owner,double area,String place)
	{
		this.id=id;
		this.type=type;
		this.OwnerID=owner;
		this.area=area;
		this.place=place;
		
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name = "type", nullable = false,updatable=true)
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
	@Column(name = "ownerID", nullable = true)
	public Integer getOwnerID() {
		return OwnerID;
	}
	public void setOwnerID(Integer ownerID) {
		OwnerID = ownerID;
	}
	
	@Column(name = "area", nullable = false)
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	
	@Column(name = "place", nullable = false, length = 200)
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	@Override
	public String toString()
	{
		return "Terrain is located at"+place+" it has an area of "+area+" and it is "+type+".";
	}
}
