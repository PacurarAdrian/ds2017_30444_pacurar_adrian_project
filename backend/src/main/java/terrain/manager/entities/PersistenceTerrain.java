package terrain.manager.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "persistenceTerrain")
public class PersistenceTerrain implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String type;
	private String pnc;
	private double area;
	private String place;
	private double price;
	
	public PersistenceTerrain()
	{
		
	}
	
	public PersistenceTerrain(int id,String type,String owner,double area,String place)
	{
		this.id=id;
		this.type=type;
		this.pnc=owner;
		this.area=area;
		this.place=place;
		
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "type", nullable = false,updatable=true)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "pnc", nullable = false,columnDefinition = "VARCHAR(13)",unique=true)
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String ownerID) {
		pnc = ownerID;
	}
	
	@Column(name = "area", nullable = false)
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	
	@Column(name = "place", nullable = false, length = 200)
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	@Override
	public String toString()
	{
		return "Terrain is located at"+place+" it has an area of "+area+" and it is "+type+".";
	}
	@Column(name = "price", nullable = false)
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
