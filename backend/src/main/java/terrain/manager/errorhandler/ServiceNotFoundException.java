package terrain.manager.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Could not send mail.")
public class ServiceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ServiceNotFoundException(String msg){
		super(msg);
	}

}
