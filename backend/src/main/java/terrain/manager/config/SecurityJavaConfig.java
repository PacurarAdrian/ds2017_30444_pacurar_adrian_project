package terrain.manager.config;

import terrain.manager.security.AuthFailureHandler;
import terrain.manager.security.AuthSuccessHandler;
import terrain.manager.security.HttpAuthenticationEntryPoint;
import terrain.manager.security.HttpLogoutSuccessHandler;
import terrain.manager.security.NuvolaUserDetailsService;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@ComponentScan(value="terrain.manager.security")
public class SecurityJavaConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private NuvolaUserDetailsService userDetailsService;
	@Autowired
	private HttpAuthenticationEntryPoint authenticationEntryPoint;
	@Autowired
	private AuthSuccessHandler authSuccessHandler;
	
	@Autowired
	private AuthFailureHandler authFailureHandler;
	
	@Autowired
	private HttpLogoutSuccessHandler logoutSuccessHandler;
	
	
    //@Autowired
    //private CustomAccessDeniedHandler accessDeniedHandler;

    // @Autowired
    // private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    // @Autowired
    // private MySavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler;
    
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManagerBean();
	}
	@Bean
	@Override
	public UserDetailsService userDetailsServiceBean() throws Exception{
		return super.userDetailsServiceBean();
	}

	@Bean
	public AuthenticationProvider authenticationProvider(){
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		//authenticationProvider.setPasswordEncoder(new ShaPasswordEncoder());
		return authenticationProvider;
	}
    //
	@Override
	public void configure(WebSecurity web) throws Exception{
		web.debug(true);
	}
	
    @Override
    protected void configure( AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(authenticationProvider());
    }
    @Override
    protected AuthenticationManager authenticationManager() throws Exception{
    	return super.authenticationManager();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {// @formatter:off
        http
        .csrf().disable()
        .authorizeRequests()
        //.antMatchers("/api/csrfAttacker*").permitAll()
        .antMatchers("/client/**").hasAnyAuthority("type:user","type:admin")
        .antMatchers("/terrain/update/**").hasAuthority("type:user")
        .antMatchers("/terrain/insert/**").hasAuthority("type:user")
        .antMatchers("/terrain/delete/**").hasAuthority("type:user")
        
        .antMatchers("/repartition/**").hasAuthority("type:user")
        
        //.antMatchers("/account/client/**").hasRole("CLIENT")
        //.antMatchers("/account/user/**").hasRole("USER")
        .antMatchers("/terrain/**").authenticated()
        .antMatchers("/account/**").authenticated()
        .antMatchers("/user/**").hasAuthority("type:admin")
        .antMatchers("/history/**").hasAuthority("type:admin")
        .antMatchers("/**").permitAll()
        .and()
        .authenticationProvider(authenticationProvider())
        .exceptionHandling()//.accessDeniedHandler(accessDeniedHandler)
        .authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .formLogin()
        .permitAll()
        .loginProcessingUrl("/login").permitAll()
        .usernameParameter("ssoId")
        .passwordParameter("password")
        .successHandler(authSuccessHandler)
        .failureHandler(authFailureHandler)
        .and()
        .logout()
        .permitAll()
     // sample logout customization
        //.logout().deleteCookies("remove").invalidateHttpSession(false)
        //.logoutUrl("/custom-logout").logoutSuccessUrl("/")
        .logoutSuccessHandler(logoutSuccessHandler)
        
        //.loginPage("/login") // default is /login with an HTTP get
        //.failureUrl("/loginfailed") // default is /login?error
        //.loginProcessingUrl("/getAccount") // default is /login
        //.defaultSuccessUrl("/getAccount")
        .and()
        .httpBasic()//.authenticationEntryPoint(getBasicAuthEntryPoint())
//        .and()
//        .successHandler(authenticationSuccessHandler)
//        .failureHandler(new SimpleUrlAuthenticationFailureHandler())    
        .and()
        .cors();
    } // @formatter:on
    
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("HEAD",
                "GET", "POST", "PUT", "DELETE", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
    @Bean
    public AuthSuccessHandler mySuccessHandler() {
        return authSuccessHandler;
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler myFailureHandler() {
        return authFailureHandler;
    }

}