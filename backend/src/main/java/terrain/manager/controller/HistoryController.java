package terrain.manager.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.dto.HistoryDTO;
import terrain.manager.dto.LoginDTO;
import terrain.manager.errorhandler.ResourceNotFoundException;
import terrain.manager.services.ClientService;
import terrain.manager.services.HistoryService;
import terrain.manager.services.Report;
import terrain.manager.services.ReportFactory;
import terrain.manager.services.TerrainService;
import terrain.manager.services.UserService;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/history")
public class HistoryController {
	
	@Autowired
	private HistoryService historyService;
	@Autowired
	private ClientService clientservice;
	@Autowired
	private TerrainService terrServ;
	
	@Autowired
	private UserService userServ;
	
	
	@RequestMapping(value = "/downloadPDF")
	public void downloadPDF( HttpServletResponse response) {
		 List<HistoryDTO> listBooks = historyService.findAll();
		 ReportFactory fact=new ReportFactory();
		 Report rep=fact.getReport("PDF");
		 try {
			 response=rep.download(response, listBooks);
		 } catch (Exception e) {
	
			 e.printStackTrace();
			 throw new ResourceNotFoundException("PDF download generation failed!");
		 }
		 
	}
	@RequestMapping(value = "/downloadGraph")
	public void downloadGraph( HttpServletResponse response) {
		 List<HistoryDTO> listBooks = historyService.findAll();
		 ReportFactory fact=new ReportFactory();
		 Report rep=fact.getReport("PDFGraph");
		 try {
			 response=rep.download(response, listBooks);
		 } catch (Exception e) {
	
			 e.printStackTrace();
			 throw new ResourceNotFoundException("PDF download generation failed!");
		 }
		 
	}
	@RequestMapping(value = "/downloadUserGraph")
	public void downloadUserGraph( HttpServletResponse response) {
		 List<HistoryDTO> listBooks = historyService.findAll();
		 ReportFactory fact=new ReportFactory();
		 Report rep=fact.getReport("PDFUserGraph");
		 try {
			 response=rep.download(response, listBooks);
		 } catch (Exception e) {
	
			 e.printStackTrace();
			 throw new ResourceNotFoundException("PDF download generation failed!");
		 }
		 
	}
	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV( HttpServletResponse response) {
		 List<HistoryDTO> listBooks = historyService.findAll();
		 ReportFactory fact=new ReportFactory();
	     Report rep=fact.getReport("CSV");
		 try {
			 response=rep.download(response, listBooks);
		 } catch (Exception e) {
	
			 e.printStackTrace();
			 throw new ResourceNotFoundException("CSV download generation failed!");
		 }
		 
	}
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public HistoryDTO getHistoryById(@PathVariable("id") int id) {
		return historyService.findHistoryById(id);
	}
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public List<HistoryDTO> getHistoryByDate(@PathVariable("type") String type) throws ParseException {
		type=type.trim();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		if(type.contains("to"))
		{
			String date1=type.substring(0, type.indexOf("to"));
			String date2=type.substring(type.indexOf("to")+2);
			System.out.println("start "+date1+"; end "+date2);
			Date start=format.parse(date1);
			Date end=format.parse(date2);
			return historyService.findHistoryByDates(start, end);
		}else
		{
			
			Date date = format.parse(type);
			return historyService.findHistoryByDate(date);
		}
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<HistoryDTO> getAllHistorys() {
	
		return historyService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.GET)//tre.post
	public int insertHistory()//@RequestBody HistoryDTO historyDTO)
	{
		//in req
		LoginDTO account=AccountController.getPrincipal();
		if(account==null)
	 	{
	 		return -1;
	 	}
		HistoryDTO historyDTO=new HistoryDTO();
		historyDTO.setClient(clientservice.findAll().get(0));
		historyDTO.setTerrain(terrServ.findAll().get(0));
		historyDTO.setUser(userServ.findUserById(account.getId()));
		historyDTO.setOperation("Client bought terrain");
		//
		System.out.println("ceva\n");
		return historyService.create(historyDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteHistory(@PathVariable("id")int id)
	{
		historyService.deleteHistory(id);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.GET)//tre.post
	public void updateHistory()//@RequestBody HistoryDTO historyDTO)
	{
		//in req 
		HistoryDTO historyDTO=historyService.findAll().get(0);
		historyDTO.setTerrain(terrServ.findAll().get(1));
		historyDTO.setOperation("Client bought terrain");
		//
		
		System.out.println("ceva\n");
		historyService.update(historyDTO);
	}

}
