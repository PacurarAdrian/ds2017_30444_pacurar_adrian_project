package terrain.manager.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.management.Notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.dto.HistoryDTO;
import terrain.manager.dto.LoginDTO;
import terrain.manager.dto.RepartitionDTO;
import terrain.manager.dto.RepartitionLightDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.errorhandler.ServiceNotFoundException;
import terrain.manager.services.HistoryService;
import terrain.manager.services.RepartitionService;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/repartition")
public class RepartitionController {
	@Autowired
	private RepartitionService repService;
	@Autowired
	private HistoryService histService;
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public RepartitionDTO getRepartitionById(@PathVariable("id") int id) {
		return repService.findRepartitionById(id);
	}
	@RequestMapping(value = "/clientTerrains/{id}", method = RequestMethod.GET)
	public List<TerrainDTO> getTerrainsByClientId(@PathVariable("id") int id) {
		return repService.findTerrainsByClientId(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<RepartitionDTO> getAllRepartitions() {
	
		return repService.findAll();
	}
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public List<RepartitionDTO> getHistoryByDate(@PathVariable("type") String type) throws ParseException {
		type=type.trim();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		if(type.contains("to"))
		{
			String date1=type.substring(0, type.indexOf("to"));
			String date2=type.substring(type.indexOf("to")+2);
			System.out.println("start "+date1+"; end "+date2);
			Date start=format.parse(date1);
			Date end=format.parse(date2);
			return repService.findRepartitionByDates(start, end);
		}else
		{
			
			Date date = format.parse(type);
			return repService.findRepartitionByDate(date);
		}
	}
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertRepartition(@RequestBody RepartitionLightDTO repDTO)//HttpServletRequest request)//@RequestBody RepartitionDTO userDTO)
	{
		//in req
		int resp=-1;
		LoginDTO account=AccountController.getPrincipal();
		if(account==null)
	 	{
	 		return -1;
	 	}
		
		
		//try{
		//RepartitionDTO userDTO=new RepartitionDTO(terrServ.findAll().get(0),clientservice.findAll().get(0));
		try{
			resp=repService.createLight(repDTO);
		}catch(ServiceNotFoundException e)
		{
			if(!e.getMessage().equals(Notification.class.getSimpleName()))
				throw e;
		}
		
		HistoryDTO historyDTO=new HistoryDTO();
		historyDTO.setClientId(repDTO.getClient());
		historyDTO.setTerrainId(repDTO.getTerrain());
		historyDTO.setUserId(account.getId());
		historyDTO.setOperation("Client bought land title");
		histService.create(historyDTO);
		/*}catch(EntityValidationException e)
		{
			if(e.getMessage().equals(Repartition.class.getSimpleName()))
				//System.out.println("Problem(s) occured while trying to insert data to database:\n"+e.getValidationErrors());
				resp="Problem(s) occured while trying to insert data to database:\n"+e.getValidationErrors();
			else
			if(e.getMessage().equals(History.class.getSimpleName()))
				//System.out.println("Problem(s) occured while saving operation to history:\n"+e.getValidationErrors());
				resp="Problem(s) occured while saving operation to history:\n"+e.getValidationErrors();
			else resp="Unexpected problem occured while saving operation to database:\n"+e.getValidationErrors();
		}
		catch (Exception e)
		{
			//System.out.println(e.getMessage());
			e.printStackTrace();
			resp="Unexpected problem occured. Please try again";
		}*/

		System.out.println("ceva\n");
		return resp;
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteRepartition(@PathVariable("id")int id)//,HttpServletRequest request)
	{
		/*String resp=null;
		
		try{*/
		LoginDTO account=AccountController.getPrincipal();
		
			RepartitionDTO rep =repService.findRepartitionById(id);
			
			HistoryDTO historyDTO=new HistoryDTO();
			historyDTO.setClientId(rep.getClient().getId());
			historyDTO.setTerrainId(rep.getTerrain().getId());
			try{
			repService.deleteRepartition(id);
			}catch(ServiceNotFoundException e)
			{
				if(!e.getMessage().equals(Notification.class.getSimpleName()))
					throw e;
			}
			
			
			historyDTO.setUserId(account.getId());
			historyDTO.setOperation("Client sold land title");
			histService.create(historyDTO);
		/*}catch(EntityValidationException e)
		{
				if(e.getMessage().equals(History.class.getSimpleName()))
					//System.out.println("Problem(s) occured while saving operation to history:\n"+e.getValidationErrors());
					resp="Problem(s) occured while saving operation to history:\n"+e.getValidationErrors();
				else resp="Unexpected problem occured while saving operation to database:\n"+e.getValidationErrors();
		}catch(Exception e)
		{
			e.printStackTrace();
			resp="Unexpected problem occured. Please try again";
		}
		
		return resp;*/
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)//tre.post
	public void updateRepartition(@PathVariable("id") int repId,@RequestBody RepartitionLightDTO repDTO)//HttpServletRequest request)//@RequestBody RepartitionDTO userDTO)
	{
		//in req 
		//String resp=null;
		LoginDTO account=AccountController.getPrincipal();
		
		//try{
			/*RepartitionDTO repDTO=repService.findAll().get(0);
			repDTO.setTerrain(terrServ.findAll().get(0));
			repDTO.setClient(clientservice.findAll().get(0));*/
			repDTO.setId(repId);
			try{
			repService.updateLight(repDTO);
			}catch(ServiceNotFoundException e)
			{
				if(!e.getMessage().equals(Notification.class.getSimpleName()))
					throw e;
			}
			//userDTO.setId(16);
			//
			HistoryDTO historyDTO=new HistoryDTO();
			historyDTO.setClientId(repDTO.getClient());
			historyDTO.setTerrainId(repDTO.getTerrain());
			historyDTO.setUserId(account.getId());
			historyDTO.setOperation("Client-land title repartition updated");
			histService.create(historyDTO);
			//resp="successfully executed operation";
		/*}catch(EntityValidationException e)
		{
			if(e.getMessage().equals(Repartition.class.getSimpleName()))
				//System.out.println("Problem(s) occured while trying to insert data to database:\n"+e.getValidationErrors());
				resp="Problem(s) occured while trying to insert data to database:\n"+e.getValidationErrors();
			else
			if(e.getMessage().equals(History.class.getSimpleName()))
				//System.out.println("Problem(s) occured while saving operation to history:\n"+e.getValidationErrors());
				resp="Problem(s) occured while saving operation to history:\n"+e.getValidationErrors();
			else resp="Unexpected problem occured while saving operation to database:\n"+e.getValidationErrors();
		}
		catch(ResourceNotFoundException e)
		{
			if(e.getMessage().equals(Repartition.class.getSimpleName()))
				//System.out.println("Problem(s) occured while trying to insert data to database:\n"+e.getValidationErrors());
				resp="Could not find existing repartition in database\n";
			else resp="Unexpected problem occured while saving operation to database:\n";
		}
		catch (Exception e)
		{
			//System.out.println(e.getMessage());
			e.printStackTrace();
			resp="Unexpected problem occured. Please try again";
		}*/
		
		System.out.println("ceva\n");
		//return resp;
	}

}
