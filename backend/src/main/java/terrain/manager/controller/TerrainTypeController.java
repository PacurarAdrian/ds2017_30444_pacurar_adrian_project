package terrain.manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.entities.Type;
import terrain.manager.services.TerrainService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/terrainType")
public class TerrainTypeController {
	@Autowired
	private TerrainService terrainService;
	
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public Type getTerrainById(@PathVariable("id") int id) {
		return terrainService.findTerrainTypeById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Type> getAllTerrains() {
	
		return terrainService.getTerrainTypes();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)//tre.post
	public int insertTerrain(@RequestBody Type terrainDTO)
	{
		int val=terrainService.createType(terrainDTO);
		terrainDTO.setId(val);
		
		
		System.out.println("ceva\n");
		return val;
		
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteTerrain(@PathVariable("id")int id)
	{
		terrainService.deleteTerrainType(id);
	}
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public void updateTerrain(@PathVariable("id") int terrainId,@RequestBody Type terrainDTO)
	{
		//in req 
	//	TerrainDTO terrainDTO=new TerrainDTO(terrainService.getTypes().get(1),null, 2450,"Str. de sus, nr 326,23534, Cluj,RO");
		//terrainDTO.setId(16);
		//		
		terrainDTO.setId(terrainId);
		terrainService.updateType(terrainDTO);
		
		
		System.out.println("ceva\n");
		
	}

}
