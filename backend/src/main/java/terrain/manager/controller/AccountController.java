package terrain.manager.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import terrain.manager.dto.ClientDTO;
import terrain.manager.dto.LoginDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.dto.UserDTO;
import terrain.manager.entities.Authentificable;
import terrain.manager.security.LoggedInChecker;
import terrain.manager.services.ClientService;
import terrain.manager.services.RepartitionService;
import terrain.manager.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private RepartitionService repartitionService;
	
	@Autowired
	private LoggedInChecker loggedInChecker;
	
	
	
	public Boolean isCurrentUserLoggedIn(){
		return loggedInChecker.getLoggedInUser()!=null;
	}
	public Authentificable getCurrentUser(){
		return loggedInChecker.getLoggedInUser();
	}
	public static LoginDTO getPrincipal(){
		 LoginDTO log=new LoginDTO();
	        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	       // Object credentials = SecurityContextHolder.getContext().getAuthentication().getCredentials();
	        if (principal instanceof UserDetails) {
	        	System.out.println("login data:"+((UserDetails)principal).getUsername());
	        	System.out.println("authorities:"+((UserDetails)principal).getAuthorities());
	        	log.setUsername(((UserDetails)principal).getUsername());
	        	Collection<? extends GrantedAuthority> authorities = ((UserDetails)principal).getAuthorities();
	        	for (GrantedAuthority grantedAuthority : authorities) {
	                if (grantedAuthority.getAuthority().startsWith("userId")) {
                	 try{
                		 log.setId(Integer.parseInt(grantedAuthority.getAuthority().substring(6)));
         	        	System.out.println("log.id:"+log.getId());
         	            }catch (Exception e)
         	            {
         	            	log.setId(-1);
         	            }
	                    
	                    
	                }
	                if(grantedAuthority.getAuthority().startsWith("type:")){
	                	log.setType(grantedAuthority.getAuthority().substring(5));
	                }
	            }
	           
	           
	        	return log;		
	        } 
	       
	        
	        return null;
	    }
	@RequestMapping(value="/LoginCheck",method=RequestMethod.GET)
	ResponseEntity<Boolean> isCurrentUserLoggedInRest(){
		return new ResponseEntity<>(isCurrentUserLoggedIn(),HttpStatus.OK);
	}
	@RequestMapping(value="/getLoginData",method=RequestMethod.GET)
	ResponseEntity<Authentificable> getCurrentUserRest(){
		return ResponseEntity.ok(getCurrentUser());
	}
	@RequestMapping(value = "/user/getTypes", method = RequestMethod.GET)
	public List<String> getTypes() {
		return userService.getTypes();
	}
	
	@RequestMapping(value = "/user/details", method = RequestMethod.GET)
	public UserDTO getUserByAccount() {
		
		LoginDTO account=getPrincipal();
		if(account==null)
	 	{
	 		return null;
	 	}
		UserDTO user=null;
		//if(account.getId()>0)
			user=userService.findUserById(account.getId());
		//else user=userService.login(account);
	 	return user;
		
	}
	
	@RequestMapping(value = "/client/terrains", method = RequestMethod.GET)
	public List<TerrainDTO> getTerrainsByAccount() {
		
		LoginDTO account=getPrincipal();
		if(account==null)
	 	{
	 		return null;
	 	}
		
		//if(account.getId()>0)
		List<TerrainDTO> terrains=repartitionService.findTerrainsByClientId(account.getId());
		//else user=userService.login(account);
	 	return terrains;
		
	}
	@RequestMapping(value = "/client/terrainList/{type}", method = RequestMethod.GET)
	public List<TerrainDTO> getTerrainByType(@PathVariable("type") String type) {
		LoginDTO account=getPrincipal();
		if(account==null)
	 	{
	 		return null;
	 	}
		
		//if(account.getId()>0)
		List<TerrainDTO> terrains=repartitionService.findTerrainsByClientId(account.getId());
		List<TerrainDTO> toreturn = new ArrayList<TerrainDTO>();
		for(TerrainDTO terr:terrains)
		{
			if(terr.getType().equals(type))
				toreturn.add(terr);
		}
		
		return toreturn;
		
	}
	@RequestMapping(value = "/client/details", method = RequestMethod.GET)
	public ClientDTO getClientByAccount() {
		LoginDTO account=getPrincipal();
		if(account==null)
	 	{
	 		return null;
	 	}
		ClientDTO user=clientService.findClientById(account.getId());
	 	return user;
	}
	@RequestMapping(value = "client/detailsInfo/{id}", method = RequestMethod.GET)
	public ClientDTO getClientInfoById(@PathVariable("id") int id) {
		return clientService.findClientInformationById(id);
	}
	
	@RequestMapping(value = "user/delete/{id}", method=RequestMethod.GET)
	public void deleteUser(@PathVariable("id")int id)
	{
		userService.deleteUser(id);
	}
	
	@RequestMapping(value="user/update/{id}",method=RequestMethod.POST)
	public void updateUserProfile(@PathVariable("id") int userId,@RequestBody UserDTO user){
		user.setId(userId);
		System.out.println(user+" pass:"+user.getPassword());
		userService.update(user);
	}
	
	
	
	@RequestMapping(value = "client/updateAcc/{id}", method = RequestMethod.POST)//tre.post
	public void updateClientAcc(@PathVariable("id") int userId,@RequestBody ClientDTO clientDTO)
	{
		//in req 
		//ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Ploiesti,Ploiesti, Str Facliei, nr 325","other",1350,null,"argc","argv","adi.pacurarbg@gmail.com");
		//clientDTO.setId(1);
		//
		clientDTO.setId(userId);
		System.out.println("ceva\n");
		clientService.updateAccount(clientDTO);
		
	}
}

