package terrain.manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import terrain.manager.dto.LoginDTO;
import terrain.manager.dto.UserDTO;
import terrain.manager.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public UserDTO loginUser(@RequestBody LoginDTO user){
		//code processing the input parameters
		
		System.out.println("ceva\n");
		
		//String response ="JSON: The user firstname:"+user.getFirstname()+", surname:"+user.getSurname()+", address:"+user.getAddress()+", email:"+user.getEmail();
		return userService.login(user);
	}
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("id") int id) {
		return userService.findUserById(id);
	}
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public List<UserDTO> getUserByType(@PathVariable("type") String type) {
		return userService.findUserByType(type);
	}
	@RequestMapping(value = "/getTypes", method = RequestMethod.GET)
	public List<String> getTypes() {
		return userService.getTypes();
	}
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers() {
	
		return userService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody UserDTO userDTO)
	{
		//in req
		//UserDTO userDTO=new UserDTO("Konay","Diana","adi.pacurarbg@gmail.com","Str. de jos, nr 326","23534","Cluj","RO", "0542345346", 2450,"passparola",userService.getTypes().get(1),"didiana");
	
		//
		System.out.println("ceva\n");
		return userService.create(userDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteUser(@PathVariable("id")int id)
	{
		userService.deleteUser(id);
	}
	
	/*@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)//tre.post
	public void updateUser(@PathVariable("id") int userId)//@RequestBody UserDTO userDTO)
	{
		//in req 
		UserDTO userDTO= userService.findUserById(userId);
		//UserDTO userDTO=new UserDTO("Kona","Diana","adi.pacurarbg@gmail.com","Str. de jos, nr 326","23534","Cluj","RO", "0542345346", 2450,"passparola",userService.getTypes().get(1));
		//userDTO.setId(16);
		//
		userDTO.setSalary(2100);
		//System.out.println("ceva\n");
		userService.update(userDTO);
	}*/
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public void updateUserProfile(@PathVariable("id") int userId,@RequestBody UserDTO user){
		user.setId(userId);
		
		
		userService.update(user);
		//String response="{\"message\":\"Post with ngResource - id: "+String.valueOf(userId)+", firstname: "+user.getFirstname()+", lastname: "+user.getSurname()+", address: "+user.getAddress()+
		//", email: "+user.getEmail()+"\"}";
		
	}
}

