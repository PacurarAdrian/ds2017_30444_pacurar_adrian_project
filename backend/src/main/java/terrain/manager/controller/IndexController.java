package terrain.manager.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.dto.LoginDTO;
import terrain.manager.entities.ClientAccount;
import terrain.manager.errorhandler.EntityValidationException;
import terrain.manager.services.ClientService;
import terrain.manager.controller.AccountController;
@CrossOrigin(maxAge = 3600)
@RestController
public class IndexController {
	
	@Autowired
	ClientService clientservice;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public int start() {
		return  1;
	}
	
	 @RequestMapping(value="/custom-logout", method = RequestMethod.GET)
     public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("dasgas");
        if (auth != null){    
           new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "you have been logged out";
     }
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public void loginPage(HttpServletResponse response) throws IOException {
		 response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
	       // return "login";
	    }
	 @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	    public String loginFailedPage() {
	        return "loginfailed";
	    }
	 @RequestMapping(value = "/getAccount", method = RequestMethod.GET)
	    public LoginDTO getAccount() {
		 	LoginDTO account=AccountController.getPrincipal();
		 	if(account==null)
		 	{
		 		List<String> validationErrors = Arrays.asList("Incorrect username or password");
				
				throw new EntityValidationException(ClientAccount.class.getSimpleName(),validationErrors);
		 	}
		 	System.out.println("login data:"+account.getPassword()+account.getUsername());
		 	return account;
		 	
		 	/*ClientDTO cli=clientservice.login(account);
		 	if(cli!=null)
		 	{
		 		account.setId(cli.getId());
		 		account.setType("client");
		 		return account;
		 	}
		 	UserDTO user=userService.login(account);
		 	if(user!=null)
		 	{
		 		account.setId(user.getId());
		 		if(user.getType().equalsIgnoreCase("admin"))
		 			account.setType("admin");
		 		else account.setType("user");
		 		return account;
		 	}*/
		 	
			//}
	        //return null;
	    }
	 

}
