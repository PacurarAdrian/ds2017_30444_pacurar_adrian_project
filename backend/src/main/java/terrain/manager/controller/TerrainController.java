package terrain.manager.controller;

import java.util.List;

import javax.management.Notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.dto.HistoryDTO;
import terrain.manager.dto.LoginDTO;
import terrain.manager.dto.TerrainDTO;
import terrain.manager.errorhandler.ServiceNotFoundException;
import terrain.manager.services.HistoryService;
import terrain.manager.services.TerrainService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/terrain")
public class TerrainController {
	@Autowired
	private TerrainService terrainService;
	@Autowired
	private HistoryService histService;
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public TerrainDTO getTerrainById(@PathVariable("id") int id) {
		return terrainService.findTerrainById(id);
	}
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public List<TerrainDTO> getTerrainByType(@PathVariable("type") String type) {
		return terrainService.findTerrainByType(type);
	}
	@RequestMapping(value = "/getTypes", method = RequestMethod.GET)
	public List<String> getTypes() {
		return terrainService.getStringTypes();
	}
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<TerrainDTO> getAllTerrains() {
	
		return terrainService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)//tre.post
	public int insertTerrain(@RequestBody TerrainDTO terrainDTO)
	{
		
		LoginDTO account=AccountController.getPrincipal();
		if(account==null)
	 	{
	 		return -1;
	 	}
		
		int val = 0;
		try{
			val=terrainService.create(terrainDTO);
			terrainDTO.setId(val);
		}catch(ServiceNotFoundException e)
		{
			if(!e.getMessage().equals(Notification.class.getSimpleName()))
				throw e;
		}
		HistoryDTO historyDTO=new HistoryDTO();
		historyDTO.setClientId(0);
		historyDTO.setTerrainId(terrainDTO.getId());
		historyDTO.setUserId(account.getId());
		historyDTO.setOperation("Insert new land title");
		histService.create(historyDTO);
		
		System.out.println("ceva\n");
		return val;
		
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteTerrain(@PathVariable("id")int id)
	{
		LoginDTO account=AccountController.getPrincipal();
		TerrainDTO terrainDTO=terrainService.findTerrainById(id);
		try{
		terrainService.deleteTerrain(id);
		}catch(ServiceNotFoundException e)
		{
			if(!e.getMessage().equals(Notification.class.getSimpleName()))
				throw e;
		}
		
		HistoryDTO historyDTO=new HistoryDTO();
		Integer clientId=terrainService.findTerrainById(id).getOwnerID();
		if(clientId!=null)
			historyDTO.setClientId(clientId);
		else historyDTO.setClientId(0);
		historyDTO.setTerrainId(terrainDTO.getId());
		historyDTO.setUserId(account.getId());
		historyDTO.setOperation("Deleted land title information");
		histService.create(historyDTO);
	}
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public void updateTerrain(@PathVariable("id") int terrainId,@RequestBody TerrainDTO terrainDTO)
	{
		//in req 
	//	TerrainDTO terrainDTO=new TerrainDTO(terrainService.getTypes().get(1),null, 2450,"Str. de sus, nr 326,23534, Cluj,RO");
		//terrainDTO.setId(16);
		//
		LoginDTO account=AccountController.getPrincipal();
		
		terrainDTO.setId(terrainId);
		
		try{
		terrainService.update(terrainDTO);
		}catch(ServiceNotFoundException e)
		{
			if(!e.getMessage().equals(Notification.class.getSimpleName()))
				throw e;
		}
		
		HistoryDTO historyDTO=new HistoryDTO();
		Integer clientId=terrainService.findTerrainById(terrainId).getOwnerID();
		if(clientId!=null)
			historyDTO.setClientId(clientId);
		else historyDTO.setClientId(0);
		historyDTO.setTerrainId(terrainDTO.getId());
		historyDTO.setUserId(account.getId());
		historyDTO.setOperation("Update land title information");
		histService.create(historyDTO);
		System.out.println("ceva\n");
		
	}

}
