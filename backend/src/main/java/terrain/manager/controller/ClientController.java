package terrain.manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import terrain.manager.dto.ClientDTO;
import terrain.manager.services.ClientService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	private ClientService userService;
	
	
//	@RequestMapping(value="/login",method=RequestMethod.POST)
//	public ClientDTO loginUser(@RequestBody LoginDTO user){
//		//code processing the input parameters
//		
//		System.out.println("ceva\n");
//		
//		//String response ="JSON: The user firstname:"+user.getFirstname()+", surname:"+user.getSurname()+", address:"+user.getAddress()+", email:"+user.getEmail();
//		return userService.login(user);
//	}
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ClientDTO getClientById(@PathVariable("id") int id) {
		return userService.findClientAccountById(id);
	}
	@RequestMapping(value = "/detailsInfo/{id}", method = RequestMethod.GET)
	public ClientDTO getClientInfoById(@PathVariable("id") int id) {
		return userService.findClientInformationById(id);
	}
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public List<ClientDTO> getClientByType(@PathVariable("type") String type) {
		return userService.findClientByType(type);
	}
	@RequestMapping(value = "/getId/{pnc}", method = RequestMethod.GET)
	public int getClientByPnc(@PathVariable("pnc") String pnc) {
		return userService.findClientIdByPNC(pnc);
	}
	@RequestMapping(value = "/getTypes", method = RequestMethod.GET)
	public List<String> getTypes() {
		return userService.getTypes();
	}
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ClientDTO> getAllClients() {
	
		return userService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)//tre.post
	public int insertClient(@RequestBody ClientDTO clientDTO)
	{
		//in req
		//ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Cluj,Cluj, Str Andrei Saguna","regular",1000,new Date(),"argc","argv","adi.pacurarbg@gmail.com");
	
		//
		System.out.println("ceva\n");
		return userService.create(clientDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method=RequestMethod.GET)
	public void deleteClient(@PathVariable("id")int id)
	{
		userService.deleteClient(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)//tre.post
	public void updateClient(@PathVariable("id") int userId,@RequestBody ClientDTO clientDTO)
	{
		//in req 
		//ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Ploiesti,Ploiesti, Str Facliei, nr 325","other",1350,null,"argc","argv","adi.pacurarbg@gmail.com");
		//clientDTO.setId(1);
		//
		clientDTO.setId(userId);
		System.out.println("ceva\n");
		ClientDTO backup=userService.findClientAccountById(userId);
		userService.updateAccount(clientDTO);
		try{
			userService.updateInfo(clientDTO);
		}catch(Exception e)
		{
			userService.updateAccount(backup);
		}
		
	}
	
	@RequestMapping(value = "/updateAcc/{id}", method = RequestMethod.POST)//tre.post
	public void updateClientAcc(@PathVariable("id") int userId,@RequestBody ClientDTO clientDTO)
	{
		//in req 
		//ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Ploiesti,Ploiesti, Str Facliei, nr 325","other",1350,null,"argc","argv","adi.pacurarbg@gmail.com");
		//clientDTO.setId(1);
		//
		clientDTO.setId(userId);
		System.out.println("ceva\n");
		userService.updateAccount(clientDTO);
		
	}
	@RequestMapping(value = "/updateInfo/{id}", method = RequestMethod.POST)//tre.post
	public void updateClientInfo(@PathVariable("id") int userId,@RequestBody ClientDTO clientDTO)
	{
		//in req 
		//ClientDTO clientDTO=new ClientDTO("Ion Agarboviceanu","23463","1285938569","Jud Ploiesti,Ploiesti, Str Facliei, nr 325","other",1350,null,"argc","argv","adi.pacurarbg@gmail.com");
		//clientDTO.setId(1);
		//
		clientDTO.setId(userId);
		System.out.println("ceva\n");
		userService.updateInfo(clientDTO);
	}
}
