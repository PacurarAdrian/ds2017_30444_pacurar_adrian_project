package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.ClientAccount;

public interface ClientAccountRepository extends JpaRepository<ClientAccount, Integer> {

	ClientAccount findByUsername(String name);
	
	List<ClientAccount> findByType(String type);

	ClientAccount findById(int id);
	
	void delete(Integer id);
	
	void delete(ClientAccount entity);
	
	long count();
	
	<S extends ClientAccount> S save(S entity);

	ClientAccount findByInfoId(int id);
	
}
