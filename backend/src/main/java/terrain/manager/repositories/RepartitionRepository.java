package terrain.manager.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import terrain.manager.entities.Repartition;

public interface RepartitionRepository extends JpaRepository<Repartition, Integer> {

	@Query("SELECT h FROM Repartition h where h.purchaseDate between :dateStart and :dateEnd")
	List<Repartition> findByPurchaseDate(@Param("dateStart") Date start,@Param("dateEnd") Date end);

	Repartition findById(int id);
	
	List<Repartition> findByClientId(int id);
	
	List<Repartition> findByTerrainId(int id);
	
	void delete(Integer id);
	
	
	long count();
	
	<S extends Repartition> S save(S entity);
}
