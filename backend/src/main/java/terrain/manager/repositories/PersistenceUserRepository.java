package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.PersistenceUser;

public interface PersistenceUserRepository extends JpaRepository<PersistenceUser, Integer> {

	PersistenceUser findByName(String name);
	
	List<PersistenceUser> findByType(String type);

	PersistenceUser findById(int id);
	
	void delete(Integer id);
	
	void delete(PersistenceUser entity);
	
	long count();
	
	<S extends PersistenceUser> S save(S entity);

	
}
