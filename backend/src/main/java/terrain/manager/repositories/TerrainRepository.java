package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.Terrain;
import terrain.manager.entities.Type;

public interface TerrainRepository extends JpaRepository<Terrain, Integer> {
	Terrain findById(int id);
	
	Terrain findByOwnerID(int id);
	
	List<Terrain> findByType(Type type);
	
	void delete(Integer id);
	
	void delete(Terrain entity);
	
	long count();
	
	<S extends Terrain> S save(S entity);
	
}
