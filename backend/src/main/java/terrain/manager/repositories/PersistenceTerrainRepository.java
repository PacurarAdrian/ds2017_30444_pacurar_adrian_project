package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.PersistenceTerrain;

public interface PersistenceTerrainRepository extends JpaRepository<PersistenceTerrain, Integer> {
	PersistenceTerrain findById(int id);
	
	PersistenceTerrain findByPnc(int id);
	
	List<PersistenceTerrain> findByType(String type);
	
	void delete(Integer id);
	
	void delete(PersistenceTerrain entity);
	
	long count();
	
	<S extends PersistenceTerrain> S save(S entity);
	
}
