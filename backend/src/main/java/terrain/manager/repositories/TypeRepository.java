package terrain.manager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.Type;

public interface TypeRepository extends JpaRepository<Type, Integer> {
	Type findById(int id);
	
	Type findByName(String name);
	
	void delete(Integer id);
	
	void delete(Type entity);
	
	long count();
	
	<S extends Type> S save(S entity);
	
}
