package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByName(String name);
	
	List<User> findByType(String type);

	User findById(int id);
	
	void delete(Integer id);
	
	void delete(User entity);
	
	long count();
	
	<S extends User> S save(S entity);

	User findByUsername(String username);
}
