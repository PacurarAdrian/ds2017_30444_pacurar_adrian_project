package terrain.manager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.PersistenceClient;

public interface PersistenceClientRepository extends JpaRepository<PersistenceClient, Integer> {

	
	
	List<PersistenceClient> findByType(String type);

	PersistenceClient findById(int id);
	
	void delete(Integer id);
	
	void delete(PersistenceClient entity);
	
	long count();
	
	<S extends PersistenceClient> S save(S entity);

	PersistenceClient findByName(String name);

	PersistenceClient findByPnc(String pnc);
	
}
