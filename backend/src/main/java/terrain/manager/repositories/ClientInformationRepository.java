package terrain.manager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import terrain.manager.entities.ClientInformation;

public interface ClientInformationRepository extends JpaRepository<ClientInformation, Integer> {

	ClientInformation findByName(String name);
	

	ClientInformation findById(int id);
	
	void delete(Integer id);
	
	void delete(ClientInformation entity);
	
	long count();
	
	<S extends ClientInformation> S save(S entity);


	ClientInformation findByPnc(String pnc);
	

}
