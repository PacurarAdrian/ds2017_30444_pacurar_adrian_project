package terrain.manager.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import terrain.manager.entities.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {

	@Query("SELECT h FROM History h where h.date between :dateStart and :dateEnd")
	List<History> findByDate(@Param("dateStart") Date start,@Param("dateEnd") Date end);

	History findById(int id);
	
	void delete(Integer id);
	
	void delete(History entity);
	
	long count();
	
	<S extends History> S save(S entity);
}
