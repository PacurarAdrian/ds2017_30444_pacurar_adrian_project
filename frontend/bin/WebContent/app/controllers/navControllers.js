(function() {

	var homeModule = angular.module('navControllers', [ 'ngRoute' ])

	homeModule.config(function($routeProvider) {
		$routeProvider.when('/login:id', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeLoginController',
			controllerAs : "homeLogCtrl"
		})
		.when('/clientlogin:id', {
			templateUrl : 'app/views/clientview.html',
			controller : 'ClientLoginController',
			controllerAs : "clientLogCtrl"
		})
		.when('/', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeController',
			controllerAs : "homeCtrl"
		})
	});

	homeModule.controller('HomeLoginController',['$scope','$routeParams','loginFactory','UserFactory', function($scope,$routeParams,loginFactory,UserFactory) {
		$scope.message = "message from controler";
		
		var id = $routeParams.id;
		
		var promise = UserFactory.findById(id);
		$scope.user = null;
		promise.success(function(data) {
			$scope.user=loginFactory.saveCredentials(data);
		}).error(function(data, status, header, config) {
			alert("Problem occured while connecting to server: "+status);
		});
		
	
	  }]);
	homeModule.controller('ClientLoginController',['$scope','$routeParams','loginFactory','ClientFactory',
	    function($scope,$routeParams,loginFactory,ClientFactory) {
		$scope.message = "message from controler";
		
		var id = $routeParams.id;
		
		var promise = ClientFactory.findAccById(id);
		$scope.client = null;
		promise.success(function(data) {
			$scope.client=loginFactory.saveCredentials(data);
		}).error(function(data, status, header, config) {
			alert("Problem occured while connecting to server: "+status);
		});
		
	
	  }]);
	homeModule.controller('HomeController',['$scope','loginFactory', function($scope,loginFactory) {
		$scope.message = "message from controler";
		
		$scope.user=loginFactory.saveCredentials(null);
	
		
	
	  }]);

})();