(function() {

	var terrainsModule = angular.module('terrainControllers', [ 'ngRoute' ])
	var type="all";
	terrainsModule.config(function($routeProvider) {
		$routeProvider.when('/terrains', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'AllTerrainsController',
			controllerAs : "allTerrainsCtrl"
		}).when('/terrainGoSave',{
			templateUrl: 'app/views/terrain/terrain-insert.html',
			controller: 'GoSaveTerrainController',
			controllerAs: "goSaveTerrainCtrl"
		}).when('/terrain/:id', {
			templateUrl : 'app/views/terrain/terrain-details.html',
			controller : 'TerrainController',
			controllerAs : "terrainCtrl"
		}).when('/terrainUpdate/:id', {
			templateUrl : 'app/views/terrain/terrain-insert.html',
			controller : 'UpdateTerrainController',
			controllerAs : "updateTerrainCtrl"
		}).when('/terrainDelete/:id', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'deleteTerrainController',
			controllerAs : "deleteTerrainCtrl"
		}).when('/terrainType/:type', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'typeTerrainController',
			controllerAs : "typeTerrainCtrl"
		
				
		}).otherwise({
			templateUrl: 'app/views/terrain/terrain-list.html',
			controller: 'HttpController',
			controllerAs : "httpTerrainCtrl"
		})

	});
	terrainsModule.controller('TypeTerrainController', 
			function TypeController($location, $scope) {
				$scope.inTypeT = '-select-';
				this.types = ['-select-','all',"Recreational","transport","agricultural","residential","commercial"];
				 $scope.go=function go(path){
					 if(path=='all')
					{
						 $scope.inTypeT = '-select-';
						 $location.path('/terrains');
						type=path;
						
					}
					 else
					 {
						 $scope.inTypeT = '-select-';
						 $location.path('/terrainType/'+path);
						 type=path;
					 }
				}
			});
	
	terrainsModule.controller('AllTerrainsController', [ '$scope','$timeout', 'TerrainFactory',
			function($scope,$timeout, TerrainFactory) {
				$scope.terrains = [];
				$scope.status="loading";
				var promise = TerrainFactory.findAll();
				promise.success(function(data) {
					$scope.terrains = data;
					
					$timeout(function () {
						if($scope.terrains.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					alert("Problem occured while connecting to server: "+status);
				});

			} ]);
	terrainsModule.controller('typeTerrainController', [ '$scope','$timeout','$routeParams', 'TerrainFactory',
   			function($scope,$timeout,$routeParams, TerrainFactory) {
				var type = $routeParams.type;
   				$scope.terrains = [];
   				$scope.status="loading";
   				var promise = TerrainFactory.findByType(type);
   				promise.success(function(data) {
   					$scope.terrains = data;
   					
   					$timeout(function () {
						if($scope.terrains.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
   				}).error(function(data, status, header, config) {
   					$scope.status = "empty";
   					alert("Problem occured while connecting to server: "+status);
   				});

   			} ]);
	terrainsModule.controller('deleteTerrainController', [ '$scope','$timeout','$routeParams','$location', 'TerrainFactory',
		function($scope,$timeout,$routeParams,$location, TerrainFactory) {
		var id = $routeParams.id;
			$scope.terrains = [];
			$scope.status="loading";
			var promise = TerrainFactory.deleteTerrain(id);
			promise.success(function(data) {
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				alert("Problem occured while connecting to server: "+status);
			});

		} ]);
	terrainsModule.controller('TerrainController', [ '$scope', '$routeParams','TerrainFactory',
	         function($scope, $routeParams, TerrainFactory) {
				var id = $routeParams.id;
				
				var promise = TerrainFactory.findById(id);
				$scope.terrain = null;
				promise.success(function(data) {
					$scope.terrain = data;
				}).error(function(data, status, header, config) {
					alert("Problem occured while connecting to server: "+status);
				});
			} ]);
	terrainsModule.controller('GoSaveTerrainController', [ '$scope','$timeout','$location','TerrainFactory',
         function($scope,$timeout,$location, TerrainFactory) {
			
			var promise = TerrainFactory.getTypes();
			$scope.types = null;
			promise.success(function(data) {
				$scope.types = data;
			}).error(function(data, status, header, config) {
				alert("Problem occured while connecting to server: "+status);
			});
		$scope.saveTerrain=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainFactory.saveTerrain($scope.terrain);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
				 
			});
			res.error(function(data,status,headers,config){
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!");
			});
			
		}	
			
			
		} ]);
	terrainsModule.controller('UpdateTerrainController', [ '$scope','$location','$routeParams','TerrainFactory',
         function($scope,$location,$routeParams, TerrainFactory) {
		
		var promise1 = TerrainFactory.getTypes();
		$scope.types = [];
		promise1.success(function(data) {
			$scope.types = data;
		}).error(function(data, status, header, config) {
			alert("Problem occured while connecting to server: "+status);
		});
		var id = $routeParams.id;
		var promise = TerrainFactory.findById(id);
		$scope.terrain = null;
		promise.success(function(data) {
			$scope.terrain = data;
		}).error(function(data, status, header, config) {
			alert("Problem occured while connecting to server: "+status);
		});
		$scope.saveTerrain=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainFactory.updateTerrain(id,$scope.terrain);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
			});
			res.error(function(data,status,headers,config){
				
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!");
			});
			
		}	
			
			
		} ]);
	
	
	var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];

})();
