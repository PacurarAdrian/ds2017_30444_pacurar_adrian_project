(function() {

	var homeModule = angular.module('loginControllers', [ 'ngRoute' ])
	
	homeModule.service('loginFactory', function () {
		this.user=null;
		
		this.saveCredentials = function(value) {
			if(value!=null)
				this.user=value;
			
			 return this.user;
			
		};
		
	});
	homeModule.controller('loginController',['$scope','$window','AuthenticationFactory','ClientFactory','loginFactory', 
	    function($scope,$window,AuthenticationFactory,ClientFactory,loginFactory) {
		$scope.login=function(){
			$scope.message = "processing input...";
			var promise = AuthenticationFactory.login($scope.credentials);
			$scope.user = null;
			promise.success(function(data) {
				//var getDetails = AuthenticationFactory.loginDetails();
				//$scope.message = data;
				//getDetails.success(function(data) {
					//$scope.message = data;
					if(data!=null&&data!='')
					{
						if(data.type=='admin')
							$window.location.href='admin.html#/login'+data.id;
						if(data.type=='user')
							$window.location.href='user.html#/login'+data.id;
						if(data.type=='client')
							$window.location.href='client.html#/clientlogin'+data.id;
					}else $scope.message ="invalid credentials";
				/*}).error(function(data, status, header, config) {
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});	*/
				/*if(data!=null)
				{
					//loginFactory.saveCredentials(data);
					if(data.type=='admin')
						$window.location.href='admin.html#/login'+data.id;
					else $window.location.href='user.html#/login'+data.id;
				}else{
					promise=ClientFactory.loginClient($scope.credentials)
					promise.success(function(data) {
					if(data!=null)
					{
						$window.location.href='client.html#/clientlogin'+data.id;
						loginFactory.saveCredentials(data);
					}else	$scope.message ="invalid credentials";
					}).error(function(data, status, header, config) {
						if(data!=null)
						{
							alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
							$scope.message=data.details;
						}else alert("Could not connect to server!\n status:"+status);
					});	
				}*/
				
			}).error(function(data, status, header, config) {
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});	
		}
	  }]);
	homeModule.controller('logoutController',['$scope','$window','$location','loginFactory','AuthenticationFactory', 
	  function($scope,$window,$location,loginFactory,AuthenticationFactory) {
		$scope.user=loginFactory.saveCredentials(null);
		$scope.logoutUser=function(){
		
			var promise = AuthenticationFactory.logout();
			promise.success(function(data) {
				$scope.message = "";
				 var str = $location.absUrl();
				 var arr = str.split("/");
				$window.location.href='/'+arr[3];
				alert("You have been logged out successfully!");
				
			}).error(function(data, status, header, config) {
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});	
			
		}
	  }]);

})();