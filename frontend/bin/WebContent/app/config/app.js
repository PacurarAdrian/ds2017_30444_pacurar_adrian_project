(function() {
	var app = angular.module('terrainManagerAJS', [ 'ngRoute', 'configModule',
			'navControllers', 'userControllers' ,'terrainControllers', 'userServices',
			'terrainServices','loginControllers','clientControllers','clientServices',
			'authenticationServices'])
})();