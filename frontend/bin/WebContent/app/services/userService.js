(function() {
	var userServices = angular.module('userServices', []);

	userServices.factory('UserFactory', [ '$http', 'config',
			function($http, config) {

				var privateUserDetails = function(id) {
					return $http.get(config.API_URL + '/user/details/' + id);
				};
				var privateUserDelete = function(id) {
					return $http.get(config.API_URL + '/user/delete/' + id);
				};
				var privateUserUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/user/update/' + id,dataObj);
				};
				var privateUserList = function() {
					return $http.get(config.API_URL + '/user/all');
				};
				var privateUserListType = function(type) {
					return $http.get(config.API_URL + '/user/list/'+type);
				};
				var privateUserTypes = function() {
					return $http.get(config.API_URL + '/user/getTypes');
				};
				var privateUserLogin=function(dataObj) 
				{
					return $http.post(config.API_URL + '/user/login',dataObj); 
				};
				var privateUserSave=function(dataObj) 
				{
					return $http.post(config.API_URL + '/user/insert',dataObj); 
				};
				return {
					findById : function(id) {
						return privateUserDetails(id);
					},

					findAll : function() {
						return privateUserList();
					},
					findByType : function(type){
						return privateUserListType(type);
					},
					getTypes : function(){
						return privateUserTypes();
					
					},
					loginUser : function(dataObj){
						return privateUserLogin(dataObj)
					},
					saveUser : function(dataObj){
						return privateUserSave(dataObj)
					},
					deleteUser : function(id){
						return privateUserDelete(id)
					},
					updateUser : function(id,dataObj){
						return privateUserUpdate(id,dataObj)
					}
				};
			} ]);

})();