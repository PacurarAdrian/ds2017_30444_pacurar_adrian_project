(function() {
	var userServices = angular.module('authenticationServices', []);

	userServices.factory('AuthenticationFactory', [ '$http', 'config',
			function($http, config) {

				
				var privateLogin = function(credentials) {
					//return $http.post(config.API_URL + '/login',credentials);
					return $http({
					    method: 'POST',
					    url: config.API_URL + '/login',
					    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					    transformRequest: function(obj) {
					        var str = [];
					        for(var p in obj)
					        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					        return str.join("&");
					    },
					    data: {ssoId: credentials.username, password: credentials.password}
					}).success(function () {});
				};
				var privateLogout = function() {
					return $http.post(config.API_URL + '/custom-logout');
				};
				
				var privateLoginDetails=function() 
				{
					return $http.get(config.API_URL + '/getAccount'); 
				};
				
				return {
					login : function(id) {
						return privateLogin(id);
					},

					logout : function() {
						return privateLogout();
					},
					
					
					loginDetails : function(){
						return privateLoginDetails()
					}
				};
			} ]);

})();