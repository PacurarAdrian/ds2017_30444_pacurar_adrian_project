(function() {
	var repartitionServices = angular.module('repartitionServices', []);

	repartitionServices.factory('RepartitionFactory', [ '$http', 'config',
			function($http, config) {

				var privateRepartitionDetails = function(id) {
					return $http.get(config.API_URL + '/repartition/details/' + id);
				};
				var privateListClientTerrains = function(id) {
					return $http.get(config.API_URL + '/repartition/clientTerrains/' + id);
				};
				var privateRepartitionDelete = function(id) {
					return $http.get(config.API_URL + '/repartition/delete/' + id);
				};
				var privateRepartitionUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/repartition/update/' + id,dataObj);
				};
				var privateRepartitionList = function() {
					return $http.get(config.API_URL + '/repartition/all');
				};
				var privateRepartitionListDate = function(date) {
					return $http.get(config.API_URL + '/repartition/list/'+date);
				};
				var privateRepartitionSave=function(dataObj) 
				{
					return $http.post(config.API_URL + '/repartition/insert',dataObj); 
				};
				return {
					findById : function(id) {
						return privateRepartitionDetails(id);
					},
					findTerrainsByClientId : function(id) {
						return privateListClientTerrains(id);
					},
					findAll : function() {
						return privateRepartitionList();
					},
					findByDate : function(date){
						return privateRepartitionListDate(date);
					},
					
					
					saveRepartition : function(dataObj){
						return privateRepartitionSave(dataObj)
					},
					deleteRepartition : function(id){
						return privateRepartitionDelete(id)
					},
					updateRepartition : function(id,dataObj){
						return privateRepartitionUpdate(id,dataObj)
					}
				};
			} ]);

})();