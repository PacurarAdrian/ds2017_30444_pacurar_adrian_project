(function() {
	var terrainServices = angular.module('terrainServices', []);

	terrainServices.factory('TerrainFactory', [ '$http', 'config',
			function($http, config) {

				var privateTerrainDetails = function(id) {
					return $http.get(config.API_URL + '/terrain/details/' + id);
				};
				var privateTerrainDelete = function(id) {
					return $http.get(config.API_URL + '/terrain/delete/' + id);
				};
				var privateTerrainUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/terrain/update/' + id,dataObj);
				};
				var privateTerrainList = function() {
					return $http.get(config.API_URL + '/terrain/all');
				};
				var privateTerrainListType = function(type) {
					return $http.get(config.API_URL + '/terrain/list/'+type);
				};
				var privateTerrainTypes = function() {
					return $http.get(config.API_URL + '/terrain/getTypes');
				};
				
				var privateTerrainSave=function(dataObj) 
				{
					return $http.post(config.API_URL + '/terrain/insert',dataObj); 
				};
				return {
					findById : function(id) {
						return privateTerrainDetails(id);
					},

					findAll : function() {
						return privateTerrainList();
					},
					findByType : function(type){
						return privateTerrainListType(type);
					},
					getTypes : function(){
						return privateTerrainTypes();
					
					},
					saveTerrain : function(dataObj){
						return privateTerrainSave(dataObj)
					},
					deleteTerrain : function(id){
						return privateTerrainDelete(id)
					},
					updateTerrain : function(id,dataObj){
						return privateTerrainUpdate(id,dataObj)
					}
				};
			} ]);

})();