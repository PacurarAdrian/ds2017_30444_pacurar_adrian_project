(function() {
	var userServices = angular.module('authenticationServices', []);

	userServices.factory('AuthenticationFactory', [ '$http','$window','$location', 'config',
			function($http,$window,$location,config) {

				
				var privateLogin = function(credentials) {
					//return $http.post(config.API_URL + '/login',credentials);
					return $http({
					    method: 'POST',
					    url: config.API_URL + '/login',
					    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					    transformRequest: function(obj) {
					        var str = [];
					        for(var p in obj)
					        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					        return str.join("&");
					    },
					    data: {ssoId: credentials.username, password: credentials.password}
					}).success(function () {});
				};
				var privateLogout = function() {
					return $http.get(config.API_URL + '/logout');
				};
				var privateUnauthorizedAccess=function(){
					
						var promise = $http.get(config.API_URL + '/logout');
						promise.success(function(data) {
						
							 var str = $location.absUrl();
							 var arr = str.split("/");
							$window.location.href='/'+arr[3];
							alert("You are not allowed to this page!");
							
						}).error(function(data, status, header, config) {
							if(data!=null)
							{
								alert("failure message while logging out:"+data.statusMessage);//JSON.stringify({data:data}));
								
							}else alert("Could not logout user!\n status:"+status);
						});	
						
					}
				var privateLoginDetails=function() 
				{
					return $http.get(config.API_URL + '/getAccount'); 
				};
				var privateLoggedUser=function() 
				{
					return $http.get(config.API_URL + '/account/user/details'); 
				};
				var privateLoggedClient=function() 
				{
					return $http.get(config.API_URL + '/account/client/details'); 
				};
				var privateClientTerrains=function() 
				{
					return $http.get(config.API_URL + '/account/client/terrains'); 
				};
				var privateClientTypeTerrains = function(type) {
					return $http.get(config.API_URL + '/account/client/terrainList/'+type);
				};
				var privateUserTypes = function() {
					return $http.get(config.API_URL + '/account/user/getTypes');
				};
				var privateUserUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/account/user/update/' + id,dataObj);
				};
				var privateClientUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/account/client/updateAcc/' + id,dataObj);
				};
				return {
					login : function(id) {
						return privateLogin(id);
					},

					logout : function() {
						return privateLogout();
					},
					
					
					loginDetails : function(){
						return privateLoginDetails();
					},
					unauthorized : function(){
						return privateUnauthorizedAccess();
					},
					loggedUserDetails:function(){
						return privateLoggedUser();
					},
					loggedClientDetails:function(){
						return privateLoggedClient();
					},
					loggedClientTerrains:function(){
						return privateClientTerrains();
					},
					clientTypeTerrains:function(type){
						return privateClientTypeTerrains(type);
					},
					getTypes : function(){
						return privateUserTypes();
					
					},
					updateUser : function(id,dataObj){
						return privateUserUpdate(id,dataObj)
					},
					updateClient : function(id,dataObj){
						return privateClientUpdate(id,dataObj)
					}
				};
			} ]);

})();