(function() {
	var clientServices = angular.module('clientServices', []);

	clientServices.factory('ClientFactory', [ '$http', 'config',
			function($http, config) {

				var privateClientAccDetails = function(id) {
					return $http.get(config.API_URL + '/client/details/' + id);
				};
				var privateUserLogin=function(dataObj) 
				{
					return $http.post(config.API_URL + '/user/login',dataObj); 
				};
				var privateClientInfoDetails = function(id) {
					return $http.get(config.API_URL + '/client/detailsInfo/' + id);
				};
				var privateClientDelete = function(id) {
					return $http.get(config.API_URL + '/client/delete/' + id);
				};
				var privateClientInfoUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/client/updateInfo/' + id,dataObj);
				};
				var privateClientAccUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/client/updateAcc/' + id,dataObj);
				};
				var privateClientList = function() {
					return $http.get(config.API_URL + '/client/all');
				};
				var privateClientListType = function(type) {
					return $http.get(config.API_URL + '/client/list/'+type);
				};
				var privateClientByPnc = function(pnc) {
					return $http.get(config.API_URL + '/client/getId/'+pnc);
				};
				var privateClientTypes = function() {
					return $http.get(config.API_URL + '/client/getTypes');
				};
				
				var privateClientSave=function(dataObj) 
				{
					return $http.post(config.API_URL + '/client/insert',dataObj); 
				};
				return {
					findInfoById : function(id) {
						return privateClientInfoDetails(id);
					},
					findAccById : function(id) {
						return privateClientAccDetails(id);
					},
					findAll : function() {
						return privateClientList();
					},
					findByType : function(type){
						return privateClientListType(type);
					},
					getId : function(pnc){
						return privateClientByPnc(pnc);
					},
					getTypes : function(){
						return privateClientTypes();
					
					},
					loginClient:function(dataObj){
						return privateUserLogin(dataObj)
					},
					saveClient : function(dataObj){
						return privateClientSave(dataObj)
					},
					deleteClient : function(id){
						return privateClientDelete(id)
					},
					updateClientInfo : function(id,dataObj){
						return privateClientInfoUpdate(id,dataObj)
					},
					updateClientAcc : function(id,dataObj){
						return privateClientAccUpdate(id,dataObj)
					}
				};
			} ]);

})();