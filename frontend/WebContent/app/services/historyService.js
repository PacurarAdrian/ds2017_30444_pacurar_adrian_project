(function() {
	var historyServices = angular.module('historyServices', []);

	historyServices.factory('HistoryFactory', [ '$http', 'config',
			function($http, config) {

				var privateHistoryDetails = function(id) {
					return $http.get(config.API_URL + '/history/details/' + id);
				};
				var privateHistoryDate = function(date) {
					return $http.get(config.API_URL + '/history/list/' + date);
				};
				var privateHistoryDelete = function(id) {
					return $http.get(config.API_URL + '/history/delete/' + id);
				};
				
				var privateHistoryList = function() {
					return $http.get(config.API_URL + '/history/all');
				};
				
				
				
				return {
					findById : function(id) {
						return privateHistoryDetails(id);
					},
					findByDate : function(date) {
						return privateHistoryDate(date);
					},
					findAll : function() {
						return privateHistoryList();
					},
					
					deleteHistory : function(id){
						return privateHistoryDelete(id)
					}
				};
			} ]);

})();