(function() {
	var terrainTypeServices = angular.module('terrainTypeServices', []);

	terrainTypeServices.factory('TerrainTypeFactory', [ '$http', 'config',
			function($http, config) {

				var privateTerrainDetails = function(id) {
					return $http.get(config.API_URL + '/terrainType/details/' + id);
				};
				var privateTerrainDelete = function(id) {
					return $http.get(config.API_URL + '/terrainType/delete/' + id);
				};
				var privateTerrainUpdate = function(id,dataObj) {
					return $http.post(config.API_URL + '/terrainType/update/' + id,dataObj);
				};
				var privateTerrainList = function() {
					return $http.get(config.API_URL + '/terrainType/all');
				};
				
				var privateTerrainSave=function(dataObj) 
				{
					return $http.post(config.API_URL + '/terrainType/insert',dataObj); 
				};
				return {
					findById : function(id) {
						return privateTerrainDetails(id);
					},

					findAll : function() {
						return privateTerrainList();
					},
				
					saveTerrain : function(dataObj){
						return privateTerrainSave(dataObj)
					},
					deleteTerrain : function(id){
						return privateTerrainDelete(id)
					},
					updateTerrain : function(id,dataObj){
						return privateTerrainUpdate(id,dataObj)
					}
				};
			} ]);

})();