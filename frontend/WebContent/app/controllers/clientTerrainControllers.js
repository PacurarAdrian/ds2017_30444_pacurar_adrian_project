(function() {

	var terrainsModule = angular.module('clientTerrainControllers', [ 'ngRoute' ])
	
	terrainsModule.config(function($routeProvider) {
		$routeProvider.when('/clientTerrains', {
			templateUrl : 'app/views/client/client-terrain-list.html',
			controller : 'AllTerrainsController',
			controllerAs : "allTerrainsCtrl"
	
		}).when('/clientTerrain/:id', {
			templateUrl : 'app/views/client/client-terrain-details.html',
			controller : 'TerrainController',
			controllerAs : "terrainCtrl"
		}).when('/clientTerrainType/:type', {
			templateUrl : 'app/views/client/client-terrain-list.html',
			controller : 'typeTerrainController',
			controllerAs : "typeTerrainCtrl"
		
				
		}).when('/clientBoughtTerrainType/:type', {
			templateUrl : 'app/views/client/client-terrain-list.html',
			controller : 'boughtTypeTerrainController',
			controllerAs : "typeTerrainCtrl"
		
				
		}).when('/clientBoughtTerrains', {
			templateUrl : 'app/views/client/client-terrain-list.html',
			controller : 'boughtTerrainsController',
			controllerAs : "allTerrainsCtrl"
	
		}).otherwise({
			templateUrl: 'app/views/client/terrain-list.html',
			controller: 'HttpController',
			controllerAs : "httpTerrainCtrl"
		})

	});
	terrainsModule.controller('TypeClientTerrainController', 
		function TypeController($location, $scope) {
			$scope.inTypeT = '-select-';
			this.types = ['-select-','all',"Recreational","transport","agricultural","residential","commercial"];
			 $scope.go=function go(path){
				 if(path=='all')
				{
					 $scope.inTypeT = '-select-';
					 $location.path('/clientTerrains');
					
					
				}
				 else
				 {
					 $scope.inTypeT = '-select-';
					 $location.path('/clientTerrainType/'+path);
					 
				 }
			}
		});
	terrainsModule.controller('BoughtTypeTerrainController', 
			function TypeController($location, $scope) {
				$scope.inTypeB = '-select-';
				this.types = ['-select-','all',"Recreational","transport","agricultural","residential","commercial"];
				 $scope.go=function go(path){
					 if(path=='all')
					{
						 $scope.inTypeB = '-select-';
						 $location.path('/clientBoughtTerrains');
						
						
					}
					 else
					 {
						 $scope.inTypeB = '-select-';
						 $location.path('/clientBoughtTerrainType/'+path);
						 
					 }
				}
			});
	terrainsModule.controller('boughtTerrainsController', [ '$scope','$timeout', 'AuthenticationFactory',
	function($scope,$timeout, AuthenticationFactory) {
		$scope.terrains = [];
		$scope.status="loading";
		var promise = AuthenticationFactory.loggedClientTerrains();
		promise.success(function(data) {
			$scope.terrains = data;
			
			$timeout(function () {
				if($scope.terrains.length==0)
		        $scope.status = "empty";
				else $scope.status = "full";
		    }, 2000);
		}).error(function(data, status, header, config) {
			$scope.status = "empty";
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	terrainsModule.controller('boughtTypeTerrainController', [ '$scope','$timeout','$routeParams', 'AuthenticationFactory',
		function($scope,$timeout,$routeParams, AuthenticationFactory) {
		var type = $routeParams.type;
			$scope.terrains = [];
			$scope.status="loading";
			var promise = AuthenticationFactory.clientTypeTerrains(type);
			promise.success(function(data) {
				$scope.terrains = data;
				
				$timeout(function () {
				if($scope.terrains.length==0)
		        $scope.status = "empty";
				else $scope.status = "full";
		    }, 2000);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	
	

})();
