(function() {

	var historyModule = angular.module('historyControllers', [ 'ngRoute','userControllers' ])
	var type="all";
	historyModule.config(function($routeProvider) {
		$routeProvider.when('/history', {
			templateUrl : 'app/views/history/history-list.html',
			controller : 'AllHistoryController',
			controllerAs : "allHistoryCtrl"
		}).when('/historyChart', {
			templateUrl : 'app/views/history/history-chart.html',
			controller : 'ChartDateCtrl',
			controllerAs : "chartLineCtrl"
		
		
		}).when('/historyByDate/:date', {
			templateUrl : 'app/views/history/history-list.html',
			controller : 'HistoryDateController',
			controllerAs : "historyDateCtrl"
		
		}).when('/history/:id', {
			templateUrl : 'app/views/history/history-details.html',
			controller : 'HistoryController',
			controllerAs : "historyCtrl"
		
		}).when('/historyDelete/:id', {
			templateUrl : 'app/views/history/history-list.html',
			controller : 'deleteHistoryController',
			controllerAs : "deleteHistoryCtrl"		
		})

	});
	historyModule.controller('ChartDateCtrl',['$scope','$timeout', 'HistoryFactory','AuthenticationFactory',
		function($scope,$timeout,HistoryFactory,AuthenticationFactory){
	$scope.historys = [];
	$scope.status="loading";
	var promise = HistoryFactory.findAll();
	promise.success(function(data) {
		$scope.historys = data;
		
		$timeout(function () {
			if($scope.historys.length==0)
	        $scope.status = "empty";
			else{
				$scope.status = "full";
				computeChart(data,$scope);
				
			}
	    }, 2000);
	}).error(function(data, status, header, config) {
		$scope.status = "empty";
		if(status==401||status==403)//unauthorized
		{
			AuthenticationFactory.unauthorized();
			
		}else
		if(data!=null)
		{
			alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
			$scope.message=data.details;
		}else alert("Could not connect to server!\n status:"+status);
	});
}]);
	
	function computeChart(data,$scope){
			var items=[];
			var datasets;
			var map=[];
			$scope.series=[];
			 $scope.data=[];
			///items.push({name:data[0].user.firstname+" "+data[0].user.surname,nrop:0});
			//console.log(items[0].name, items[0].nrop);
			angular.forEach(data, function(item, index) {
				 // console.log(item.user.firstname+item.user.surname, index);
				  var foundIndex=-1; /*= items.findIndex(function(element){
					  return element.name == item.user.firstname+" "+item.user.surname; // x => x.id == item.id;
					  });*/
				  
				  var indexm=-1;
				  for (var i = 0; i < map.length; i++) {
				      if(map[i].name==item.user.firstname+" "+item.user.surname)
				      {
				    	  indexm=i;
				    	  break;
				      } 
				  } 
				  if(indexm==-1)
					{
					  
					  	$scope.series.push(item.user.firstname+" "+item.user.surname);//add names to series
					  	$scope.data[map.length]=[];//initialize scope.data
					  	//map user names to number values
						map.push({name:item.user.firstname+" "+item.user.surname,id:map.length})
						
					}
				  var arrayLength = items.length;
				  for (var i = 0; i < arrayLength; i++) {
				      if(items[i].date==item.date)
				      {
				    	  foundIndex=i;
				    	  break;
				      }
				      
				      
				  }
				  
				  if(foundIndex==-1)
				{
					  
					  var temp=[{name:item.user.firstname+" "+item.user.surname,nrop:1}];
					  items.push({date:item.date,oper:temp});
				}
				  else
				  {
					  var v=items[foundIndex].oper;
					  var username=item.user.firstname+" "+item.user.surname;
					  var index=-1;
					 
					  for (var i = 0; i < v.length; i++) {
					      if(v[i].name==username)
					      {
					    	  index=i;
					    	  break;
					      } 
					  }
					  if(index==-1)
					{
						  v.push({name:username,nrop:1});
						  items[foundIndex].oper=v;
						  if(datasets<v.length)
							  datasets=v.length;
							  
					}else{
						v[index].nrop++;
						 items[foundIndex].oper=v;
					}
					 
				  }
			});
			 $scope.labels=[];
			 console.log(map)
			 console.log($scope.series)
			 
			 
			angular.forEach(items, function(item, index) {
				  console.log(item.date, item.oper);
				  $scope.labels.push(item.date);
				  //$scope.series.push(item.name);
				  
				  for (var i = 0; i < map.length; i++) {
					  var ind=-1;
					  //var cnt=0;
					  //angular.forEach(item.oper, function(item, index) {
					for(var j=0;j<item.oper.length;j++){  
					  
					      if(map[i].name==item.oper[j].name)
					      {
					    	  ind=j;
					    	  
					      } 
					      
					  }
					  if(ind!=-1)
					  {
						  $scope.data[i].push(item.oper[ind].nrop);
					  }else $scope.data[i].push(0);
					  
				  
				  }
				  
			});
			 console.log($scope.data);
			
		
	}
	historyModule.controller('ChartSubsetCtrl',['$scope','$timeout',
      		function($scope,$timeout){
      	//$scope.historys = [];
		$scope.chartStatus="none";
      	//var promise = HistoryFactory.findAll();
      	//promise.success(function(data) {
      	$scope.displayChart=function(){
      		//$scope.historys = data;
      		var data=$scope.historys;
      		$scope.chartStatus="loading";
      		$timeout(function () {
      			if($scope.historys.length==0)
      	        $scope.chartStatus = "empty";
      			else{
      				$scope.chartStatus = "full";
      				computeChart(data,$scope);
      			}
      	    }, 2000);
      	}
      	
      }]);
	historyModule.controller('ChartLineCtrl',['$scope','$timeout', 'HistoryFactory','AuthenticationFactory',
			function($scope,$timeout,HistoryFactory,AuthenticationFactory){
		$scope.historys = [];
		$scope.status="loading";
		var promise = HistoryFactory.findAll();
		promise.success(function(data) {
			$scope.historys = data;
			
			$timeout(function () {
				if($scope.historys.length==0)
		        $scope.status = "empty";
				else{
					$scope.status = "full";
					var items=[];
					items.push({name:data[0].user.firstname+" "+data[0].user.surname,nrop:0});
					console.log(items[0].name, items[0].nrop);
					angular.forEach(data, function(item, index) {
						 // console.log(item.user.firstname+item.user.surname, index);
						  var foundIndex=-1; /*= items.findIndex(function(element){
							  return element.name == item.user.firstname+" "+item.user.surname; // x => x.id == item.id;
							  });*/
						  var arrayLength = items.length;
						  for (var i = 0; i < arrayLength; i++) {
						      if(items[i].name==item.user.firstname+" "+item.user.surname)
						      {
						    	  foundIndex=i;
						    	  break;
						      }
						      
						      
						  }
						  
						  if(foundIndex==-1)
							  items.push({name:item.user.firstname+" "+item.user.surname,nrop:1})
						  else
						  items[foundIndex].nrop ++;
					});
					 $scope.labels=[];
					  
					 $scope.data=[];
					angular.forEach(items, function(item, index) {
						  console.log(item.name, item.nrop);
						  $scope.labels.push(item.name);
						  
						  $scope.data.push(item.nrop);
					});
					$scope.series=["number of operations"];
					
				}
		    }, 2000);
		}).error(function(data, status, header, config) {
			$scope.status = "empty";
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		//$scope.labels=['2006','2007','2008','2009','2010','2011','2012'];
		//$scope.series=["Series A",'Series B'];
		//$scope.data=[[65,59,80,81,56,55,40],[28,48,40,19,86,27,90]];
	}]);
	
	historyModule.controller('AllHistoryController', [ '$scope','$timeout', 'HistoryFactory','AuthenticationFactory',
			function($scope,$timeout, HistoryFactory,AuthenticationFactory) {
				$scope.historys = [];
				$scope.showMe = false;
				$scope.status="loading";
				var promise = HistoryFactory.findAll();
				promise.success(function(data) {
					$scope.historys = data;
					computeChart(data,$scope);
					$timeout(function () {
						if($scope.historys.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
				
			    $scope.myFunc = function() {
			        $scope.showMe = !$scope.showMe;
			    }

			} ]);
	historyModule.controller('HistoryDateController', [ '$scope','$timeout','$routeParams', 'HistoryFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams, HistoryFactory,AuthenticationFactory) {
			var date = $routeParams.date;
			$scope.historys = [];
			$scope.showMe = false;
			$scope.status="loading";
			var promise = HistoryFactory.findByDate(date);
			promise.success(function(data) {
				$scope.historys = data;
				computeChart(data,$scope);
				$timeout(function () {
					if($scope.historys.length==0)
			        $scope.status = "empty";
					else $scope.status = "full";
			    }, 2000);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			$scope.myFunc = function() {
		        $scope.showMe = !$scope.showMe;
		    }
		} ]);
	historyModule.controller('deleteHistoryController', [ '$scope','$timeout','$routeParams','$location', 'HistoryFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, HistoryFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.historys = [];
			$scope.status="loading";
			var promise = HistoryFactory.deleteHistory(id);
			promise.success(function(data) {
				
			$location.path('/historys');
				
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	historyModule.controller('HistorySearchController',['$scope','$location', 
    	  function($scope,$location) {
    		
    		$scope.search=function(){
    		
    			
    			
    			if($scope.histSearch==null||$scope.histSearch=='')
				{	 
					 $location.path('/history');
				}
				 else
				 {
					 $location.path('/historyByDate/'+$scope.histSearch);
				 }
    			
    		}
    	  }]);
	historyModule.controller('HistoryController', [ '$scope', '$routeParams','HistoryFactory','AuthenticationFactory',
	         function($scope, $routeParams, HistoryFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = HistoryFactory.findById(id);
				$scope.history = null;
				promise.success(function(data) {
					$scope.history = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	
})();
