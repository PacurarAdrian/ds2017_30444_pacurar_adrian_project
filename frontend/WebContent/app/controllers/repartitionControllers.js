(function() {

	var repartitionsModule = angular.module('repartitionControllers', [ 'ngRoute' ])
	var type="all";
	repartitionsModule.config(function($routeProvider) {
		$routeProvider.when('/repartitions', {
			templateUrl : 'app/views/repartition/repartition-list.html',
			controller : 'AllRepartitionsController',
			controllerAs : "allRepartitionsCtrl"
		}).when('/repartitionByDate/:date', {
			templateUrl : 'app/views/repartition/repartition-list.html',
			controller : 'RepartitionDateController',
			controllerAs : "repartitionDateCtrl"
		
		}).when('/repartitionSave/:id',{
			templateUrl: 'app/views/repartition/repartition-list.html',
			controller: 'SaveRepartitionController',
			controllerAs: "SaveRepartitionCtrl"
		}).when('/repartition/:id', {
			templateUrl : 'app/views/repartition/repartition-details.html',
			controller : 'RepartitionController',
			controllerAs : "repartitionCtrl"
		}).when('/repartitionUpdate/:id', {
			templateUrl : 'app/views/repartition/repartition-list.html',
			controller : 'UpdateRepartitionController',
			controllerAs : "updateRepartitionCtrl"
		}).when('/repartitionDelete/:id', {
			templateUrl : 'app/views/repartition/repartition-list.html',
			controller : 'deleteRepartitionController',
			controllerAs : "deleteRepartitionCtrl"
		}).when('/repartitionType/:type', {
			templateUrl : 'app/views/repartition/repartition-list.html',
			controller : 'typeRepartitionController',
			controllerAs : "typeRepartitionCtrl"
		
				
		})

	});
	
	
	repartitionsModule.controller('AllRepartitionsController', [ '$scope','$timeout', 'RepartitionFactory','AuthenticationFactory',
			function($scope,$timeout, RepartitionFactory,AuthenticationFactory) {
				$scope.repartitions = [];
				$scope.status="loading";
				var promise = RepartitionFactory.findAll();
				promise.success(function(data) {
					$scope.repartitions = data;
					
					$timeout(function () {
						if($scope.repartitions.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});

			} ]);
	repartitionsModule.controller('RepartitionDateController', [ '$scope','$timeout','$routeParams', 'RepartitionFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams, RepartitionFactory,AuthenticationFactory) {
			var date = $routeParams.date;
			$scope.repartitions = [];
			$scope.status="loading";
			var promise = RepartitionFactory.findByDate(date);
			promise.success(function(data) {
				$scope.repartitions = data;
				
				$timeout(function () {
					if($scope.repartitions.length==0)
			        $scope.status = "empty";
					else $scope.status = "full";
			    }, 2000);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	repartitionsModule.controller('RepartitionSearchController',['$scope','$location', 
      	  function($scope,$location) {
      		
      		$scope.search=function(){
      		
      			
      			
      			if($scope.repSearch==null)
  				{	 
  					 $location.path('/repartitions');
  				}
  				 else
  				 {
  					 $location.path('/repartitionByDate/'+$scope.repSearch);
  				 }
      			
      		}
      	  }]);
	                                          	
	repartitionsModule.controller('deleteRepartitionController', [ '$scope','$timeout','$routeParams','$location', 'RepartitionFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, RepartitionFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.repartitions = [];
			$scope.status="loading";
			var promise = RepartitionFactory.deleteRepartition(id);
			promise.success(function(data) {
				if(type=='all')
					$location.path('/repartitions');
				else $location.path('/repartitionType/'+type);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	repartitionsModule.controller('RepartitionController', [ '$scope', '$routeParams','RepartitionFactory','AuthenticationFactory',
	         function($scope, $routeParams, RepartitionFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = RepartitionFactory.findById(id);
				$scope.repartition = null;
				promise.success(function(data) {
					$scope.repartition = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	repartitionsModule.controller('SaveRepartitionController', [ '$scope','$timeout','$location','$routeParams','RepartitionFactory','ClientFactory','AuthenticationFactory',
         function($scope,$timeout,$location,$routeParams, RepartitionFactory,ClientFactory,AuthenticationFactory) {
		
		
		var pnc = prompt("Please enter PNC of client", "PNC");
		var terrid = $routeParams.id;
	    if (pnc != null) {
	    	$scope.message="Processing data...";
	       
	       // uprep.id=data.id;
	        var reqId=ClientFactory.getId(pnc);
	        reqId.success(function(clientId){
	        	var uprep={terrain:terrid,client:clientId};	
	        	var res = RepartitionFactory.saveRepartition(uprep);
		
	        	res.success(function(data,status,headers,config){
	        		$location.path('/repartitions');
			
	        	});
	        	res.error(function(data,status,headers,config){
	        		$location.path('/terrains');
	        		if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage+"\n"+data.details);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
	        	});
	        }).error(function(data,status,headers,config){
    	
	        	alert("Invalid PNC inserted");
	        	$location.path('/terrains');
	        });
    
	    }else alert("Must enter PNC!");
		
	    $location.path('/terrains');
			
		} ]);
	repartitionsModule.controller('UpdateRepartitionController', [ '$scope','$location','$routeParams','RepartitionFactory','ClientFactory','AuthenticationFactory',
         function($scope,$location,$routeParams, RepartitionFactory,ClientFactory,AuthenticationFactory) {
		
		
		var id = $routeParams.id;
		var promise = RepartitionFactory.findById(id);
		//$scope.repartition = null;
		promise.success(function(data) {
			//$scope.repartition = data;
			var pnc = prompt("Please enter PNC of client", "PNC");
			
		    if (pnc != null) {
		    	$scope.message="Processing data...";
		       
		       // uprep.id=data.id;
		        var reqId=ClientFactory.getId(pnc);
		        reqId.success(function(clientId){
		        	 var uprep={id:id,terrain:data.terrain.id,client:clientId};
		        	
		        	var res = RepartitionFactory.updateRepartition(id,uprep);
					
					res.success(function(data,status,headers,config){
						
						
							$location.path('/repartitions');
						
					});
					res.error(function(data,status,headers,config){
						$location.path('/repartitions');
						
						if(status==401||status==403)//unauthorized
						{
							AuthenticationFactory.unauthorized();
							
						}else
						if(data!=null)
						{
							alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
							$scope.message=data.details;
						}else alert("Could not connect to server!\n status:"+status);
					});
		        }).error(function(data,status,headers,config){
		        	
		        	alert("Invalid PNC inserted");
		        	$location.path('/repartitions');
		        });
		        
		    }else alert("Must enter PNC!");
		    $location.path('/repartitions');
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
			
			
			
		} ]);
	

})();
