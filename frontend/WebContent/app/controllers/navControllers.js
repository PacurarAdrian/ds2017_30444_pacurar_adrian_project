(function() {

	var homeModule = angular.module('navControllers', [ 'ngRoute' ])

	homeModule.config(function($routeProvider) {
		$routeProvider/*.when('/login:id', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeLoginController',
			controllerAs : "homeLogCtrl"
		})*/
		.when('/accountUpdate/:id', {
			templateUrl : 'app/views/user/user-account.html',
			controller : 'UpdateAccountController',
			controllerAs : "updateAccCtrl"
		}).when('/accountUpdateClient/:id', {
			templateUrl : 'app/views/client/client-accUpdate.html',
			controller : 'UpdateClientAccountController',
			controllerAs : "updateClientAccCtrl"
		}).when('/client', {
			templateUrl : 'app/views/clientview.html',
			controller : 'ClientHomeController',
			controllerAs : "clientHomeCtrl"
		})
		.when('/', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeController',
			controllerAs : "homeCtrl"
		})
	});
	homeModule.controller('UpdateAccountController', [ '$scope','$location','$routeParams','AuthenticationFactory',
         function($scope,$location,$routeParams, AuthenticationFactory) {
		
		/*var promise1 = AuthenticationFactory.getTypes();
		$scope.types = [];
		promise1.success(function(data) {
			$scope.types = data;
		}).error(function(data, status, header, config) {
			
			alert("Problem occured while connecting to server: "+status);
		});*/
		var id = $routeParams.id;
		var promise = AuthenticationFactory.loggedUserDetails();
		$scope.user = null;
		promise.success(function(data) {
			$scope.user = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			alert("Problem occured while connecting to server: "+status);
		});
		$scope.saveUser=function(){
			
			$scope.message="Processing data...";
			
			var res = AuthenticationFactory.updateUser(id,$scope.user);
			
			res.success(function(data,status,headers,config){
				
				
				$location.path('/');
				
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	homeModule.controller('UpdateClientAccountController', [ '$scope','$location','$routeParams','AuthenticationFactory',
        function($scope,$location,$routeParams, AuthenticationFactory) {
  		
  		var id = $routeParams.id;
  		var promise = AuthenticationFactory.loggedClientDetails();
  		$scope.client = null;
  		promise.success(function(data) {
  			$scope.client = data;
  		}).error(function(data, status, header, config) {
  			if(status==401||status==403)//unauthorized
  			{
  				AuthenticationFactory.unauthorized();
  				
  			}else
  			alert("Problem occured while connecting to server: "+status);
  		});
  		$scope.saveClient=function(){
  			
  			$scope.message="Processing data...";
  			
  			var res = AuthenticationFactory.updateClient(id,$scope.client);
  			
  			res.success(function(data,status,headers,config){
  				
  				
  				$location.path('/client');
  				
  			});
  			res.error(function(data,status,headers,config){
  				
  				if(status==401||status==403)//unauthorized
  				{
  					AuthenticationFactory.unauthorized();
  					
  				}else
  				if(data!=null)
  				{
  					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
  					$scope.message=data.details;
  				}else alert("Could not connect to server!\n status:"+status);
  			});
  			
  		}	
  			
  			
  		} ]);
	homeModule.controller('HomeController',['$scope','AuthenticationFactory',
	    function($scope,AuthenticationFactory) {
		$scope.message = "message from controler";
		
		var promise = AuthenticationFactory.loggedUserDetails();
		$scope.user = null;
		promise.success(function(data) {
			$scope.user=data
		}).error(function(data, status, header, config) {
			//alert("Problem occured while connecting to server: "+status);
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		
	
	  }]);
	homeModule.controller('ClientHomeController',['$scope','AuthenticationFactory',
	    function($scope,AuthenticationFactory) {
		$scope.message = "message from controler";
		
		var promise = AuthenticationFactory.loggedClientDetails();
		$scope.client = null;
		promise.success(function(data) {
			$scope.client=data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		
	
	  }]);
	/*homeModule.controller('HomeController',['$scope','loginFactory', function($scope,loginFactory) {
		$scope.message = "message from controler";
		
		$scope.user=loginFactory.saveCredentials(null);
	
		
	
	  }]);*/

})();