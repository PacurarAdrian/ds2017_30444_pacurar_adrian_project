(function() {

	var terrainsModule = angular.module('terrainControllers', [ 'ngRoute' ])
	var type="all";
	terrainsModule.config(function($routeProvider) {
		$routeProvider.when('/terrains', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'AllTerrainsController',
			controllerAs : "allTerrainsCtrl"
		}).when('/terrainGoSave',{
			templateUrl: 'app/views/terrain/terrain-insert.html',
			controller: 'GoSaveTerrainController',
			controllerAs: "goSaveTerrainCtrl"
		}).when('/terrain/:id', {
			templateUrl : 'app/views/terrain/terrain-details.html',
			controller : 'TerrainController',
			controllerAs : "terrainCtrl"
		}).when('/terrainUpdate/:id', {
			templateUrl : 'app/views/terrain/terrain-insert.html',
			controller : 'UpdateTerrainController',
			controllerAs : "updateTerrainCtrl"
		}).when('/terrainDelete/:id', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'deleteTerrainController',
			controllerAs : "deleteTerrainCtrl"
		}).when('/terrainType/:type', {
			templateUrl : 'app/views/terrain/terrain-list.html',
			controller : 'typeTerrainController',
			controllerAs : "typeTerrainCtrl"
		
				
		}).otherwise({
			templateUrl: 'app/views/terrain/terrain-list.html',
			controller: 'HttpController',
			controllerAs : "httpTerrainCtrl"
		})

	});
	terrainsModule.controller('TypeTerrainController',['$location','$scope','TerrainFactory',
			function TypeController($location, $scope,TerrainFactory) {
				$scope.inTypeT = '-select-';
				var promise1 = TerrainFactory.getTypes();
				$scope.terrtypes = [];
				promise1.success(function(data) {
					$scope.terrtypes = data;
					$scope.terrtypes.unshift('all');
					$scope.terrtypes.unshift('-select-');
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
				//this.types = ['-select-','all',"Recreational","transport","agricultural","residential","commercial"];
				 $scope.go=function go(path){
					 if(path=='all')
					{
						 $scope.inTypeT = '-select-';
						 $location.path('/terrains');
						type=path;
						
					}
					 else
					 {
						 $scope.inTypeT = '-select-';
						 $location.path('/terrainType/'+path);
						 type=path;
					 }
				}
			}]);
	
	terrainsModule.controller('AllTerrainsController', [ '$scope','$timeout', 'TerrainFactory','AuthenticationFactory',
			function($scope,$timeout, TerrainFactory,AuthenticationFactory) {
				$scope.terrains = [];
				$scope.status="loading";
				var promise = TerrainFactory.findAll();
				promise.success(function(data) {
					$scope.terrains = data;
					
					$timeout(function () {
						if($scope.terrains.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});

			} ]);
	terrainsModule.controller('typeTerrainController', [ '$scope','$timeout','$routeParams', 'TerrainFactory','AuthenticationFactory',
   			function($scope,$timeout,$routeParams, TerrainFactory,AuthenticationFactory) {
				var type = $routeParams.type;
   				$scope.terrains = [];
   				$scope.status="loading";
   				var promise = TerrainFactory.findByType(type);
   				promise.success(function(data) {
   					$scope.terrains = data;
   					
   					$timeout(function () {
						if($scope.terrains.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
   				}).error(function(data, status, header, config) {
   					$scope.status = "empty";
   					if(status==401||status==403)//unauthorized
   					{
   						AuthenticationFactory.unauthorized();
   						
   					}else
   					if(data!=null)
   					{
   						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
   						$scope.message=data.details;
   					}else alert("Could not connect to server!\n status:"+status);
   				});

   			} ]);
	terrainsModule.controller('deleteTerrainController', [ '$scope','$timeout','$routeParams','$location', 'TerrainFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, TerrainFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.terrains = [];
			$scope.status="loading";
			var promise = TerrainFactory.deleteTerrain(id);
			promise.success(function(data) {
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	terrainsModule.controller('TerrainController', [ '$scope', '$routeParams','TerrainFactory','AuthenticationFactory',
	         function($scope, $routeParams, TerrainFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = TerrainFactory.findById(id);
				$scope.terrain = null;
				promise.success(function(data) {
					$scope.terrain = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	terrainsModule.controller('GoSaveTerrainController', [ '$scope','$timeout','$location','TerrainFactory','AuthenticationFactory',
         function($scope,$timeout,$location, TerrainFactory,AuthenticationFactory) {
			
			var promise = TerrainFactory.getTypes();
			$scope.types = null;
			promise.success(function(data) {
				$scope.types = data;
			}).error(function(data, status, header, config) {
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
		$scope.saveTerrain=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainFactory.saveTerrain($scope.terrain);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
				 
			});
			res.error(function(data,status,headers,config){
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	terrainsModule.controller('UpdateTerrainController', [ '$scope','$location','$routeParams','TerrainFactory','AuthenticationFactory',
         function($scope,$location,$routeParams, TerrainFactory,AuthenticationFactory) {
		
		var promise1 = TerrainFactory.getTypes();
		$scope.types = [];
		promise1.success(function(data) {
			$scope.types = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		var id = $routeParams.id;
		var promise = TerrainFactory.findById(id);
		$scope.terrain = null;
		promise.success(function(data) {
			$scope.terrain = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		$scope.saveTerrain=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainFactory.updateTerrain(id,$scope.terrain);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/terrains');
				else $location.path('/terrainType/'+type);
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	

})();
