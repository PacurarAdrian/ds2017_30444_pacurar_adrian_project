(function() {

	var terrainTypesModule = angular.module('terrainTypeControllers', [ 'ngRoute' ])
	var type="all";
	terrainTypesModule.config(function($routeProvider) {
		$routeProvider.when('/terrainTypes', {
			templateUrl : 'app/views/terrain/terrainType-list.html',
			controller : 'AllTerrainTypesController',
			controllerAs : "allTerrainTypesCtrl"
		}).when('/terrainTypeGoSave',{
			templateUrl: 'app/views/terrain/terrainType-insert.html',
			controller: 'GoSaveTerrainTypeController',
			controllerAs: "goSaveTerrainTypeCtrl"
		}).when('/terrainTypeDetails/:id', {
			templateUrl : 'app/views/terrain/terrainType-details.html',
			controller : 'TerrainTypeController',
			controllerAs : "terrainTypeCtrl"
		}).when('/terrainTypeUpdate/:id', {
			templateUrl : 'app/views/terrain/terrainType-insert.html',
			controller : 'UpdateTerrainTypeController',
			controllerAs : "updateTerrainTypeCtrl"
		}).when('/terrainTypeDelete/:id', {
			templateUrl : 'app/views/terrain/terrainType-list.html',
			controller : 'deleteTerrainTypeController',
			controllerAs : "deleteTerrainTypeCtrl"	
		})

	});
	
	
	terrainTypesModule.controller('AllTerrainTypesController', [ '$scope','$timeout', 'TerrainTypeFactory','AuthenticationFactory',
			function($scope,$timeout, TerrainTypeFactory,AuthenticationFactory) {
				$scope.terrainTypes = [];
				$scope.status="loading";
				var promise = TerrainTypeFactory.findAll();
				promise.success(function(data) {
					$scope.terrainTypes = data;
					
					$timeout(function () {
						if($scope.terrainTypes.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});

			} ]);
	
	terrainTypesModule.controller('deleteTerrainTypeController', [ '$scope','$timeout','$routeParams','$location', 'TerrainTypeFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, TerrainTypeFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.terrainTypes = [];
			$scope.status="loading";
			var promise = TerrainTypeFactory.deleteTerrain(id);
			promise.success(function(data) {
				
					$location.path('/terrainTypes');
				
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	terrainTypesModule.controller('TerrainTypeController', [ '$scope', '$routeParams','TerrainTypeFactory','AuthenticationFactory',
	         function($scope, $routeParams, TerrainTypeFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = TerrainTypeFactory.findById(id);
				$scope.terrainType = null;
				promise.success(function(data) {
					$scope.terrainType = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	terrainTypesModule.controller('GoSaveTerrainTypeController', [ '$scope','$timeout','$location','TerrainTypeFactory','AuthenticationFactory',
         function($scope,$timeout,$location, TerrainTypeFactory,AuthenticationFactory) {
			
			
		$scope.saveTerrainType=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainTypeFactory.saveTerrain($scope.terrainType);
			
			res.success(function(data,status,headers,config){
				
				
					$location.path('/terrainTypes');
				
				 
			});
			res.error(function(data,status,headers,config){
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	terrainTypesModule.controller('UpdateTerrainTypeController', [ '$scope','$location','$routeParams','TerrainTypeFactory','AuthenticationFactory',
         function($scope,$location,$routeParams, TerrainTypeFactory,AuthenticationFactory) {
		
		
		var id = $routeParams.id;
		var promise = TerrainTypeFactory.findById(id);
		$scope.terrainType = null;
		promise.success(function(data) {
			$scope.terrainType = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		$scope.saveTerrainType=function(){
			
			$scope.message="Processing data...";
			
			var res = TerrainTypeFactory.updateTerrain(id,$scope.terrainType);
			
			res.success(function(data,status,headers,config){
				
				
					$location.path('/terrainTypes');
				
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	

})();
