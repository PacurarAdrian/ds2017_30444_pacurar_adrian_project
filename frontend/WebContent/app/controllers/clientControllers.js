(function() {

	var clientsModule = angular.module('clientControllers', [ 'ngRoute' ])
	var type="all";
	clientsModule.config(function($routeProvider) {
		$routeProvider.when('/clients', {
			templateUrl : 'app/views/client/client-list.html',
			controller : 'AllClientsController',
			controllerAs : "allClientsCtrl"
		}).when('/clientGoSave',{
			templateUrl: 'app/views/client/client-insert.html',
			controller: 'GoSaveClientController',
			controllerAs: "goSaveClientCtrl"
		}).when('/clientAcc/:id', {
			templateUrl : 'app/views/client/client-accDetails.html',
			controller : 'ClientAccController',
			controllerAs : "clientAccCtrl"
		}).when('/clientInfo/:id', {
			templateUrl : 'app/views/client/client-infoDetails.html',
			controller : 'ClientInfoController',
			controllerAs : "clientInfoCtrl"
		}).when('/clientUpdateInfo/:id', {
			templateUrl : 'app/views/client/client-infoUpdate.html',
			controller : 'UpdateInfoClientController',
			controllerAs : "updateClientInfoCtrl"
		}).when('/clientUpdateAcc/:id', {
			templateUrl : 'app/views/client/client-accUpdate.html',
			controller : 'UpdateAccClientController',
			controllerAs : "updateClientAccCtrl"
		}).when('/clientDelete/:id', {
			templateUrl : 'app/views/client/client-list.html',
			controller : 'deleteClientController',
			controllerAs : "deleteClientCtrl"
		}).when('/clientType/:type', {
			templateUrl : 'app/views/client/client-list.html',
			controller : 'typeClientController',
			controllerAs : "typeClientCtrl"
		
				
		}).otherwise({
			templateUrl: 'app/views/client/client-list.html',
			controller: 'HttpController',
			controllerAs : "httpClientCtrl"
		})

	});
	clientsModule.controller('TypeClientController', 
			function TypeController($location, $scope) {
				$scope.inTypeC = '-select-';
				this.types = ['-select-','all',"regular","other","private"];
				 $scope.go=function go(path){
					 if(path=='all')
					{
						 $scope.inTypeC = '-select-';
						 $location.path('/clients');
						type=path;
						
					}
					 else
					 {
						 $scope.inTypeC = '-select-';
						 $location.path('/clientType/'+path);
						 type=path;
					 }
				}
			});
	
	clientsModule.controller('AllClientsController', [ '$scope','$timeout', 'ClientFactory','AuthenticationFactory',
			function($scope,$timeout, ClientFactory,AuthenticationFactory) {
				$scope.clients = [];
				$scope.status="loading";
				var promise = ClientFactory.findAll();
				promise.success(function(data) {
					$scope.clients = data;
					
					$timeout(function () {
						if($scope.clients.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
					
				});

			} ]);
	clientsModule.controller('typeClientController', [ '$scope','$timeout','$routeParams', 'ClientFactory','AuthenticationFactory',
   			function($scope,$timeout,$routeParams, ClientFactory,AuthenticationFactory) {
				var type = $routeParams.type;
   				$scope.clients = [];
   				$scope.status="loading";
   				var promise = ClientFactory.findByType(type);
   				promise.success(function(data) {
   					$scope.clients = data;
   					
   					$timeout(function () {
						if($scope.clients.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
   				}).error(function(data, status, header, config) {
   					$scope.status = "empty";
   					if(status==401||status==403)//unauthorized
   					{
   						AuthenticationFactory.unauthorized();
   						
   					}else
   					if(data!=null)
   					{
   						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
   						$scope.message=data.details;
   					}else alert("Could not connect to server!\n status:"+status);
   				});

   			} ]);
	clientsModule.controller('deleteClientController', [ '$scope','$timeout','$routeParams','$location', 'ClientFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, ClientFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.clients = [];
			$scope.status="loading";
			var promise = ClientFactory.deleteClient(id);
			promise.success(function(data) {
				if(type=='all')
					$location.path('/clients');
				else $location.path('/clientType/'+type);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	clientsModule.controller('ClientAccController', [ '$scope', '$routeParams','ClientFactory','AuthenticationFactory',
	         function($scope, $routeParams, ClientFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = ClientFactory.findAccById(id);
				$scope.client = null;
				promise.success(function(data) {
					$scope.client = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	clientsModule.controller('ClientInfoController', [ '$scope', '$routeParams','ClientFactory','RepartitionFactory','AuthenticationFactory',
  	         function($scope, $routeParams, ClientFactory,RepartitionFactory,AuthenticationFactory) {
  				var id = $routeParams.id;
  				
  				var promise = RepartitionFactory.findTerrainsByClientId(id);
  				$scope.terrains = null;
  				
  				promise.success(function(data) {
  					$scope.terrains = data;
  				}).error(function(data, status, header, config) {
  					alert("Problem occured while connecting to server: "+status);
  				});
  				promise = ClientFactory.findInfoById(id);
  				$scope.client = null;
  				$scope.accId=id;
  				promise.success(function(data) {
  					$scope.client = data;
  				}).error(function(data, status, header, config) {
  					if(status==401||status==403)//unauthorized
  					{
  						AuthenticationFactory.unauthorized();
  						
  					}else
  					if(data!=null)
  					{
  						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
  						$scope.message=data.details;
  					}else alert("Could not connect to server!\n status:"+status);
  				});
  			} ]);
	clientsModule.controller('GoSaveClientController', [ '$scope','$timeout','$location','ClientFactory','AuthenticationFactory',
         function($scope,$timeout,$location, ClientFactory,AuthenticationFactory) {
			
			var promise = ClientFactory.getTypes();
			$scope.types = null;
			promise.success(function(data) {
				$scope.types = data;
			}).error(function(data, status, header, config) {
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
		$scope.saveClient=function(){
			
			$scope.message="Processing data...";
			
			var res = ClientFactory.saveClient($scope.client);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/clients');
				else $location.path('/clientType/'+type);
				 
			});
			res.error(function(data,status,headers,config){
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	clientsModule.controller('UpdateInfoClientController', [ '$scope','$location','$routeParams','ClientFactory','AuthenticationFactory',
         function($scope,$location,$routeParams, ClientFactory,AuthenticationFactory) {
		
		var promise1 = ClientFactory.getTypes();
		$scope.types = [];
		promise1.success(function(data) {
			$scope.types = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		var id = $routeParams.id;
		var promise = ClientFactory.findInfoById(id);
		$scope.client = null;
		promise.success(function(data) {
			$scope.client = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		$scope.saveClient=function(){
			
			$scope.message="Processing data...";
			
			var res = ClientFactory.updateClientInfo(id,$scope.client);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/clients');
				else $location.path('/clientType/'+type);
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	
	clientsModule.controller('UpdateAccClientController', [ '$scope','$location','$routeParams','ClientFactory','AuthenticationFactory',
     function($scope,$location,$routeParams, ClientFactory,AuthenticationFactory) {
	
	var promise1 = ClientFactory.getTypes();
	$scope.types = [];
	promise1.success(function(data) {
		$scope.types = data;
	}).error(function(data, status, header, config) {
		if(status==401||status==403)//unauthorized
		{
			AuthenticationFactory.unauthorized();
			
		}else
		if(data!=null)
		{
			alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
			$scope.message=data.details;
		}else alert("Could not connect to server!\n status:"+status);
	});
	var id = $routeParams.id;
	var promise = ClientFactory.findAccById(id);
	$scope.client = null;
	promise.success(function(data) {
		$scope.client = data;
	}).error(function(data, status, header, config) {
		if(status==401||status==403)//unauthorized
		{
			AuthenticationFactory.unauthorized();
			
		}else
		if(data!=null)
		{
			alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
			$scope.message=data.details;
		}else alert("Could not connect to server!\n status:"+status);
	});
	$scope.saveClient=function(){
		
		$scope.message="Processing data...";
		
		var res = ClientFactory.updateClientAcc(id,$scope.client);
		
		res.success(function(data,status,headers,config){
			
			if(type=='all')
				$location.path('/clients');
			else $location.path('/clientType/'+type);
		});
		res.error(function(data,status,headers,config){
			
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		
	}	
		
		
	} ]);
})();
