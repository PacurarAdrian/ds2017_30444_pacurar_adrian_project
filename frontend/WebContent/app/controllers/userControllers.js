(function() {

	var usersModule = angular.module('userControllers', [ 'ngRoute' ])
	var type="all";
	usersModule.config(function($routeProvider) {
		$routeProvider.when('/users', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'AllUsersController',
			controllerAs : "allUsersCtrl"
		}).when('/userGoSave',{
			templateUrl: 'app/views/user/user-insert.html',
			controller: 'GoSaveUserController',
			controllerAs: "goSaveUserCtrl"
		}).when('/userSave',{
			templateUrl: 'app/views/user/user-list.html',
			controller: 'SaveUserController',
			controllerAs: "saveUserCtrl"
		}).when('/user/:id', {
			templateUrl : 'app/views/user/user-details.html',
			controller : 'UserController',
			controllerAs : "userCtrl"
		}).when('/userUpdate/:id', {
			templateUrl : 'app/views/user/user-insert.html',
			controller : 'UpdateUserController',
			controllerAs : "updateUserCtrl"
		}).when('/userDelete/:id', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'deleteUserController',
			controllerAs : "deleteUserCtrl"
		}).when('/userType/:type', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'typeUserController',
			controllerAs : "typeUserCtrl"
		
				
		}).otherwise({
			templateUrl: 'app/views/user/user-list.html',
			controller: 'HttpController',
			controllerAs : "httpUserCtrl"
		})

	});
	usersModule.controller('TypeUserController', 
			function TypeController($location, $scope) {
				$scope.inType = '-select-';
				this.types = ['-select-','all','regular', 'employee', 'admin'];
				 $scope.go=function go(path){
					 if(path=='all')
					{
						 $scope.inType = '-select-';
						 $location.path('/users');
						type=path;
						
					}
					 else
					 {
						 $scope.inType = '-select-';
						 $location.path('/userType/'+path);
						 type=path;
					 }
				}
			});
	
	usersModule.controller('AllUsersController', [ '$scope','$timeout', 'UserFactory','AuthenticationFactory',
			function($scope,$timeout, UserFactory,AuthenticationFactory) {
				$scope.users = [];
				$scope.status="loading";
				var promise = UserFactory.findAll();
				promise.success(function(data) {
					$scope.users = data;
					
					$timeout(function () {
						if($scope.users.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
				}).error(function(data, status, header, config) {
					$scope.status = "empty";
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});

			} ]);
	usersModule.controller('typeUserController', [ '$scope','$timeout','$routeParams', 'UserFactory','AuthenticationFactory',
   			function($scope,$timeout,$routeParams, UserFactory,AuthenticationFactory) {
				var type = $routeParams.type;
   				$scope.users = [];
   				$scope.status="loading";
   				var promise = UserFactory.findByType(type);
   				promise.success(function(data) {
   					$scope.users = data;
   					
   					$timeout(function () {
						if($scope.users.length==0)
				        $scope.status = "empty";
						else $scope.status = "full";
				    }, 2000);
   				}).error(function(data, status, header, config) {
   					$scope.status = "empty";
   					if(status==401||status==403)//unauthorized
   					{
   						AuthenticationFactory.unauthorized();
   						
   					}else
   					if(data!=null)
   					{
   						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
   						$scope.message=data.details;
   					}else alert("Could not connect to server!\n status:"+status);
   				});

   			} ]);
	usersModule.controller('deleteUserController', [ '$scope','$timeout','$routeParams','$location', 'UserFactory','AuthenticationFactory',
		function($scope,$timeout,$routeParams,$location, UserFactory,AuthenticationFactory) {
		var id = $routeParams.id;
			$scope.users = [];
			$scope.status="loading";
			var promise = UserFactory.deleteUser(id);
			promise.success(function(data) {
				if(type=='all')
					$location.path('/users');
				else $location.path('/userType/'+type);
			}).error(function(data, status, header, config) {
				$scope.status = "empty";
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});

		} ]);
	usersModule.controller('UserController', [ '$scope', '$routeParams','UserFactory','AuthenticationFactory',
	         function($scope, $routeParams, UserFactory,AuthenticationFactory) {
				var id = $routeParams.id;
				
				var promise = UserFactory.findById(id);
				$scope.user = null;
				promise.success(function(data) {
					$scope.user = data;
				}).error(function(data, status, header, config) {
					if(status==401||status==403)//unauthorized
					{
						AuthenticationFactory.unauthorized();
						
					}else
					if(data!=null)
					{
						alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
						$scope.message=data.details;
					}else alert("Could not connect to server!\n status:"+status);
				});
			} ]);
	usersModule.controller('GoSaveUserController', [ '$scope','$timeout','$location','UserFactory','AuthenticationFactory',
         function($scope,$timeout,$location, UserFactory,AuthenticationFactory) {
			
			var promise = UserFactory.getTypes();
			$scope.types = null;
			promise.success(function(data) {
				$scope.types = data;
			}).error(function(data, status, header, config) {
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
		$scope.saveUser=function(){
			
			$scope.message="Processing data...";
			
			var res = UserFactory.saveUser($scope.user);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/users');
				else $location.path('/userType/'+type);
				 
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	usersModule.controller('UpdateUserController', [ '$scope','$location','$routeParams','UserFactory','AuthenticationFactory',
         function($scope,$location,$routeParams, UserFactory,AuthenticationFactory) {
		
		var promise1 = UserFactory.getTypes();
		$scope.types = [];
		promise1.success(function(data) {
			$scope.types = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		var id = $routeParams.id;
		var promise = UserFactory.findById(id);
		$scope.user = null;
		promise.success(function(data) {
			$scope.user = data;
		}).error(function(data, status, header, config) {
			if(status==401||status==403)//unauthorized
			{
				AuthenticationFactory.unauthorized();
				
			}else
			if(data!=null)
			{
				alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
				$scope.message=data.details;
			}else alert("Could not connect to server!\n status:"+status);
		});
		$scope.saveUser=function(){
			
			$scope.message="Processing data...";
			
			var res = UserFactory.updateUser(id,$scope.user);
			
			res.success(function(data,status,headers,config){
				
				if(type=='all')
					$location.path('/users');
				else $location.path('/userType/'+type);
			});
			res.error(function(data,status,headers,config){
				
				if(status==401||status==403)//unauthorized
				{
					AuthenticationFactory.unauthorized();
					
				}else
				if(data!=null)
				{
					alert("failure message:"+data.statusMessage);//JSON.stringify({data:data}));
					$scope.message=data.details;
				}else alert("Could not connect to server!\n status:"+status);
			});
			
		}	
			
			
		} ]);
	
	
	var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];

})();
