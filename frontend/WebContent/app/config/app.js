(function() {
	var app = angular.module('terrainManagerAJS', [ 'ngRoute','ngCookies', 'configModule',
			'navControllers', 'userControllers' ,'terrainControllers', 'userServices',
			'terrainServices','loginControllers','clientControllers','clientServices',
			'authenticationServices','clientTerrainControllers',
			'historyControllers','historyServices',
			'repartitionControllers','repartitionServices',
			'terrainTypeControllers','terrainTypeServices','chart.js'])
			
			app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.headers.common['Authorization'] = 'Basic';
		$httpProvider.defaults.withCredentials = true;
}])
})();