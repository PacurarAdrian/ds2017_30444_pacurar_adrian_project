package messageHandler;

import com.rabbitmq.client.*;

import services.MailService;
import terrain.manager.entities.*;

import java.io.IOException;

public class ReceiveLogs {
  private static final String EXCHANGE_NAME = "logs";
 public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
    String queueName = channel.queueDeclare().getQueue();
    channel.queueBind(queueName, EXCHANGE_NAME, "");

    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    MailService mailService = new MailService("ds.laboratory1@gmail.com","ro.tuc.dsrl.ds.handsonbdv");
    Consumer consumer = new DefaultConsumer(channel) {
    	//int count=0;
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope,
                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
    	  String message = null ;//= new String(body, "UTF-8");
    	 // ReadWriteTextFileJDK7 fileWriter=new ReadWriteTextFileJDK7();
    	  System.out.println("\n [x] Received ");
          try {
  			Content<?> dvd=new Content<Notification>(body,Notification.class);
  			message=dvd.toString();
  			Notification notif=(Notification) dvd.getItem();
  			//fileWriter.writeSmallTextFile(Arrays.asList(message), PATH+"message"+queueName+count+".txt");
  			mailService.sendMail(notif.getAddress(),"Terrain manager notification!",message);
			//count++;
			System.out.print("'" + message + "'");
  		} catch (ClassNotFoundException e) {
  			
  			System.err.println("\ncould not create message object");
  			e.printStackTrace();
  		}
        
      }
    };
    channel.basicConsume(queueName, true, consumer);
  }
}
