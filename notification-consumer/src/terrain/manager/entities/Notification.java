package terrain.manager.entities;

import java.io.Serializable;
import java.util.Date;

public class Notification implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date date;
	private String message;
	private String address;
	
	public Notification(String address)
	{
		this("",address);
	}
	public Notification(String title,String address)
	{
		this.date=new Date();
		this.message=title;
		this.address=address;
		
	}
	@Override
	public String toString()
	{
		return message+"\n.Notification sent on "+date+".";
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date year) {
		this.date = year;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String title) {
		this.message = title;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
